<?php
return [
    'users'=> 'crud:api Users --fields_from_file="database/crud/users.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'brands'=> 'crud:api Brands --fields_from_file="database/crud/brands.json" --controller-namespace=Api/DataLake --route-group=datalake',

    // 'campaign_category'=> 'crud:api Brands --fields_from_file="database/crud/brands.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'campaign_category'=> 'appmake:migration CampaignCategory --fields_from_file="database/crud/campaign_category.json"',

    'campaign_matching_tags'=> 'appmake:migration campaign_matching_tags --fields_from_file="database/crud/campaign_matching_tags.json"',

    'campaigns'=> 'crud:api Campaigns --fields_from_file="database/crud/campaigns.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'category'=> 'crud:api Categories --fields_from_file="database/crud/categories.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'consumer_freebies'=> 'appmake:model_migrate ConsumerFreebies --fields_from_file="database/crud/consumer_freebies.json"',

    'consumer_matching'=> 'appmake:migration ConsumerMatchingTag --fields_from_file="database/crud/consumer_matching_tags.json"',

    'consumer_surveys'=> 'appmake:migration ConsumerSurvey --fields_from_file="database/crud/consumer_surveys.json"',

    'consumers'=> 'crud:api Consumers --fields_from_file="database/crud/consumers.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'email_resets'=> 'appmake:migration EmailReset --fields_from_file="database/crud/email_resets.json"',

    'feedback_surveys'=> 'appmake:model_migrate FeedbackSurveys --fields_from_file="database/crud/feedback_surveys.json"',

    'freebie'=> 'crud:api Freebie --fields_from_file="database/crud/freebie.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'freebie_matching_tags'=> 'appmake:migration FreebieMatchingTags --fields_from_file="database/crud/freebie_matching_tags.json"',

    'inbound_inventories'=> 'crud:api InboundInventories --fields_from_file="database/crud/inbound_inventories.json" --controller-namespace=Api/DataLake --route-group=datalake',

    'inventory_logs'=> 'appmake:model_migrate InventoryLogs --fields_from_file="database/crud/inventory_logs.json"',

    'matching_tags'=> 'appmake:migration MatchingTags --fields_from_file="database/crud/matching_tags.json"',

    'onboarding_surveys'=> 'appmake:migration OnboardingSurveys --fields_from_file="database/crud/onboarding_surveys.json"',

    'outbound_inventories'=> 'appmake:migration OutboundInventories --fields_from_file="database/crud/outbound_inventories.json"',

    'password_resets'=> 'appmake:migration PasswordReset --fields_from_file="database/crud/password_resets.json"',

    'surveys'=> 'crud:api Surveys --fields_from_file="database/crud/surveys.json" --controller-namespace=Api/DataLake --route-group=datalake',

];
