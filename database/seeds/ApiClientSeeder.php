<?php

use App\ApiClient;
use App\ApiClientPermission;
use App\Applets\ApiKey;
use Illuminate\Database\Seeder;

class ApiClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gen = new ApiKey();

        ApiClient::insert([
            ['name' => 'Organic',
                //'public_key' => $gen->setKey(),
                'public_key' => 'K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs',
                'secret_key' => md5("api_secret"),
                'access_level' => 1,
                'host' => 'gethazina.com',
                'has_verified_email' => false,
                'has_verified_phone' => false,
            ],
            ['name' => 'Organic DEV',
                // 'public_key' => $gen->setKey(),
                'public_key' => 'amfmw136hC1RjhdZCamMeiN8x349utATvrcXWJo8vfUQRKHYnGGxJQLxQ9JI',
                'secret_key' => md5("api_secret_dev"),
                'access_level' => 1,
                'host' => 'localhost',
                'has_verified_email' => false,
                'has_verified_phone' => false,
            ],
            [
                'name' => '9Pay',
                'public_key' => '2FLqPEPj9cu0G6ZCbnu8A6LpzlT2DGtayJGkv8tGWjxlWeC6jC1uWXD9Puqh',
                'secret_key' => md5("api_secret_9pay"),
                'access_level' => 2,
                'host' => '9pay.ng',
                'has_verified_email' => true,
                'has_verified_phone' => true,
            ],
            [
                'name' => 'Lead Pages',
                'public_key' => 'L0VqKP9JrpRjNIhcjaud0S682sdCCGEM5rnAs5wHoPHITZUuHkaBpUBGFUtO',
                'secret_key' => md5("api_secret_leads"),
                'access_level' => 2,
                'host' => 'onboard.gethazina.com',
                'has_verified_email' => false,
                'has_verified_phone' => false,
            ],

        ]);

        ApiClientPermission::insert([
            [
                'api_client_id'=> 2,
                'action'=> 'can_create_consumer',
                'permission'=> true,
            ],
            [
                'api_client_id'=> 3,
                'action'=> 'can_create_consumer',
                'permission'=> true,
            ],
        ]);

    }
}
