<?php

use Illuminate\Database\Seeder;

class ShippingZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zones = ['HQ', 'Nigeria'];

        $locations = [];

        foreach($zones as $zone){
            $locations[] = [
                'name'=> $zone,
                'description'=> '',
            ];
        }

       DB::table('shipping_zones')->insert($locations);
    }
}
