<?php

use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        if (User::count() == 0) {
            //$role = Role::where('name', 'admin')->firstOrFail();

            User::create([
                'username' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password'),
                //'remember_token' => Str::random(60),
                'role_id' => 1,
                'email_verified' => 1,
                'phone_verified' => 1,
            ]);

            factory(App\User::class, 1)->create([
                    'username' => 'buhari',
                    'email' => 'buhari@app.com',
                    'password' => bcrypt('password'),
                    //'remember_token' => Str::random(60),
                    'role_id' => 3,
                    'email_verified' => 1,
                    'phone_verified' => 1,
            ])->each(function ($user) use ($faker) {
                $user->consumer()->save(factory(App\Consumer::class)->make());
                $user->shipping_address()->save(factory(App\ShippingAddress::class)->make([
                    'phone' => $user->phone,
                    'email' => $user->email,
                ])); 
            });

        }
    }
}
