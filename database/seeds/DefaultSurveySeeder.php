<?php

use App\Survey;
use App\MatchingTag;
use App\MatchingTagData;
use Illuminate\Database\Seeder;

class DefaultSurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $raw_questions_original = file_get_contents(base_path('stubs/survey/default.json'));
        $raw_questions = json_decode($raw_questions_original, true);
        $all_survey_questions = $raw_questions['pages'][0]['elements'];
        $matching_tags = [];

        $survey = new Survey();
        $survey->title = 'Default Survey';
        $survey->survey = $raw_questions_original;
        $survey->number_of_questions = 4;
        $survey->save();


        foreach($all_survey_questions as $all_survey_question) {
            if($all_survey_question['type'] === 'checkbox' || $all_survey_question['type'] === 'radiogroup' || $all_survey_question['type'] === 'dropdown' || $all_survey_question['type'] === 'imagepicker'){
                $dd = [];
                $dd['tag'] = $all_survey_question['name'];
                $dd['title'] = $all_survey_question['title'];
                $ll = [];
                foreach($all_survey_question['choices'] as $choice){
                    $ll[] = [
                        'value'=> $choice['value'],
                        'title'=> $choice['text'],
                        'matching_tag_id'=> '',
                    ];
                }
                if(isset($all_survey_question['hasNone'])){
                    if($all_survey_question['hasNone'] == true){
                        $ll[] = [
                            'value'=> 'none',
                            'title'=> $all_survey_question['noneText'],
                            'matching_tag_id'=> '',
                        ];
                    }
                }
                $dd['data'] = $ll;
                 $matching_tags[] = $dd;

            }elseif($all_survey_question['type'] === 'rating'){
                $dd = [];
                $dd['tag'] = $all_survey_question['name'];
                $dd['title'] = $all_survey_question['title'];
                $ll = [];
                foreach($all_survey_question['rateValues'] as $choice){
                    $ll[] = [
                        'value'=> $choice['value'],
                        'title'=> $choice['text'],
                        'matching_tag_id'=> '',
                    ];
                }
                $dd['data'] = $ll;
                 $matching_tags[] = $dd;
            }elseif($all_survey_question['type'] === 'boolean'){
                $dd = [];
                $dd['tag'] = $all_survey_question['name'];
                $dd['title'] = $all_survey_question['title'];
                $ll = [
                    [
                        'value'=> $all_survey_question['valueTrue'],
                        'title'=> $all_survey_question['valueName'],
                        'matching_tag_id'=> '',
                    ],
                    [
                        'value'=> $all_survey_question['valueFalse'],
                        'title'=> $all_survey_question['valueName'],
                        'matching_tag_id'=> '',
                    ],
                ];

            }
        }

        foreach($matching_tags as $matching_tag){
            $tagging = new MatchingTag();
            $tagging->name = $matching_tag['tag'];
            $tagging->title = $matching_tag['title'];
            $tagging->survey_id = $survey->id;
            $tagging->is_global = true;
            $tagging->save();
            $tag_data = collect($matching_tag['data'])->map(function($value, $index)use($tagging){
                        $value['matching_tag_id'] = $tagging->id;
                        return $value;
            });
            MatchingTagData::insert($tag_data->toArray());
        }
    }
}
