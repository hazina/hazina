<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class LocationSeeder extends Seeder
{

    public $lagosLgas = [
        "ALIMOSHO"=> [
            "IDIMU",
            "IKOTUN",
            "IPAJA",
            "ABULE EGBA",
            "EGBEDA",
            "EJIGBO",
            "ALAGBADO",
            "AKESON",
            "ISHERI OLOFIN",
            "IJEGUN",
            "EGBE ",
            "AYOBO",
            "ISHERI OSUN",
            "ALAKUKO",
            "IGANDO",
            "MOSAN OKUNOLA",
            "SHASHA AKOWONJO",
            "OKE-ODO / PLEASURE",
            "ABARANJE",
            "IJEGUN",
            "ABORU",
            "BARUWA",
            "IPAJA COMMAND",
            "ABESAN",
            "ALAGUNTAN",
            "IYANA EJIGBO",
            "ILEWE EJIGBO",
            "AKOWONJO",
        ],
        "AJEROMI IFELODUN"=> [
            "SURU ALABA",
            "KIRIKIRI",
            "BADIA",
            "AJEGUNLE",
            "MALU ROAD",
            "IJORA",
            "IGANMU ROAD",
            "ALABA",
        ],
        "KOSOFE"=>[
            "OGUDU GRA",
            "ANTHONY GBAGADA",
            "MENDE",
            "OGUDU  ",
            "MAGODO",
            "ALAPERE",
            "OJOTA",
            "IKOSI",
            "IFAKO / SOLUYI",
            "OWORONSHOKI",
            "MARYLAND",
        ],
        "MUSHIN"=>[
            "AKANKRO",
            "DOSUNMU",
            "ISOLO ROAD",
            "OLOSAN",
            "IDI ARABIA",
            "PAPA AJAO",
        ],
        "OSHODI-ISOLO"=>[
            "AJAO ESTATE",
            "ISOLO",
            "ISAGA TEDO",
            "ILASAMAJA",
            "ISHERI OSUN",
            "IKOTUN ROAD",
            "OKOTA",
            "AGO PALACE ",
            "IRE AKARI",
            "AIRPORT ROAD",
            "ILAMOSE ",
            "OSOLO WAY",
            "CHI LIMITED",
            "OSHODI/BOLADE",
            "ORILE OSHODI",
            "MAFOLUKU",
            "SHOGUNLE",
            "ALASIA",
            "OKE-AFA",
        ],
        "OJO"=>[
            "LASU",
            "IGBO ERIN",
            "IYANA IBA ",
            "OLOJO DRIVE",
            "LAGOS BADAGRY EXP WAY",
            "ALABA INTL MKT",
            "OJO IGBEDE ROAD",
            "IYANA ROAD",
            "ALH GANIYU SALAMI ROAD",
            "IJANIKIN",
            "IMUDE ",
            "OBA OLOTO",
            "UPPER ERA",
            "ISASHI",
            "IYANA ISASHI",
            "COMMAND DAY SEC SCHL",
            "IGBO ELERIN",
            "OKOKOMAIKO",
            "IBA NEW SITE",
            "ISHERI ROAD",
            "IBA HOUSING ESTATE",
            "KETU IJANIKIN",
            "CONSTAIN",
        ],
        "IKORODU"=>[
            "AGRIC ",
            "TOS BENSON",
            "EBUTE IPAKODO",
            "OWODE IBESHE",
            "ORIWU",
            "IRESHE",
            "BAIYEKU",
            "IJEDE ROAD",
            "OBAFEMI AWOLOWO ROAD",
            "OWUTU-ISAWO",
            "ODOGUNYAN",
            "IGBE",
            "IGBOGBO",
        ],
        "SURULERE"=>[
            "COKER",
            "ITIRE",
            "IKATE",
            "OJUELEGBA",
            "LAWANSON",
            "ANIMASHAUN",
            "IGANMU",
            "ALAKA",
            "IJESHA TEDO",
            "ORILE IGANMU",
            "OGUNLANA",
            "AKERELE",
            "YABA",
            "ADENIRAN OGUNSANYA",
            "ALHAJI MASHA",
        ],
        "AGEGE"=>[
            "DOPEMU",
            "CAPITOL",
            "OGBA",
            "IJAIYE",
            "AYOBO GARAGE",
            "PAPA ASHAFA",
            "ALFA NLA",
            "ORILE AGEGE",
            "MAGBON",
            "IFAKO AGEGE",
            "FAGBA",
            "OKE IRA",
        ],
        "IFAKO IJAIYE"=>[
            "OJOKORO",
            "COLLEGE ROAD",
            "IJU",
            "AJUWON",
            "AGBADO",
            "AGBE",
            "JONATHAN COKER",
            "ALAKUKO",
            "AGBADO IJOKO",
            "IJOKO",
            "ADETIBA",
            "NURENI YUSUF",
            "POST OFFICE",
            "ITOKI",
            "ENKALON",
            "ORILE ILOYE",
            "ALEX UDU",
            "AGBADO CROSSING",
            "TEMPLE",
            "JESU OSEUN",
        ],
        "SOMOLU"=>[
            "AKOKA",
            "PEDRO",
            "IGBOBI",
            "BARIGA",
            "BAJULAYE",
            "ST. FINBARRS COLLEGE",
            "ABULE OKUTA",
            "OBANIKORO",
            "JIBOWU",
            "GBAGADA PHASE 1",
            "ONIPANU",
            "BASHUA",
            "IJEBU TEDO",
            "IGBARI",
            "FADEYI",
        ],
        "AMUWO ODOFIN"=>[
            "ALAKOSO",
            "AMUWO ODOFIN ESTATE",
            "FESTAC TOWN",
            "AGO PALACE WAY",
            "TRADE FAIR COMPLEX",
            "ASPAMDA",
            "AGBOJU",
            "KIRIKIRI",
            "MILE 2",
            "AMUWO  ",
            "OLUTE",
            "NAVY TOWN",
            "SATELLITE TOWN",
            "ABULE OSHUN",
            "VOLKS",
            "IREDE",
            "MUWO TEDI",
            "FAMOUS ENEARU",
            "OLD OJO",
            "MAZAMAZA",
            "IBAFON",
        ],
        "LAGOS MAINLAND"=>[
            "IDDO",
            "IJORA",
            "IGANMU",
            "EBUTE METTA",
            "OYINGBO",
            "ADEKUNLE",
            "MAKOKO",
            "ALAGOMEJI",
            "OYADIRAN ESTATE",
            "UNILAG",
            "ABULE IJESHA",
            "AKOKA",
            "IWAYA",
            "SABO YABA",
        ],
        "IKEJA"=>[
            "OBA AKRAN",
            "ALEN",
            "OREGUN",
            "ADENIYI JONES",
            "OBAFEMI AWOLOWO ROAD",
            "BILLINGS WAY",
            "OPEBI",
            "TOYIN STR",
            "MEDICAL ROAD",
            "COMPUTER VILLAGE",
            "KUDIRAT ABIOLA WAY",
            "MORISSON CRESCENT",
            "ALAUSA",
            "AGIDINGBI",
        ],
        "ETI OSA"=>[
            "LAMBASA",
            "BADORE",
            "ADDO ROAD",
            "PHASE 2 LEKKI GARDENS",
            "SANGOTEDO",
            "LEKKI GARDENS PHASE 1",
            "AJAH",
            "LEKKI PHASE 2",
            "VGC",
            "IKOTA",
            "ONIRU",
            "LEKKI PHASE 1",
            "ADMIRALTY",
            "OSAPA",
            "BANANA ISLAND",
            "IKOYI",
            "DOLPHINE ESTATE",
            "OSBORNE",
            "PARKVIEW ESTATE",
            "VICTORIA ISLAND",
            "MAROKO",
            "OBALENDE",
            "ONIKAN",
        ],
        "BADAGRY"=>[
            "AJARA FARM SETTLEMENT",
            "AGRIC",
            "YAFIN",
            "TAKO",
            "ISALU",
            "TOZUNKAHMEH",
            "ILOGBO EREMI",
            "AJARA GARAGE",
            "TOPO GARAGE",
            "MARINA",
            "EBUTE OJA",
        ],
        "APAPA"=>[
            "ABULE NLA",
            "IJORA",
            "BADIA",
            "IGANMU",
            "IJORA OLOPA",
            "GASKIYA",
            "AJEGUNLE",
            "APAPA QUAYS",
            "TIN CAN ISLAND",
            "NAVAL DOCKYARD",
        ],
        "LAGOS ISLAND"=>[
            "ADENIJI ADELE",
            "MARINA",
            "BALOGUN",
            "APONGBON",
            "CMS",
        ],
        "EPE"=>[
            "AGBOWA",
            "EJIRIN",
            "IBONWON",
            "POKA",
            "ORISHA",
            "IRAYE",
            "ILARA",
            "EYINDI",
            "IKORODU EPE EXP WAY",
        ],
        "IBEJU/LEKKI"=>[
            "AWOYAYA",
            "LAKOWE",
            "EPUTU TOWN",
            "LAGASA",
            "IWEREKUN",
            "AIYETEJU",
            "ELEKO BEACH",
            "KAYETORO",
            "ORIMEDU",
        ]
        ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $ferker)
    {
        //countries
        $this->saveCountries();
        $this->saveStates();
        $this->saveLagosLga();
        $this->saveLagosArea();
    }

    public function saveStates(){
        $getParent = DB::table('locations')->where('name', 'Nigeria')->first();
        $states = [
            "Abia",
            "Adamawa",
            "Anambra",
            "Akwa Ibom",
            "Bauchi",
            "Bayelsa",
            "Benue",
            "Borno",
            "Cross River",
            "Delta",
            "Ebonyi",
            "Enugu",
            "Edo",
            "Ekiti",
            "FCT - Abuja",
            "Gombe",
            "Imo",
            "Jigawa",
            "Kaduna",
            "Kano",
            "Katsina",
            "Kebbi",
            "Kogi",
            "Kwara",
            "Lagos",
            "Nasarawa",
            "Niger",
            "Ogun",
            "Ondo",
            "Osun",
            "Oyo",
            "Plateau",
            "Rivers",
            "Sokoto",
            "Taraba",
            "Yobe",
            "Zamfara"
        ];

        $data = [];
        foreach($states as $state){
            $data[] = [
                'parent_id'=> $getParent->id,
                'name'=> $state,
                'created_at'=>now(),
                'updated_at'=>now(),
            ];
        }
        DB::table('locations')->insert($data);

    }

    public function saveCountries(){
        $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

        //Build Database
        $data = [];
        foreach($countries as $country){
            $data[] = [
                'name'=> $country,
                'created_at'=>now(),
                'updated_at'=>now(),
            ];
        }
        DB::table('locations')->insert($data);
    }

    public function saveLagosLga(){
        $getParent = DB::table('locations')->where('name', 'Lagos')->first();
        $lgas = collect($this->lagosLgas);
        $all = $lgas->keys()->all();
        $data = [];
        foreach($all as $al){
            $data[] = [
                'parent_id'=> $getParent->id,
                'name'=> Str::title($al),
                'created_at'=>now(),
                'updated_at'=>now(),
            ];
        }
        DB::table('locations')->insert($data);
    }

    public function saveLagosArea(){
        $areas = collect($this->lagosLgas);
        $data = [];
        foreach($areas as $key => $value){
            $getParent = DB::table('locations')->where('name', Str::title($key))->first();
            foreach($value as $val){
                $data[] = [
                    'parent_id'=> $getParent->id,
                    'name'=> Str::title($val),
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ];
            }
        }
        DB::table('locations')->insert($data);

    }
}
