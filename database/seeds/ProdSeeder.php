<?php

use App\OnboardingSurvey;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ProdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            $this->call(ShippingZoneSeeder::class);
            $this->call(ShippingLocationSeeder::class);
            $this->call(DefaultSurveySeeder::class);
            $this->call(OnboardSurveySeeder::class);
            $this->call(ApiClientSeeder::class);
            OnboardingSurvey::create([
                'survey_id'=> 1,
                'created_at'=> now(),
                'updated_at'=> now()
            ]);
            User::create([
                'username' => 'Admin',
                'email' => 'admin@hazina.ng',
                'password' => bcrypt('bw5SG2exh5UEQ4a9'),
                //'remember_token' => Str::random(60),
                'role_id' => 1,
                'email_verified' => 1,
                'phone_verified' => 1,
            ]);
        }
    }
}
