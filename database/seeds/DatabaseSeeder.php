<?php

use App\Consumer;
use Carbon\Carbon;
use App\CampaignPage;
use App\FreebieSurvey;
use App\FeedbackSurvey;
use App\MatchingTagData;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\InboundInventory;
use App\InventoryLog;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // $faker = new Faker();

        $this->call(DefaultSurveySeeder::class);
        $this->call(OnboardSurveySeeder::class);
        //$this->call(DummySeeder::class);
        $this->call(ShippingZoneSeeder::class);
        $this->call(ShippingLocationSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(ApiClientSeeder::class);
        $this->call(UsersTableSeeder::class);
        factory(App\User::class, 10)->create()->each(function ($user) use ($faker) {
            if ($user->role_id == 2) {
                $user->brand()->save(factory(App\Brand::class)->make());
            } elseif ($user->role_id == 3) {
                $user->consumer()->save(factory(App\Consumer::class)->make());
                $user->shipping_address()->save(factory(App\ShippingAddress::class)->make([
                    'phone' => $user->phone,
                    'email' => $user->email,
                ]));
            }
        });

        factory(App\Category::class, 10)->create();
        factory(App\BannerAd::class, 7)->create();

        factory(App\Campaign::class, 10)->create()->each(function($campaign) use($faker){
                $matching_data = MatchingTagData::inRandomOrder()->limit(50)->get();
                $tag_data = [];
                foreach($matching_data as $data){
                    $tag_data[] = [
                        'campaign_id'=> $campaign->id,
                        'matching_tag_id'=> $data->matching_tag_id,
                        'matching_data_id'=> $data->id,
                    ];
                }
                CampaignMatchingData::insert($tag_data);

                $campaign->page()->save(new CampaignPage([
                    'data'=> $this->setPageData($faker)
                ]));

                //Match first consumer to five campaigns
                if($campaign->id <= 8){
                    $consumerData = [];
                    foreach($tag_data as $cData){
                        $consumerData[] = [
                            'consumer_id'=> 1,
                            'matching_tag_id'=> $cData['matching_tag_id'],
                            'matching_data_id'=> $cData['matching_data_id'],
                        ];
                    }
                    ConsumerMatchingData::insert($consumerData);
                }

                $campaign->inbound_inventories()->save(new InboundInventory([
                    'brand_id'=> $campaign->brand_id,
                    'name'=> $campaign->title,
                    'quantity'=> $faker->numberBetween(5, 20),
                    'in_stock'=> $faker->numberBetween(1, 10),
                    'photo'=> $campaign->photo,
                ]));
        });

        //factory(App\ShippingLocation::class, 50)->create();
        //factory(App\OnboardingSurvey::class, 1)->create();

        factory(App\Freebie::class, 50)->create();

        //factory(App\DailySurvey::class, 50)->create();

        //factory(App\ConsumerFreeby::class, 20)->create();
        //factory(App\InboundInventory::class, 50)->create();
        //factory(App\OutboundInventory::class, 100)->create();
        //factory(App\InventoryLog::class, 50)->create();

    }

    public function setPageData(Faker $faker){
        $page = [];
        $page['product_name'] = $faker->sentence();
        $page['short_description'] = $faker->text(500);
        $page['long_description_title'] = $faker->sentence();
        $page['long_description_content'] = $faker->text(1000);
        $page['features']['title'] = $faker->sentence();
        $page['features']['feature_list'] = [];
        for($n=1; $n <= 10; $n++){
            $page['features']['feature_list'][] = $faker->text(10);
        }

        $page['info_title'] = $faker->sentence();
        $page['info_content'] = $faker->text(700);
        $page['page_pictures'] = [];
        for($n=1; $n <= 6; $n++){
            $page['page_pictures'][] = 'https://hazina-dev.s3.us-east-2.amazonaws.com/stub/photo/products/'. $n .'.png';
        }
       return  json_encode($page);

    }
}


