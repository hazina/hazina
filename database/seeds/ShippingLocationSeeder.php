<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            "Abia",
            "Adamawa",
            "Anambra",
            "Akwa Ibom",
            "Bauchi",
            "Bayelsa",
            "Benue",
            "Borno",
            "Cross River",
            "Delta",
            "Ebonyi",
            "Enugu",
            "Edo",
            "Ekiti",
            "FCT - Abuja",
            "Gombe",
            "Imo",
            "Jigawa",
            "Kaduna",
            "Kano",
            "Katsina",
            "Kebbi",
            "Kogi",
            "Kwara",
            "Lagos",
            "Nasarawa",
            "Niger",
            "Ogun",
            "Ondo",
            "Osun",
            "Oyo",
            "Plateau",
            "Rivers",
            "Sokoto",
            "Taraba",
            "Yobe",
            "Zamfara"
        ];

        $locations = [];

        foreach($states as $state){
            $locations[] = [
                'name'=> $state,
                'description'=> '',
               // 'price'=> 0,
                'shipping_zone_id'=> 2
            ];
        }
        $pickUp = [
            'name'=> 'Pick Up at HQ',
            'description'=> '36 ayodele okeowo, Gbagada Ifako Lagos, Nigeria',
            //'price'=> 0,
            'shipping_zone_id'=> 1
        ];
    //    $ff =  array_push($locations, $pickUp);
       DB::table('shipping_locations')->insert($pickUp);
       DB::table('shipping_locations')->insert($locations);
    }
}
