<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('title')->nullable();
            $table->string('success_message')->nullable();
            $table->longText('survey')->nullable();
            $table->string('photo')->nullable();
            $table->integer('number_of_questions')->default();
            //$table->bigInteger('points')->default(0);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surveys');
    }
}
