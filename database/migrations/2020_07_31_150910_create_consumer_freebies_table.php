<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConsumerFreebiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumer_freebies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('consumer_id')->unsigned();
            $table->bigInteger('freebie_id')->unsigned();
            $table->text('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consumer_freebies');
    }
}
