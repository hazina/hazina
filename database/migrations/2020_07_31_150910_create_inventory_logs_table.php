<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoryLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('inbound_inventory_id')->unsigned();
            $table->bigInteger('outbound_inventory_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
           // $table->integer('quantity')->nullable();
            $table->timestamp('date')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventory_logs');
    }
}
