<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFreebiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freebies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->double('amount')->default();
            $table->string('type');
            $table->text('template')->nullable();
            $table->text('level')->nullable();
            $table->integer('points')->default();
            $table->integer('quantity')->default();
            $table->string('photo')->nullable();
            $table->timestamp('starts_on')->nullable();
            $table->timestamp('ends_on')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('freebies');
    }
}
