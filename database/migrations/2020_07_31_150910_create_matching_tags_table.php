<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchingTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matching_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('survey_id')->nullable();
            $table->string('name')->nullable();
            $table->string('ref')->nullable();
            $table->string('title')->nullable();
            $table->boolean('is_global')->default(false);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matching_tags');
    }
}
