<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedbackSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('campaign_id')->unsigned();
            $table->bigInteger('survey_id')->unsigned();
            $table->bigInteger('points')->default(0);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feedback_surveys');
    }
}
