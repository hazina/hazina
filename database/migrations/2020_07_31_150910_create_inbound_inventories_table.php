<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInboundInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbound_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('brand_id')->unsigned();
            $table->bigInteger('campaign_id')->unsigned();
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->bigInteger('quantity')->default(0);
            $table->boolean('in_stock')->nullable();
            $table->boolean('claimed')->nullable();
            $table->boolean('dispatched')->nullable();
            $table->boolean('awaiting_goods')->nullable();
            $table->longText('photo')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inbound_inventories');
    }
}
