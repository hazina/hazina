<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOutboundInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbound_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('consumer_id')->unsigned();
            $table->bigInteger('shipping_address_id')->unsigned();
            //$table->integer('number_of_samples')->nullable();
            //$table->longText('products_in_box')->nullable();
            $table->timestamp('last_sample_added_on')->nullable();
            $table->string('status')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outbound_inventories');
    }
}
