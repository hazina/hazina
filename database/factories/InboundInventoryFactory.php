<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InboundInventory;
use Faker\Generator as Faker;

$factory->define(InboundInventory::class, function (Faker $faker) {
    return [
        'brand_id'=> function(){
            return App\Brand::inRandomOrder()->first()->id;
        },
        'campaign_id'=> function(){
            return App\Brand::inRandomOrder()->first()->id;
        },
        'name'=> $faker->text(55),
        'description'=> $faker->text(100),
        'in_stock'=> $faker->boolean(70),
        'claimed'=> $faker->boolean(20),
        'dispatched'=> $faker->boolean(20),
        'awaiting_goods'=> $faker->boolean(20),
        'photo'=> 'stub/photo/campaign/' . $faker->numberBetween(1,5) . '.jpg',
    ];
});
