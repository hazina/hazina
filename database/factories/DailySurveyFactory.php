<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DailySurvey;
use Faker\Generator as Faker;

$factory->define(DailySurvey::class, function (Faker $faker) {
    return [
        'survey_id'=> function(){
            return \App\Survey::where("id", ">", 2)->first()->id;
        },
        'level'=> $faker->numberBetween(1,200),
        'points'=> $faker->numberBetween(10, 50)
    ];
});
