<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Brand;
use Faker\Generator as Faker;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name'=> $faker->company,
        'description'=> $faker->text(100),
        'photo'=> 'stub/photo/company/' . $faker->numberBetween(1,5) . '.png',
    ];
});
