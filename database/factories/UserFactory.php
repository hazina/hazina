<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $phone = $faker->boolean(60);
    $email = $faker->boolean(60);
    return [
        'ref' => Str::random(10),
        'username' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => $email ? now() : null,
        'phone_verified_at' => $phone ? now() : null,
        'email_verified' => $email,
        'phone_verified' => $phone,
        'role_id'=> $faker->numberBetween(2, 3),
        'password' => bcrypt('password'), // password
        //'remember_token' => Str::random(10),
    ];
});
