<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Consumer;
use Faker\Generator as Faker;

$factory->define(Consumer::class, function (Faker $faker) {
    return [
        'api_client_id'=> function(){
            return \App\ApiClient::inRandomOrder()->first()->id;
        },
        'firstname'=> $faker->firstName(),
        'lastname'=> $faker->lastName,
        'points'=> $faker->numberBetween(0, 1000),
        //'photo'=> 'stub/photo/user/user.png',
        'registration_status'=> json_encode($faker->randomElements(['BASIC', 'EMAIL', 'SURVEY'])),
    ];
});
