<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Freebie;
use Faker\Generator as Faker;

$factory->define(Freebie::class, function (Faker $faker) {
    $templates = [
        'airtime',
        'cash',
        'coupons',
    ];

    $type = $faker->randomElement($templates);

    return [
        'amount'=> $faker->numberBetween(1000, 20000),
        'type'=> $type,
        'template'=> $type,
        'quantity'=> $faker->numberBetween(100, 500),
        'points'=> $faker->numberBetween(100, 500),
        'level'=> $faker->numberBetween(1, 50)
    ];
});
