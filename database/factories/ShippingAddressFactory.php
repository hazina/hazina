<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ShippingAddress;
use App\ShippingLocation;
use Faker\Generator as Faker;

$factory->define(ShippingAddress::class, function (Faker $faker) {
   // $user = \App\User::inRandomOrder()->first();

    return [
        // 'user_id'=> function()use($user){
        //     return $user->id;
        // },
        'shipping_location_id'=> function(){
            return ShippingLocation::inRandomOrder()->first()->id;
        },
        'firstname'=> $faker->firstName(),
        'lastname'=> $faker->lastName,
        'address'=> $faker->address,
        // 'phone'=> $user->phone,
        // 'email'=> $user->email,
    ];
});
