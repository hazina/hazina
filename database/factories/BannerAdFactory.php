<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BannerAd;
use Faker\Generator as Faker;

$factory->define(BannerAd::class, function (Faker $faker) {
    return [
        'title'=> $faker->realText(50),
        'photo'=> 'stub/photo/campaign/' . $faker->numberBetween(1,2) . '.jpg',
    ];
});
