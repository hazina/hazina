<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Campaign;
use Faker\Generator as Faker;

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'brand_id'=> function(){
            return App\Brand::inRandomOrder()->first()->id;
        },
        'category_id'=> function(){
            return App\Category::inRandomOrder()->first()->id;
        },
        'title'=> $faker->text(100),
        'description'=> $faker->sentence(),
        'photo'=> 'stub/photo/campaign/' . $faker->numberBetween(1,5) . '.jpg',
        'starts_on'=> $faker->dateTimeBetween('-2 days', '+30 days'),
        'ends_on'=> $faker->dateTimeBetween('+31 days', '+60 days'),
        'status'=> $faker->randomElement(['PENDING', 'ENDED', 'OPEN']),
    ];
});
