<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OnboardingSurvey;
use Faker\Generator as Faker;

$factory->define(OnboardingSurvey::class, function (Faker $faker) {
    return [
        'survey_id'=> function(){
            return \App\Survey::first()->id;
        }
    ];
});
