<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OutboundInventory;
use Faker\Generator as Faker;


$factory->define(OutboundInventory::class, function (Faker $faker) {
    return [
        'consumer_id'=> function(){
            return App\Consumer::inRandomOrder()->first()->id;
        },
        'campaign_id'=> function(){
            return App\Campaign::inRandomOrder()->first()->id;
        },
        'shipping_address_id'=> function(){
            return App\ShippingAddress::inRandomOrder()->first()->id;
        },
        //'number_of_samples'=> $faker->numberBetween(5, 30),
        //'products_in_box'=> json_encode(['MILO', 'MAGGI', 'KOKO GARRI', 'GEGEMU']),
        'last_sample_added_on'=> $faker->dateTimeBetween('-30 days', '-5 days'),
        'created_at'=> $faker->dateTimeBetween('-12 month', '-5 days'),
        'status'=> $faker->randomElement(['CLAIMED', 'QUEUED', 'DISPATCHED']),
    ];
});
