<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ConsumerFreeby;
use Faker\Generator as Faker;

$factory->define(ConsumerFreeby::class, function (Faker $faker) {
    return [
        'consumer_id'=> function(){
            return App\Consumer::inRandomOrder()->first()->id;
        },
        'freebie_id'=> function(){
            return App\Freebie::inRandomOrder()->first()->id;
        },
        'status'=> $faker->randomElement(['APPROVED', 'REJECTED'])
    ];
});
