<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InventoryLog;
use Faker\Generator as Faker;

$factory->define(InventoryLog::class, function (Faker $faker) {
    return [
        'inbound_inventory_id'=> function(){
            return App\InboundInventory::inRandomOrder()->first()->id;
        },
        'outbound_inventory_id'=> function(){
            return App\OutboundInventory::inRandomOrder()->first()->id;
        },
        'category_id'=> function(){
            return App\Category::inRandomOrder()->first()->id;
        },
        //'quantity'=> $faker->numberBetween(1, 100),
        'date'=> $faker->dateTimeBetween('-20 days', '+40 days'),
        //'status'=> $faker->randomElement(['DISPATCHED', 'CLAIMED'])
    ];
});
