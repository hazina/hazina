@component('mail::message')
# Hi! {{ $user->name }}

Follow the link below to reset your password

{{-- @component('mail::button', ['url' => url('/reset-password', $token)]) --}}
@component('mail::button', ['url' => config('site.front_url') . '/reset-password/' . $token ])
Reset Password
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
