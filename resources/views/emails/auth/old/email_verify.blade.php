@component('mail::message')
# Welcome!

Follow the link below to verify your email

@component('mail::button', ['url' => config('site.front_url') . '/account/verify/' . $email_token ])
Verify Email
@endcomponent

Regards,<br>
{{ config('app.name') }} Team
@endcomponent
