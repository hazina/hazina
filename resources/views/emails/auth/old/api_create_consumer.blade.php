@component('mail::message')
# Welcome

Follow the link below to complete your registration.

@component('mail::button', ['url' => config('site.front_url') . '/welcome/account/setup/' . $user->id])
Complete Setup
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
