@component('mail::message')
# Hi!

**{{ Str::title($user->name) }}** just claimed **{{ Str::title($product->name) }}**

@component('mail::button', ['url' => '/admin'])
Visit admin
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
