@component('mail::message')
# Hi {{ Str::title($user->name) }}!
@php $name = Str::title($product->name) ; $price = number_format($product->price, 2); @endphp
You have successfully claimed {{ $product->claim_type == 'CASH' ? "the cash equivalent of **$name**, and your account has been credited with **$price**NGN!" : "**$name** and it is being shipped to the address you provided!" }}

@component('mail::button', ['url' => ''])
See Claimed Products
@endcomponent

Regards,<br>
{{ config('app.name') }} Team
@endcomponent
