@component('mail::message')
# Hi {{ $user->name }}!

Follow the link below to verify your new email address

@component('mail::button', ['url' => config('app.url') . '/user/verify-email/' . $token])
Verify Email
@endcomponent

Regards,<br>
{{ config('app.name') }}
@endcomponent
