@component('mail::message')
# Hi there!

It's time to share your freebie experience! What are your thoughts on the  freebies you got. We will like to know, your feedback is important to us.

@component('mail::button', ['url' => 'https://consumer.gethazina.com'])
Share Your Thought
@endcomponent

Thanks,<br>
Hazina Team
@endcomponent
