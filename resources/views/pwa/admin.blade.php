<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <base href="{{ route('admin.home') }}" />

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon  -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('images/favicons/site.webmanifest')}}">
    <link rel="mask-icon" href="{{ asset('images/favicons/safari-pinned-tab.svg')}}" color="#5bbad5">
    <link rel="shortcut icon" href="{{ asset('images/favicons/favicon.ico')}}">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="msapplication-config" content="{{ asset('images/favicons/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- Scripts -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,700&display=swap"
        rel="stylesheet">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/designer.css') }}" rel="stylesheet"> --}}
</head>

<body>
    <noscript>
        <h1>Your browser does not support Javascript, Hazina runs heavily on javascript. Please enable javascript to start using hazina! </h1>
     </noscript>
    <div id="__haziapp__">
        <app></app>
    </div>
    <script src="{{ mix('js/admin.js') }}" defer></script>
</body>

</html>
