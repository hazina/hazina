@extends('layouts.offer')

@section('title', '')
@php
$page = collect(json_decode($campaign->page->data, true));
@endphp
@section('content')
<!-- ======= Hero Section ======= -->
@php
function resolveImage($path){
$path = Storage::url($path);
return $path;
}

@endphp
<!-- ======= Hero Section ======= -->
<section id="hero">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

            <!-- Slide 1 -->
            @foreach($page->get('headlines', []) as $headline)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}"
                style="background-image: url({{ resolveImage($page['uploads']['banner']) }})">
                <div class="carousel-container">
                    <div class="container">
                        <h2 class="animated fadeInDown">{{ $headline['headline'] }}</span></h2>
                        <p class="animated fadeInUp">{{ $headline['tagline'] }}</p>
                        <a href="" class="btn-get-started animated fadeInUp">Accept Offer</a>
                    </div>
                </div>
            </div>
            @endforeach

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
</section><!-- End Hero -->


<main id="main">

    <!-- ======= About Section ======= -->
    <section id="story" class="about">
        <div class="container-fluid">

            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 d-flex justify-content-center align-items-stretch">
                    {{-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a> --}}
                    <img src="{{ asset('images/product_story.svg') }}" alt="Hazina" width="500">
                </div>

                <div
                    class="col-xl-5 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                    <h3>Product Story</h3>
                    {{-- <p>Esse voluptas cumque vel exercitationem. Reiciendis est hic accusamus. Non ipsam et sed minima temporibus laudantium. Soluta voluptate sed facere corporis dolores excepturi. Libero laboriosam sint et id nulla tenetur. Suscipit aut voluptate.</p> --}}
                    @foreach($page->get('product_story', []) as $story)
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-chevron-right"></i></div>
                        {{-- <h4 class="title"><a href="">Lorem Ipsum</a></h4> --}}
                        <p class="description">{{ $story }}</p>
                    </div>
                    @endforeach

                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <section id="features" class="services">
        <div class="container-fluid">

            <div class="section-title">
                <h2>Fetaures</h2>
                <h3>Campaign <span>Features</span></h3>
                {{-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p> --}}
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="row">
                        @foreach($page->get('features', []) as $feature)
                        <div class="col-lg-4 col-md-6 icon-box">
                            <div class="icon"><i class="ri-pie-chart-line"></i></div>
                            <h4 class="title"><a href="">{{ $feature['title'] }}</a></h4>
                            <p class="description">{{ $feature['description'] }}</p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="gallery" class="portfolio">
        <div class="container-fluid">

            <div class="section-title">
                <h2>Products</h2>
                <h3>Campaign <span>Products</span></h3>
                {{-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque
                    vitae autem.</p> --}}
            </div>

            {{-- <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">App</li>
                        <li data-filter=".filter-card">Card</li>
                        <li data-filter=".filter-web">Web</li>
                    </ul>
                </div>
            </div> --}}

            <div class="row portfolio-container justify-content-center">

                <div class="col-xl-10">
                    <div class="row">
                        @if(isset($page['uploads']['products']))
                        @foreach($page['uploads']['products'] as $product)
                        <div class="col-xl-3 col-lg-4 col-md-6 portfolio-item filter-app">
                            <div class="portfolio-wrap">
                                <img src="{{ resolveImage($product) }}" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4>Product {{ $loop->index + 1 }}</h4>
                                    {{-- <p>App</p> --}}
                                    <div class="portfolio-links">
                                        <a href="{{ resolveImage($product) }}" data-gall="portfolioGallery"
                                            class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                                        {{-- <a href="portfolio-details.html" title="More Details"><i
                                                class="bx bx-link"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                        </div><!-- End portfolio item -->
                        @endforeach
                        @endif
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials section-bg">
        <div class="container-fluid">

            <div class="section-title">
                <h2>Testimonials</h2>
                <h3>What They <span>Are Saying</span> About This Product</h3>
                {{-- <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p> --}}
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10">

                    <div class="row">
                        @foreach($page->get('testimonials') as $testimonial)
                        <div class="col-lg-6 mb-3">
                            <div class="testimonial-item">
                                <img src="{{ asset('images/default.png') }}" class="testimonial-img" alt="">
                                <h3>{{ $testimonial['customer_details'] }}</h3>
                                <h4>{{ $testimonial['title'] }}</h4>
                                <p>
                                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    {{ $testimonial['testimonials'] }}
                                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                            </div>
                        </div><!-- End testimonial-item -->
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Testimonials Section -->



</main>

@endsection
