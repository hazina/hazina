<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerDailySurvey extends Model
{

    protected $fillable = ['daily_survey_id'];

    public function daily_survey(){
        return $this->belongsTo('App\DailySurvey');
    }

    public function consumer(){
        return $this->belongsTo('App\Consumer');
    }
}
