<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingZone extends Model
{
    public function shipping_locations(){
        return $this->hasMany('App\ShippingLocation');
    }
}
