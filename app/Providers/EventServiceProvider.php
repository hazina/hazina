<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Account\Created' => [
            'App\Listeners\Account\SendSms',
            'App\Listeners\Account\SendEmail',
            'App\Listeners\Account\NotifyAdmin',
        ],
        'App\Events\ApiClient\Created' => [
            'App\Listeners\ApiClient\SendSms',
            'App\Listeners\ApiClient\SendEmail',
            'App\Listeners\ApiClient\NotifyAdmin',
        ],
        'App\Events\ApiClientVerifiedEmail\Created' => [
            'App\Listeners\ApiClientVerifiedEmail\SendEmail'
        ],
        'App\Events\Onboard\Notify' => [
            'App\Listeners\Onboard\NotifyConsumer'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
