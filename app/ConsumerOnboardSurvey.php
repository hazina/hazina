<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerOnboardSurvey extends Model
{
    protected $fillable = ["survey_id", "onboarding_survey_id"];

    public function onboarding_survey(){
        return $this->belongsTo('App\OnboardingSurvey');
    }

}
