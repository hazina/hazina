<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerFeedbackSurvey extends Model
{
    protected $fillable = ["feedback_survey_id"];

    public function feedback_survey(){
        return $this->belongsTo('App\FeedbackSurvey');
    }

    public function consumer(){
        return $this->belongsTo('App\Consumer');
    }

}
