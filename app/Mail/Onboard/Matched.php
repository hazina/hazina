<?php

namespace App\Mail\Onboard;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Matched extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $user;
     public $campaigns;

    public function __construct($user, $campaigns)
    {
        $this->user = $user;
        $this->campaigns = $campaigns;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@gethazina.com', config('app.name'))
                ->subject('Welcome to Hazina!')
                ->view('emails.onboarding.matched');
    }
}
