<?php

namespace App\Mail\User;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    public $email_token;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $email_token, $user)
    {
        $this->request = $request;
        $this->email_token = $email_token;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@gethazina.com', 'Hazina')
                ->subject('Please confirm your e-mail address')
                ->markdown('emails.auth.email_verify');
    }
}
