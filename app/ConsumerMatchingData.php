<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerMatchingData extends Model
{
    protected $fillable = ['matching_tag_id', 'matching_data_id'];

    public function consumer(){
        return $this->belongsTo('App\Consumer');
    }

    public function matching_tag(){
        return $this->belongsTo('App\MatchingTag', 'matching_tag_id');
    }

    public function matching_data(){
        return $this->belongsTo('App\MatchingTagData', 'matching_data_id');
    }
}
