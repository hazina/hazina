<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailySurvey extends Model
{
    protected $fillable = ['points', 'survey_id', 'level', 'title', 'photo'];

    public function survey(){
        return $this->belongsTo('App\Survey', 'survey_id');
    }

    public function matching_tags()
    {
        return $this->belongsToMany('App\MatchingTag', 'daily_survey_matching_tags')->withPivot('value');
    }

    public function getPhotoAttribute($value)
    {
        $photos = $value;
        return !$value ? asset("images/bg-survey.jpg") : asset(\Illuminate\Support\Facades\Storage::disk('s3')->url($photos));
    }

}
