<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OutboundInventory extends Model
{

    protected $appends = ['date'];

    protected $fillable = ['shipping_address_id', 'status', 'last_sample_added_on', 'campaign_id'];

    public function campaign(){
        return $this->belongsTo('App\Campaign');
    }

    public function consumer(){
        return $this->belongsTo('App\Consumer');
    }

    public function shipping_address(){
        return $this->hasOne('App\ShippingAddress');
    }

    public function inventory_logs(){
        return $this->hasMany('App\InventoryLog');
    }

    public function getDateAttribute(){
        return Carbon::parse($this->updated_at)->toFormattedDateString();
    }
}
