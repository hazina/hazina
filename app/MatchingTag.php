<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchingTag extends Model
{
    protected $table = 'matching_tags';

    public function matching_data(){
        return $this->hasMany('App\MatchingTagData');
    }
}
