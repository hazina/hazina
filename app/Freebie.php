<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Freebie extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'freebies';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['amount', 'type', 'template', 'level', 'points', 'quantity', 'photo', 'starts_on', 'ends_on'];

    //protected $appends = ['added_on', 'formatted_amount'];

    public function survey()
    {
        return $this->hasOne('App\FreebieSurvey');
    }
    public function matching_tags()
    {
        return $this->belongsToMany('App\MatchingTag', 'freebie_matching_tags');
    }

    // public function getAddedOnAttribute(){
    //     return Carbon::parse($this->created_at)->format('D jS \\of M Y h:i:s A');
    // }

    // public function getFormattedAmountAttribute(){
    //     return number_format($this->amount, 2);
    // }

}
