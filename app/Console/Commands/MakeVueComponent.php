<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeVueComponent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vue:component {name} {portal}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds vue component';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $portal = $this->argument('portal');
        $name_var = '@name';
        $path_var = '@name_path';
        $class_var = '@name_class';

        $path = resource_path('js/' . $portal . '/app//');

        $format_name = Str::studly($name);
        $stub = base_path('/stubs/vue/component');
        if (!file_exists($path . 'components/' . $format_name)) {
            mkdir($path . 'components/' . $format_name);
        }

        file_put_contents($path . 'components/' . $format_name . '/' . $format_name . '.vue', str_replace($name_var, $format_name, file_get_contents($stub . '/template.stub')));
        file_put_contents($path . 'components/' . $format_name . '/' . $format_name . '.vue.ts', str_replace($class_var, $format_name, file_get_contents($stub . '/controller.stub')));
        //file_put_contents($path . 'views/' . $format_name . '.vue', str_replace($path_var, $format_name, file_get_contents($stub . '/view.stub')));
    }
}
