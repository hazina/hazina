<?php
namespace App\Applets;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use Artesaos\SEOTools\Facades\JsonLdMulti;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\URL;

class Seo{

    protected $data = [];

    protected $title = 'Discover freebies from favorite brands';

    protected $description = 'Digital platform for brands to deliver product samples to the right consumers, and gain insights through simplified feedback collection.';

    public function __construct($data = [])
    {
        $this->data = collect($data);
    }

    public function setSeo(){



        SEOMeta::setTitle('Hazina - ' . $this->data->get('title', $this->title));
        SEOMeta::setDescription($this->data->get('description', $this->description));
        SEOMeta::addKeyword($this->data->get('keywords', []));

        OpenGraph::setDescription($this->data->get('description', $this->description));
        OpenGraph::setTitle('Hazina - ' . $this->data->get('title', $this->title));
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(asset('images/seo.png'));
        OpenGraph::setSiteName('Hazina');

        TwitterCard::setTitle('Hazina - ' . $this->data->get('title', $this->title));
        TwitterCard::setSite('@gethazina');
        TwitterCard::setImage(asset('images/seo.png'));
        TwitterCard::setUrl(URL::current());

        JsonLd::setTitle('Hazina - ' . $this->data->get('title', $this->title));
        JsonLd::setDescription($this->data->get('description', $this->description));
        JsonLd::addImage(asset('images/seo.png'));

    }
}
