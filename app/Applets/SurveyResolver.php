<?php
namespace App\Applets;

use App\MatchingTag;
use App\MatchingTagData;
use Illuminate\Support\Str;

class SurveyResolver
{

    public $survey;

    public function __construct($survey)
    {
        $this->survey = json_decode($survey, true);
    }

    public function rebuild_survey()
    {
        $all_survey_questions = $this->survey;
        $modified_survey = [];
        $Built = [];
        foreach ($all_survey_questions['pages'][0]['elements'] as $all_survey_question) {
           // if(!isset($all_survey_question['ref'])){
                $all_survey_question['ref'] = (string) Str::uuid();
                // $all_survey_question['value_name'] = isset($all_survey_question['valueName']) ? $all_survey_question['valueName'] : " ";
           // }

            if ($all_survey_question['type'] === 'checkbox' || $all_survey_question['type'] === 'radiogroup' || $all_survey_question['type'] === 'dropdown' || $all_survey_question['type'] === 'imagepicker') {
                $choices = [];
                //$all_survey_question['hasNone'] = (string) Str::uuid();
                foreach ($all_survey_question['choices'] as $choice) {
                    if(!$this->isUuid($choice['value'])){
                        $choice['value'] = (string) Str::uuid();
                        $choices[] = $choice;
                    }else{
                        $choices[] = $choice;
                    }
                }
                $all_survey_question['choices'] = $choices;
            } elseif ($all_survey_question['type'] === 'rating') {
                $values = [];
                foreach ($all_survey_question['rateValues'] as $choice) {
                    if(!$this->isUuid($choice['value'])){
                    $choice['value'] = (string) Str::uuid();
                    $values[] = $choice;
                    }else{
                        $choices[] = $choice;
                    }
                }
                $all_survey_question['rateValues'] = $values;
            } elseif ($all_survey_question['type'] === 'boolean') {
                if(!$this->isUuid($all_survey_question['valueTrue']) && !$this->isUuid($all_survey_question['valueFalse'])){
                    $all_survey_question['valueTrue'] = (string) Str::uuid();
                    $all_survey_question['valueFalse'] = (string) Str::uuid();
                }

            }
            $modified_survey[] = $all_survey_question;
        }
        $Built = $modified_survey;
        unset($all_survey_questions['pages'][0]['elements']);
        $all_survey_questions['pages'][0]['elements'] = $Built;
        return $all_survey_questions;
    }

    public function build_tags()
    {
        $raw_survey = $this->rebuild_survey();
        $matching_tags = [];
        foreach ($raw_survey['pages'][0]['elements'] as $all_survey_question) {
            if ($all_survey_question['type'] === 'checkbox' || $all_survey_question['type'] === 'radiogroup' || $all_survey_question['type'] === 'dropdown' || $all_survey_question['type'] === 'imagepicker') {
                $dd = [];
                $dd['tag'] = $all_survey_question['name'];
                $dd['title'] = $all_survey_question['title'];
                $dd['display'] = $all_survey_question['valueName'];
                //$dd['ref'] = $all_survey_question['ref'];
                $ll = [];
                foreach ($all_survey_question['choices'] as $choice) {
                    $ll[] = [
                        'value' => $choice['value'],
                        'title' => $choice['text'],
                       // 'ref' => $choice['ref'],
                        'matching_tag_id' => '',
                    ];
                }
                if (isset($all_survey_question['hasNone'])) {
                    if($all_survey_question['hasNone'] == true){
                        $ll[] = [
                            'value' => 'none',
                          //  'ref' => $all_survey_question['has_none_ref'],
                            'title' => $all_survey_question['noneText'],
                            'matching_tag_id' => '',
                        ];
                    }
                }

                $dd['data'] = $ll;
                $matching_tags[] = $dd;

            } elseif ($all_survey_question['type'] === 'rating') {
                $dd = [];
                $dd['tag'] = $all_survey_question['name'];
                $dd['title'] = $all_survey_question['title'];
                $dd['ref'] = $all_survey_question['ref'];
                $dd['display'] = $all_survey_question['valueName'];
                $ll = [];
                foreach ($all_survey_question['rateValues'] as $choice) {
                    $ll[] = [
                        'value' => $choice['value'],
                        'title' => $choice['text'],
                       // 'ref' => $choice['ref'],
                        'matching_tag_id' => '',
                    ];
                }
                $dd['data'] = $ll;
                $matching_tags[] = $dd;
            } elseif ($all_survey_question['type'] === 'boolean') {
                $dd = [];
                $dd['tag'] = $all_survey_question['name'];
                $dd['title'] = $all_survey_question['title'];
                $dd['ref'] = $all_survey_question['ref'];
                $dd['display'] = $all_survey_question['valueName'];
                $ll = [
                    [
                        'value' => $all_survey_question['valueTrue'],
                        'title' => $all_survey_question['valueName'],
                        //'ref' => $all_survey_question['true_ref'],
                        'matching_tag_id' => '',
                    ],
                    [
                        'value' => $all_survey_question['valueFalse'],
                        'title' => $all_survey_question['valueName'],
                        //'ref' => $all_survey_question['false_ref'],
                        'matching_tag_id' => '',
                    ],
                ];
                $dd['data'] = $ll;
                $matching_tags[] = $dd;
            }
        }
        return $matching_tags;
    }

    public function save_tags($survey){
        $matching_tags = $this->build_tags();
        foreach($matching_tags as $matching_tag){
            logger($matching_tag);
            $tagging = new MatchingTag();
            $tagging->name = $matching_tag['tag'];
            $tagging->title = $matching_tag['title'];
            $tagging->display = $matching_tag['display'];
           // $tagging->ref = $matching_tag['ref'];
            $tagging->survey_id = $survey->id;
            $tagging->is_global = false;
            $tagging->save();
            $tag_data = collect($matching_tag['data'])->map(function($value, $index)use($tagging){
                        $value['matching_tag_id'] = $tagging->id;
                        return $value;
            });
            MatchingTagData::insert($tag_data->toArray());
        }
    }


    public function update_tags($survey){
        $matching_tags = $this->build_tags();
        foreach($matching_tags as $matching_tag){
            $tagging = MatchingTag::where('survey_id', $survey->id)->first();
            $tagging->name = $matching_tag['tag'];
            $tagging->title = $matching_tag['title'];
            $tagging->display = $matching_tag['display'];
           // $tagging->ref = !$tagging->ref ? $matching_tag['ref'] : $tagging->ref;
            $tagging->survey_id = $survey->id;
            $tagging->is_global = false;
            $tagging->save();
            $tag_data = collect($matching_tag['data'])->map(function($value, $index)use($tagging){
                        $value['matching_tag_id'] = $tagging->id;
                        return $value;
            })->all();
          //logger($tag_data);
            foreach($tag_data as $tag_data2){
                MatchingTagData::updateOrCreate(
                   ['value'=> $tag_data2['value'], 'matching_tag_id'=> $tag_data2['matching_tag_id']],
                   [
                       'value'=> $tag_data2['value'],
                       'title'=> $tag_data2['title'],
                       'matching_tag_id'=> $tag_data2['matching_tag_id'],
                   ]
                );
            }
        }
    }

    public function saveOrCreateTag($tag_data){
        $matching_tag = MatchingTagData::where([['value', $tag_data['value']], ['matching_tag_id', $tag_data['matching_tag_id']]])->first();
        if($matching_tag){
            $matching_tag->value = $tag_data['value'];
            $matching_tag->title = $tag_data['title'];
            $matching_tag->display = $matching_tag['display'];
            $matching_tag->save();
        }
    }

    public function isUuid($value){
        return \Ramsey\Uuid\Uuid::isValid($value);
    }



}
