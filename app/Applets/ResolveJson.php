<?php
namespace App\Applets;

class ResolveJson{

    public static function decodeFile(string $filePath): array{
        return json_decode(file_get_contents($filePath), true);
    }


}
