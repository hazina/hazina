<?php
namespace App\Applets;

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class ZappierSms
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function send()
    {
        $client = new Client();

        $code = $this->generatePIN(6);
        $code_sms = $this->user->id . '-' . $code;
        // $email = "michael@hazina.ng";
        // $password = "@Naija1960";
        $urlFollow = url('/leads/welcome', $code_sms);
        //$urlFollow = 'https://gethazina.com/account/leads/complete/' . $code;
        $message = "Welcome! click $urlFollow to claim your freebie";
        $sender_name = "Hazina";
        $recipients = $this->user->phone;
        //$forcednd = "set to 1 if you want DND numbers to ";

        $response = $client->post('https://api.bulksmslive.com/v2/app/sendsms', [
            'form_params' => [
                // 'email'=> $email,
                // 'password'=> $password,
                'message' => $message,
                'sender_name' => $sender_name,
                'recipients' => $recipients,
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer RY9vExzWbz6PgC1xCHnTqxqQJql6OubARHJ2YEOJBKJPCOdhbHCrD4ieZgkr',
            ],
        ]);
        // $response->get
        if ($response->getStatusCode() == 200) {
            //save pin to db with expiry
            DB::table('phone_verify')->insert([
                'phone' => $this->user->phone,
                'code' => $code,
                'expiry' => Carbon::now()->addMinutes(30),
            ]);
        }
    }

    public function generatePIN($digits = 4)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(1, 9);
            $i++;
        }
        return $pin;
    }

}
