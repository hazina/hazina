<?php
namespace App\Applets;

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class SendSms
{

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function send()
    {
        $client = new Client();

        $code = $this->generatePIN(6);
        // $email = "michael@hazina.ng";
        // $password = "@Naija1960";
        $alt_code = Str::random(10);
        $link = "https://gethazina.com/phone-code/verify/" . $alt_code;
        $message = "Your Hazina authentication code is $code. You can also click the link below to verify $link";
        $sender_name = "Hazina";
        $recipients = $this->user->phone;
        //$forcednd = "set to 1 if you want DND numbers to ";

        $response = $client->post('https://termii.com/api/sms/send', [
            'form_params' => [
                // 'email'=> $email,
                // 'password'=> $password,
                'sms' => $message,
                'channel' => 'dnd',
                'api_key' => 'TLFwXhakD94uvfEtsprzpMgJzCyQKRDx7XnCbUJFkKlEa12dTOCAMPBlnt7pCZ',
                'from' => 'Hazina',
                'to' => $recipients,
                'type' => 'plain',
            ],
            'headers' => [
                'Accept' => 'application/json',
                // 'Authorization' => 'Bearer RY9vExzWbz6PgC1xCHnTqxqQJql6OubARHJ2YEOJBKJPCOdhbHCrD4ieZgkr',
            ],
        ]);
        // $response->get
        if ($response->getStatusCode() == 200) {
            //save pin to db with expiry
            DB::table('phone_verify')->insert([
                'phone' => $this->user->phone,
                'code' => $code,
                'alt_code' => $alt_code,
                'expiry' => Carbon::now()->addMinutes(2),
            ]);
        }
    }

    public function generatePIN($digits = 4)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(1, 9);
            $i++;
        }
        return $pin;
    }

}
