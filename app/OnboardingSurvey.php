<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OnboardingSurvey extends Model
{

    protected $appends = ['added_on'];

    public function survey(){
        return $this->belongsTo('App\Survey', 'survey_id');
    }

    public function scopeSearch($query, $string)
    {
        if ($string) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', $string . '%');
                //$query->orWhere($column, 'LIKE', '%' . $string . '%');
            }
            return $query;
        }
        return $query;

    }

    public function getAddedOnAttribute()
    {
        return Carbon::parse($this->created_at)->toFormattedDateString();
    }

}
