<?php

namespace App\Http\Middleware;

use App\ApiClient;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CheckApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
           // $requestHost = parse_url($request->headers->get('origin'),  PHP_URL_HOST);
            $get_key = collect(explode(' ', $request->header('Haz-Authorization')));
            //$client = ApiClient::where([['public_key', $get_key->last()], ['host', $requestHost]])->firstOrFail();
            $client = ApiClient::where([['public_key', $get_key->last()]])->firstOrFail();
            $request['api_client'] = $client;
            return $next($request);
        }catch(ModelNotFoundException $e){
            return response()->json(['error' => 'You are not Authorized to perform this action'], 500);
        }
    }
}
