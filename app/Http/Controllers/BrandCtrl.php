<?php

namespace App\Http\Controllers;

use App\User;
use App\Brand;
use App\Survey;
use App\Campaign;
use App\Consumer;
use App\Location;
use Carbon\Carbon;
use App\MatchingTag;
use App\CampaignPage;
use App\FeedbackSurvey;
use App\MatchingTagData;
use App\ShippingAddress;
use App\ConsumerCampaign;
use App\InboundInventory;
use App\OutboundInventory;
use Illuminate\Support\Str;
use App\ConsumerDailySurvey;
use Illuminate\Http\Request;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\ConsumerFeedbackSurvey;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BrandCtrl extends Controller
{
    public function home(Request $request)
    {

        return view('brands.home', []);
    }

    public function manual_match(Request $request)
    {
        if ($request->query('password') && $request->query('password') == "whobe") {
            $consumers = [];
            foreach ($consumers as $consumer) {
                $campaign_id = 0;
                $matching_conds = [];
                $campaign_matchings = CampaignMatchingData::where('campaign_id', $campaign_id)->get();
                foreach ($campaign_matchings as $campaign_matching) {
                    $matching_conds[] = [
                        new ConsumerMatchingData(['matching_tag_id' => $campaign_matching->matching_tag_id, 'matching_data_id' => $campaign_matching->matching_data_id]),
                    ];
                }

                $user = User::where('phone', $consumer['phone'])->first();
                if ($user) {
                    $consumer = Consumer::where('user_id', $user->id)->first();
                    $consumer->matching_data()->saveMany($campaign_matchings);
                }
            }
        }

    }

    public function match_consumers_to_campaigns(Request $request){
        $campaigns = collect([
            [
              "id"=> 52,
              "qty"=> 200,
              "expiry"=> "19/02/2022"
            ],
            [
              "id"=> 51,
              "qty"=> 56,
              "expiry"=> "19/11/2021"
            ],
            [
              "id"=> 49,
              "qty"=> 52,
              "expiry"=> "1/02/2022"
            ],
            [
              "id"=> 46,
              "qty"=> 66,
              "expiry"=> "15/09/2021"
            ],
            [
              "id"=> 50,
              "qty"=> 8,
              "expiry"=> null
            ],
            [
              "id"=> 56,
              "qty"=> 36,
              "expiry"=> "01/05/2025"
            ],
            [
              "id"=> 48,
              "qty"=> 24,
              "expiry"=> "01/06/2023"
            ],
            [
              "id"=> 16,
              "qty"=> 14,
              "expiry"=> "09/03/2022"
            ],
            [
              "id"=> 17,
              "qty"=> 11,
              "expiry"=> "01/01/2023"
            ],
            [
              "id"=> 22,
              "qty"=> 19,
              "expiry"=> "27/09/2022"
            ],
            [
              "id"=> 23,
              "qty"=> 52,
              "expiry"=> "09/11/2021"
            ],
            [
              "id"=> 24,
              "qty"=> 18,
              "expiry"=> "03/02/2022"
            ],
            [
              "id"=> 26,
              "qty"=> 174,
              "expiry"=> "01/12/2021"
            ],
            [
              "id"=> 25,
              "qty"=> 159,
              "expiry"=> "01/03/2022"
            ],
            [
              "id"=> 28,
              "qty"=> 105,
              "expiry"=> "16/08/2022"
            ],
            [
              "id"=> 30,
              "qty"=> 8,
              "expiry"=> "01/01/2024"
            ],
            [
              "id"=> 31,
              "qty"=> 4,
              "expiry"=> "01/03/2024"
            ],
            [
              "id"=> 44,
              "qty"=> 5,
              "expiry"=> "03/04/2023"
            ],
            [
              "id"=> 27,
              "qty"=> 11,
              "expiry"=> "17/07/2021"
            ],
        ]);
        $consumers = Consumer::has('user')->get()->filter(function($value, $key){
            $inb = OutboundInventory::where([['consumer_id', $value->id], ['status', 'QUEUED']])->count();
            return ($inb == 1 || $inb == 2 || $inb == 0) ? true : false;
        });
        $getConditions = CampaignMatchingData::whereIn('campaign_id', $campaigns->pluck('id')->all())->get();
        $matching_conds = [];
        foreach ($getConditions as $campaign_matching) {
            $matching_conds[] = [
                'matching_tag_id' => $campaign_matching->matching_tag_id, 'matching_data_id' => $campaign_matching->matching_data_id
            ];
        }
        $data = [];
    //   foreach($consumers as $consumer){
    //       $consumer->matching_data()->createMany($matching_conds);
    //       $data[] = [
    //         'user_id' => $consumer->id,
    //         'firstname' => $consumer->firstname,
    //         'lastname' => $consumer->lastname,
    //         'email' => $consumer->email,
    //         'phone' => $consumer->phone,
    //         'age' => $consumer->age,
    //         'sex' => $consumer->gender,
    //     ];
    //   }

    //   $csvExporter = new \Laracsv\Export();
    //   $fileName = 'CONSUMERS.csv';
    //   $csvExporter->build(collect($data), ['user_id' => 'ID','email'=> 'Email', 'phone'=> 'Phone', 'firstname' => 'Firstname', 'lastname' => 'Lastname', 'age' => 'Age', 'sex' => 'Gender'])->download($fileName);
    print_r($matching_conds);

    }

    public function checkRange($number, $min, $max, $inclusive = FALSE){
        if (is_int($number) && is_int($min) && is_int($max))
        {
            return $inclusive
                ? ($number >= $min && $number <= $max)
                : ($number > $min && $number < $max) ;
        }

        return FALSE;
    }

    public function setHairs(Request $request)
    {
        // $users = array(
        //     array("campaign" => 33, "phone" => "+2348064165455"),
        //     array("campaign" => 33, "phone" => "+2348033162266"),
        //     array("campaign" => 33, "phone" => "+2348105180797"),
        //     array("campaign" => 33, "phone" => "+2348069094349"),
        //     array("campaign" => 37, "phone" => "+2348038750097"),
        //     array("campaign" => 36, "phone" => "+2348036853667"),
        //     array("campaign" => 33, "phone" => "+2348164441180"),
        //     array("campaign" => 37, "phone" => "+2348061350060"),
        //     array("campaign" => 38, "phone" => "+2348099455600"),
        //     array("campaign" => 37, "phone" => "+2348083979858"),
        //     array("campaign" => 33, "phone" => "+2348123789686"),
        //     array("campaign" => 33, "phone" => "+2348139373163"),
        //     array("campaign" => 37, "phone" => "+2348066275281"),
        // );
        // $selected = [];
        // foreach ($users as $us) {
        //     $campaign_id = $us['campaign'];
        //     $user = User::where('phone', $us['phone'])->first();
        //     if ($user) {
        //         $consumer = Consumer::where('user_id', $user->id)->first();
        //         $shipping = ShippingAddress::where('user_id', $user->id)->first();
        //         $outbound = new OutboundInventory();
        //         $outbound->consumer_id = $consumer->id;
        //         $outbound->status = 'CLAIMED';
        //         $outbound->shipping_address_id = $shipping->id;
        //         $outbound->campaign_id = $campaign_id;
        //         $outbound->save();
        //         InboundInventory::where('campaign_id', $campaign_id)->decrement('quantity', 1);
        //         $selected[] = $consumer->id;

        //     }
        // }

        // echo implode(",", $selected);
    }

    public function setHairs_OLDss(Request $request)
    {
        $consumer_campaigns = ConsumerCampaign::get()->pluck('consumer_id');
        $consumers = Consumer::with(['user'])->whereNotIn('id', $consumer_campaigns)->limit(600)->get();
        $supportedCampaigns = [14, 16, 19, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];

        $shipping_data = [];

        foreach ($consumers as $consumer) {
            $campaigns = Campaign::whereIn('id', $supportedCampaigns)->limit(4)->inRandomOrder()->get();

            $campaign_matchings = CampaignMatchingData::whereIn('campaign_id', $campaigns->pluck('id'))->get();

            // $matchings = $campaign_matchings->transform(function ($item, $key) {
            //     return [
            //         'matching_tag_id' => $item->matching_tag_id, 'matching_data_id' => $item->matching_data_id,
            //     ];
            // });

            $matchings = [];
            foreach ($campaign_matchings as $campaign_matching) {
                $matchings[] = [
                    'matching_tag_id' => $campaign_matching->matching_tag_id, 'matching_data_id' => $campaign_matching->matching_data_id,
                ];
            }

            $outbound = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();

            $diffSet = collect($campaigns->pluck('id'));
            $sortIds = $diffSet->diff($outbound->pluck('id'));

            $ship = ShippingAddress::where('user_id', $consumer->user_id)->first();
            $matchedCampaign = Campaign::whereIn('id', $sortIds->all())->get();
            $already_claimed = Campaign::whereIn('id', $outbound->all())->get();

            $shipping_data[] = [
                'user_id' => $consumer->user_id,
                'firstname' => $consumer->firstname,
                'lastname' => $consumer->lastname,
                'email' => $consumer->user['email'],
                'phone' => $consumer->user['phone'],
                'full_address' => !empty($ship) ? $ship->full_address : "",
                'already_claimed' => $already_claimed->pluck('title')->implode(", "),
                'matched' => $matchedCampaign->pluck('title')->implode(", "),
            ];
            //print_r($matchings);
            $consumer->matching_data()->createMany($matchings);
        }

        $csvExporter = new \Laracsv\Export();
        $frodo = collect($shipping_data);

        $csvExporter->build($frodo, ['user_id' => 'ID', 'firstname' => 'Firstname', 'lastname' => 'Lastname', 'email' => 'Email', 'phone' => 'Phone', 'full_address' => 'Address', 'already_claimed' => 'Already Claimed', 'matched' => 'Newly Matched'])->download("umatchedConsumersAndProducts.csv");

    }

    public function setHairs_odd(Request $request)
    {
        // $campaigns = array(
        //     array("id"=>24,"name"=>"Golden Penny Instant Noodles","qty"=>73),
        //     array("id"=>25,"name"=>"Gino TOMATO - Magic Pepper & Onion Tomato","qty"=>217),
        //     array("id"=>19,"name"=>"Sunvita Choco Crunch 40G","qty"=>61),
        //     array("id"=>23,"name"=>"Nestle Golden Morn","qty"=>138),
        //     array("id"=>28,"name"=>"Mentos Mint Roll","qty"=>196),
        //     array("id"=>27,"name"=>"Munch It Crunchy Snacks - Sweet Surprsie","qty"=>138),
        //     array("id"=>22,"name"=>"Family Custard Vanilla","qty"=>158),
        //     array("id"=>26,"name"=>"Tasty Tom Tomato Mix","qty"=>222),
        //     array("id"=>16,"name"=>"Cadbury 3in1 Chocolate","qty"=>42),
        //     array("id"=>30,"name"=>"Durex Extra Safe","qty"=>2),
        //     array("id"=>31,"name"=>"Durex Performa","qty"=>3),
        //     array("id"=>29,"name"=>"Durex Sensation","qty"=>2),
        //     array("id"=>14,"name"=>"Pin Pop XXX","qty"=>200)
        //   );
        // foreach($campaigns as $campaign){
        //     $camp = Campaign::where('id', $campaign->get('id'))->first();
        //     $camp->status = "OPEN";
        //     $camp->save();
        //     $out = InboundInventory::where('campaign_id', $camp->id)->first();
        //     $out->quantity = $campaign->get('qty');
        //     $out->save();

        // }
    }

    public function campaign_report(Request $request)
    {
        if ($request->query('cam')) {
            $campaign_id = $request->query('cam');
            $campaign = Campaign::where('id', $campaign_id)->first();
            $consumerFeedbacks = ConsumerCampaign::where([['has_feedback', true], ['campaign_id', $campaign_id]])->get();
            $consumers = Consumer::whereIn('id', $consumerFeedbacks->pluck('consumer_id'))->get();
            $campaign_feedbacks = FeedbackSurvey::where('campaign_id', $campaign_id)->first();
            // print_r($consumers);
            if ($campaign_feedbacks) {
                $matchingTags = MatchingTag::with('matching_data')->where('survey_id', $campaign_feedbacks->survey_id)->get();
                if (count($consumers) > 0) {
                    $data = [];
                    foreach ($consumers as $consumer) {
                        $shipping = ShippingAddress::where('user_id', $consumer->user_id)->first();
                        $mar = MatchingTag::where('name', 'marital_status')->first();
                        $marr = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $mar->id)->first();

                        $occ = MatchingTag::where('name', 'employment_status')->first();
                        $occc = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $occ->id)->first();

                        $dat = [
                            'user_id' => $consumer->id,
                            'firstname' => $consumer->firstname,
                            'lastname' => $consumer->lastname,
                            'email' => $consumer->email,
                            'phone' => $consumer->phone,
                            'age' => $consumer->age,
                            'sex' => $consumer->gender,
                            'occupation' => $occc ? $occc->matching_data->title : "",
                            'marital_status' => $marr ? $marr->matching_data->title : "",
                            'lga' => $this->drake($shipping, 'lga'),
                            'area' => $this->drake($shipping, 'area'),
                        ];
                        $res = [];
                        foreach ($matchingTags as $tag) {
                            $cc = ConsumerMatchingData::with('matching_data')->where([['consumer_id', $consumer->id], ['matching_tag_id', $tag->id]])->first();
                            //$tag_data = $tag->matching_data()->first();

                            $res[$tag->title] = $cc ? $cc->matching_data->title : "";
                        }
                        $data[] = array_merge($dat, $res);
                    }

                    $csvExporter = new \Laracsv\Export();
                    $csvExporter->build(collect($data), array_keys(collect($data)->first()))->download(Str::slug($campaign->title) . '_feedbacks.csv');

                } else {
                    echo 'No consumer Feedback found in ' . $campaign->title;
                }
            } else {
                echo 'No feedback survey created for ' . $campaign->title;
            }

        }
    }

    public function setHairsd(Request $request)
    {

        // $users = User::whereIn('phone', $emails)->get();
        // // $users = DB::table('consumer_daily_surveys')->where('daily_survey_id', 2)->get();
        $consumers = [];
        if ($request->query('no-feedback')) {
            $non = ConsumerFeedbackSurvey::get();
            $consumers = Consumer::whereNotIn('user_id', $non->pluck('consumer_id'))->get();
        } elseif ($request->query('no-feedback-nosurvey')) {
            $consumers = Consumer::get()->filter(function ($value, $key) {
                $out = OutboundInventory::where([['consumer_id', $value->id], ['status', 'CLAIMED']])->count();
                $feedback = ConsumerFeedbackSurvey::where('consumer_id', $value->id)->count();
                $daily = ConsumerDailySurvey::where('consumer_id', $value->id)->count();
                $tot = intval($feedback) + intval($daily);
                return $out > 1 && $tot < 1;
            });
        }

        $data = [];
        foreach ($consumers as $consumer) {
            $occ = MatchingTag::where('name', 'employment_status')->first();
            $occc = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $occ->id)->first();

            $mar = MatchingTag::where('name', 'marital_status')->first();
            $marr = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $mar->id)->first();

            $academic = MatchingTag::where('name', 'academic_level')->first();
            $academy = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $academic->id)->first();

            $address = ShippingAddress::where('user_id', $consumer->user_id)->first();
            $location = $address ? Location::where('id', $address->state)->first() : null;

            $getRef = User::where('id', $consumer->user_id)->first();
            $refCount = User::where('referrer', $getRef->ref)->count();
            $data[] = [
                'user_id' => $consumer->user_id,
                'firstname' => $consumer->firstname,
                'lastname' => $consumer->lastname,
                'email' => $consumer->email,
                'phone' => $consumer->phone,
                'age' => $consumer->age,
                'sex' => $consumer->gender,
                'state' => is_null($location) ? "" : $location->name,
                'address' => $address->full_address,
                'points' => $consumer->points,
                'created_on' => Carbon::parse($consumer->created_on)->toFormattedDateString(),
                'occupation' => $occc ? $occc->matching_data->title : "",
                'marital_status' => $marr ? $marr->matching_data->title : "",
                'academic_level' => $marr ? $marr->matching_data->title : "",
                'claimed' => OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'QUEUED']])->count(),
                'recieved' => OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->count(),
                'daily_surveys_taken' => ConsumerDailySurvey::where('consumer_id', $consumer->id)->count(),
                'ref_count' => $refCount,
            ];
        }
        $csvExporter = new \Laracsv\Export();
        $csvExporter->build(collect($data), ['consumer_id' => 'ID', 'email' => 'Email', 'phone' => 'Phone', 'firstname' => 'Firstname', 'lastname' => 'Lastname', 'age' => 'Age', 'sex' => 'Gender', 'state' => 'State', 'address' => 'Address', 'points' => 'Points', 'created_on' => 'Date Joined', 'occupation' => 'Occupation', 'marital_status' => 'Relationship', 'academic_level' => 'Academic Level', 'claimed' => 'Claimed', 'recieved' => 'Recieved Products', 'daily_surveys_taken' => 'Number Of Fun Surveys Taken', 'ref_count' => 'Number Of Referrals'])->download("CONSUMERS_PROFILE.csv");
    }

    public function drake($shipping, $type = "")
    {
        if ($shipping) {
            if ($type == 'lga') {
                $nn = Location::where('id', $shipping->lga)->first();
                return $nn["name"];
            } else {
                $nn = Location::where('id', $shipping->area)->first();
                return $nn["name"];
            }

        }
        return "";
    }

    //Migrations
    public function setHairsMatchingCollection(Request $request)
    {
        $matching_conds = MatchingTag::get();
        $data = [];
        foreach ($matching_conds as $matching_cond) {
            $survey = Survey::where('id', $matching_cond->survey_id)->first();
            if ($survey) {
                $dd = json_decode($survey->survey, true);
                $fed = collect($dd['pages'][0]['elements'])->where('title', $matching_cond->title)->first();

                $data[] = [
                    'id' => $matching_cond->id,
                    'survey_id' => $matching_cond->survey_id,
                    'title' => $matching_cond->title,
                    'input_type' => $fed['type'],
                    'display' => !$matching_cond->display ? $matching_cond->title : $matching_cond->display,
                    'created_at' => $matching_cond->created_at,
                    'updated_at' => $matching_cond->updated_at,
                ];
            }

        }
        $csvExporter = new \Laracsv\Export();
        $frodo = collect($data);
        $csvExporter->build($frodo, ['id', 'survey_id', 'title', 'input_type', 'display'])->download("MatchingCollect1.csv");
    }

    public function setHairsMatchingTag(Request $request)
    {
        $matching_conds = MatchingTagData::get();
        $data = [];
        foreach ($matching_conds as $matching_cond) {
            $matchingTag = MatchingTag::where('id', $matching_cond->matching_tag_id)->first();
            $survey = Survey::where('id', $matchingTag->survey_id)->first();
            if ($survey) {
                $dd = json_decode($survey->survey, true);
                $fed = collect($dd['pages'][0]['elements'])->where('title', $matchingTag->title)->first();
                if (isset($fed['choices'])) {
                    //$jj = $dd['pages'][0]['elements'][0]['choices'];
                    $val = collect($fed['choices'])->where('text', $matching_cond->title)->first();

                    $data[] = [
                        'id' => $matching_cond->id,
                        'matching_collection_id' => $matching_cond->matching_tag_id,
                        'title' => $matching_cond->title,
                        'value' => !$val['value'] ? Str::slug($matching_cond->title) : $val['value'],
                        'image_link' => ($fed['type'] == 'imagepicker') ? $val['imageLink'] : "",
                        'type' => $fed['type'],
                        'created_at' => $matching_cond->created_at,
                        'updated_at' => $matching_cond->updated_at,
                    ];
                }
            }
        }
        $csvExporter = new \Laracsv\Export();
        $frodo = collect($data);
        $csvExporter->build($frodo, ['id', 'matching_collection_id', 'title', 'value', 'type', 'image_link'])->download("MatchingCollect1.csv");
    }

    public function setHairsCampaignAndBrands(Request $request)
    {
        $users = [];
        $brands = [];
        $consumer = User::latest('id')->first();
        $brands = Brand::get();
        foreach ($brands as $key => $brand) {
            $id = ($key + 1) + $consumer->id;
            $users[] = [
                'id' => $id,
                'username' => Str::slug($brand->name),
                'email' => Str::slug($brand->name) . '@app.com',
                'password' => bcrypt('password'),
                'phone' => "",
                'old_id' => $brand->id,
            ];
            $brands[] = [
                'id' => $brand->id,
                'user_id' => $id,
                'name' => $brand->name,
                'description' => $brand->description,
                'photo' => $brand->photo,
            ];
        }
        $data = [];
        $campaigns = Campaign::get();

        foreach ($campaigns as $campaign) {
            $brand = collect($users)->where('old_id', $campaign->brand_id)->first();
            $pages = CampaignPage::where('campaign_id', $campaign->id)->first();
            $page_data = collect(json_decode($pages->data, true));
            //$text = $page_data['long_description_content'] . ' <br>' . implode("," ,$page_data['feature_list']);
            $text = $page_data['long_description_content'];
            $data[] = [
                'id' => $campaign->id,
                'user_id' => $brand['id'],
                'category_id' => $campaign->category_id,
                'title' => $campaign->title,
                'ref' => Str::random(10),
                'short_description' => $page_data['short_description'],
                'description' => $text,
                'terms' => "",
                'banner' => $page_data->get('page_banner', ""),
                'display_picture' => $campaign->photo,
                'product_pictures' => json_encode($page_data['page_pictures']),
                'starts_on' => $campaign->starts_on,
                'ends_on' => $campaign->ends_on,
                'status' => $campaign->status,
                'amount' => 0,
                'created_at' => $campaign->created_at,
                'updated_at' => $campaign->updated_at,
            ];
        }

        if ($request->qt == 1) {
            $csvExporter = new \Laracsv\Export();
            $frodo = collect($data);
            $csvExporter->build($frodo, ['id', 'user_id', 'category_id', 'title', 'ref', 'short_description', 'description', 'terms', 'banner', 'display_picture', 'product_pictures', 'starts_on', 'ends_on', 'status', 'amount', 'created_at', 'updated_at'])->download("Campaign.csv");
        }

        if ($request->qt == 2) {
            $csvExporter2 = new \Laracsv\Export();
            $frodo2 = collect($users);
            $csvExporter2->build($frodo2, ['id', 'username', 'email', 'password', 'phone'])->download("BrandsUser.csv");
        }

        if ($request->qt == 3) {
            $csvExporter3 = new \Laracsv\Export();
            $frodo3 = collect($brands);
            $csvExporter2->build($frodo3, ['id', 'user_id', 'name', 'description'])->download("Brands.csv");
        }

    }

}
