<?php

namespace App\Http\Controllers;

use App\User;
use App\Campaign;
use App\Consumer;
use App\ApiClient;
use App\InventoryLog;
use Firebase\JWT\JWT;
use App\ShippingAddress;
use App\ConsumerCampaign;
use App\OutboundInventory;
use JamesGordo\CSV\Parser;
use Illuminate\Http\Request;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;
use App\InboundInventory;
use App\Mail\NotifyConsumers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HomeCtrl extends Controller
{
    public function home(Request $request)
    {
        // echo bcrypt('password');
        // $campaignID=4;
        // $campaign = CampaignMatchingData::where('campaign_id', $campaignID)->get()->groupBy('matching_tag_id');
        // $consumers = [];
        // ConsumerMatchingData::chunk(100, function($chunk)use(&$consumers, $campaign){
        //     $cc = $chunk->groupBy('consumer_id');
        //     foreach (collect($cc) as $gg_key => $gg) {
        //         $match_count = 0;
        //         $get_tag_group_count = $campaign->count();
        //         foreach($campaign as $camp_key => $camp_value){
        //             $intersect = array_intersect($camp_value->pluck('matching_data_id')->all(), $gg->pluck('matching_data_id')->all());
        //             if (count($intersect) > 0) {
        //                 //$matches[] = 1;
        //                 $match_count += 1;
        //             }
        //         }
        //         if($get_tag_group_count == $match_count){
        //             $consumers[] = $gg_key;
        //         }

        //     }
        // });


    }

    public function campaign_landing(Request $request, $id)
    {
        $campaign = Campaign::with(['page', 'brand'])->where('id', $id)->first();
        return view('campaign.landing', ['campaign' => $campaign]);
    }

    public function get_matched_campaigns($consumer)
    {
        /**
         * Lets get matching campaign
         */
        $consumer_matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        $matched_campaign = Campaign::with(['matching_data'])->where('status', 'OPEN')->get()->filter(function ($value, $key) use ($consumer_matching_data) {
            //group by matching tag
            $get_tag_group_count = $value->matching_data->groupBy('matching_tag_id')->count();
            $group_by_campaign_matching_tag = $value->matching_data->groupBy('matching_tag_id')->all();
            $consumer = $consumer_matching_data->pluck('matching_data_id')->all();
            $matches = [];
            $match_count = 0;
            foreach (collect($group_by_campaign_matching_tag) as $gg) {
                $intersect = array_intersect($gg->pluck('matching_data_id')->all(), $consumer);
                if (count($intersect) > 0) {
                    // logger($gg->pluck('matching_data_id')->all());
                    // logger($consumer);
                    // logger($intersect);
                    $matches[] = 1;
                    $match_count += 1;
                }
            }
            // logger(count($matches));
            // logger($match_count);
            return $match_count === $get_tag_group_count;
        });

        // $matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        // $campaign_matching_data = CampaignMatchingData::whereIn('matching_data_id', $matching_data->pluck('matching_data_id'))->get();
        // $matched_campaign = Campaign::with('brand')->whereIn('id', $campaign_matching_data->pluck('campaign_id'))->where('status', 'OPEN');

        /**
         * Lets filter out the campaigns the user has not taken before
         */
        $consumer_campaigns = ConsumerCampaign::where('consumer_id', $consumer->id)->get();
        $filter_not_taken = $matched_campaign->whereNotIn('id', $consumer_campaigns->pluck('campaign_id'));

        /**
         *  Lets get the campaign categories the user has not claimed
         */
        $user_inventories = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
        $logs = InventoryLog::whereIn('outbound_inventory_id', $user_inventories->pluck('id'))->get();
        $final_filter = $filter_not_taken->whereNotIn('category_id', $logs->pluck('category_id'));
        $cc = Campaign::with('brand')->whereIn('id', $final_filter->pluck('id'))->get();
        return $cc;
    }

    public function playgroundgt(Request $request)
    {
        $ids = [20, 21, 18];
        $phones = [
            "+2348138139458","+2347064450026","+2348161178500","+2348060462394","+2348161333171","+2348020765852","+2347033252844","+2348163847503","+2348134124726","+2348106622496","+2348169243781","+2347033827657","+2347039445460","+2347089306571","+2348166192081","+2349093845395","+2348180779488","+2348068834484","+2348167992660","+2347065940184","+2348160427544","+2349022365990","+2348169115213","+2348184908965","+2348035184844","+2347058906503","+2348136591585","+2348027922363","+2348102953646","+2348182635033","+2347011475847","+2347011830423","+2347017228668","+2347031237071","+2347033418113","+2347034914441","+2347035040189","+2347036877468","+2347037162639","+2347037392006","+2347057217297","+2347060473812","+2347061536154","+2347068949921","+2347081330414","+2347083086498","+2347087708422","+2348022823226","+2348036423918","+2348036446014","+2348037525243","+2348050696665","+2348057484372","+2348060344039","+2348062070181","+2348063423020","+2348063756358","+2348064017874","+2348065086025","+2348065516160","+2348066979161","+2348067864475","+2348068575191","+2348075305617","+2348077094890","+2348085076982","+2348087842191","+2348090651344","+2348092572647","+2348100099155","+2348100609909","+2348100869585","+2348101097519","+2348103267833","+2348103833150","+2348105794681","+2348106700405","+2348107418827","+2348109947354","+2348113855282","+2348118610973","+2348128198530","+2348133598150","+2348133871438","+2348134820864","+2348136509730","+2348137137224","+2348139485182","+2348148405666","+2348157770776","+2348161651115","+2348162945069","+2348163613252","+2348163976652","+2348164181144","+2348165470836","+2348166498423","+2348168290493","+2348168688643","+2348171891099","+2348177135924","+2348182816442","+2348184894652","+2348185349325","+2348189413326","+2349017152152","+2349018178806","+2349032155733","+2349034920303","+2349050227642","+2349064695484","+2349065637796","+2349066991205","+2349069524923","+2349081693297","+2347012315917"
        ];
        $campaign = Campaign::whereNotIn('id', $ids)->inRandomOrder();
        $users = User::whereIn('phone', $phones)->get();
        $consumers = Consumer::whereIn('user_id', $users->pluck('id'))->get();
        foreach ($consumers as $consumer) {
            $getCount = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->count();
            //$counnt = $getCount < 3 ? 3 : $getCount;
            $counnt = 5 - $getCount;
            //logger([$getCount, $counnt]);
            if ($getCount <= 5 && $counnt > 0) {
                $added = random_int(1, $counnt);
                $campaignIds = $campaign->limit($added)->get();
                // $matching_data = CampaignMatchingData::whereIn('campaign_id', $campaignIds->pluck('id'))->get();
                // $saveAble = [];
                // foreach($matching_data as $md){
                //     $saveAble[] = new ConsumerMatchingData([
                //         'matching_data_id'=> $md->matching_data_id,
                //         'matching_tag_id'=> $md->matching_tag_id,
                //     ]);
                // }
                // $consumer->matching_data()->saveMany($saveAble);
                $saveAble = [];
                $saveCampaign = [];
                $shipping = ShippingAddress::where('user_id', $consumer->user_id)->first();
                foreach ($campaignIds as $cc) {
                    $saveAble[] = new OutboundInventory([
                        'shipping_address_id' => $shipping->id,
                        'status' => 'QUEUED',
                        'campaign_id' => $cc->id,
                        'last_sample_added_on' => now(),
                    ]);
                    $saveCampaign[] = new ConsumerCampaign([
                        'campaign_id' => $cc->id,
                    ]);
                }

                $consumer->outbound_inventories()->saveMany($saveAble);
                $consumer->campaigns()->saveMany($saveCampaign);
            }

        }
    }
    public function playgroundkkkk(Request $request)
    {
        $consumers = Consumer::count();
        //$pay9 = ApiClient::where('name', '9Pay')->
        $consumer_9pay = Consumer::where('api_client_id', 3)->count();
        $counser_organic = Consumer::where('api_client_id', 1)->count();
        return response()->json(['total_users' => $consumers, '9pay_users' => $consumer_9pay, 'organic' => $counser_organic], 200);
    }

    public function playground(Request $request)
    {
        $link_query = $request->query('type');
        $filename = "";
        if ($link_query == 0) {
            $consumer = Consumer::with(['user', 'outbound_inventories'])->get()->filter(function ($consumer) {
                $claimed = $consumer->outbound_inventories->where('status', '!=', 'CLAIMED')->count();
                $dd = ShippingAddress::where('user_id', $consumer->user_id)->first();
                if ($claimed >= 3 && !is_null($dd->address)) {
                    return true;
                }
            });
            $filename = "All Consumers";
        } elseif ($link_query == 1) {
            //matched_and_not_claimed
            $consumer = Consumer::with(['user', 'outbound_inventories'])->get()->filter(function ($consumer) {
                $get_campaigns = $this->get_matched_campaigns($consumer);
                $claimed = $consumer->outbound_inventories->count();
                $dd = ShippingAddress::where('user_id', $consumer->user_id)->first();
                if ($get_campaigns->count() >= 3 && $claimed < 1 && !is_null($dd->address)) {
                    return true;
                }

            });
            $filename = "Matched And Not Claimed Consumers";
        } elseif ($link_query == 2) {
            //matched_and_no_address
            $consumer = Consumer::with(['user', 'outbound_inventories'])->get()->filter(function ($consumer) {
                $get_campaigns = $this->get_matched_campaigns($consumer);
                $claimed = $consumer->outbound_inventories->count();
                $dd = ShippingAddress::where('user_id', $consumer->user_id)->first();
                if ($get_campaigns->count() >= 3 && $claimed < 1 && is_null($dd->address)) {
                    return true;
                }

            });
            $filename = "Matched And No Address Consumers";
        } elseif ($link_query == 3) {
            //claimed_and_no_address
            $consumer = Consumer::with(['user', 'outbound_inventories'])->get()->filter(function ($consumer) {
                $claimed = $consumer->outbound_inventories->count();
                $dd = ShippingAddress::where('user_id', $consumer->user_id)->first();
                if ($claimed >= 3 && is_null($dd->address)) {
                    return true;
                }

            });
            $filename = "Claimed And No Address Consumers";
        } elseif ($link_query == 4) {
            //claimed_and_with address
            $consumer = Consumer::with(['user', 'outbound_inventories'])->get()->filter(function ($consumer) {
                $claimed = $consumer->outbound_inventories->count();
                $dd = ShippingAddress::where('user_id', $consumer->user_id)->first();
                if ($claimed >= 1 && !is_null($dd->address)) {
                    return true;
                }

            });
            $filename = "Claimed And have Address";
        }

        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($consumer) {
            $consumer->name = $consumer->firstname . ' ' . $consumer->lastname;
            $campaignsID = $consumer->outbound_inventories->pluck('campaign_id');
            $consumer->products = Campaign::whereIn('id', $campaignsID)->where('status', 'OPEN')->get()->implode('title', ',');
            $consumer->phone = $consumer->user->phone;
            $consumer->email = $consumer->user->email;
            $dd = ShippingAddress::where('user_id', $consumer->user_id)->first();
            $consumer->address = $dd->address;
            $consumer->address_type = $dd->is_home_address ? 'HOME ADDRESS' : 'OFFICE ADDRESS';
        });
        $csvExporter->build($consumer, ['name' => 'Name', 'address' => 'Address', 'phone' => 'Phone Number', 'email' => 'Email', 'products' => 'Products'])->download($filename . ".csv");

    }

    public function createToken()
    {
        $payload = [
            'sub' => 2,
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60),
        ];
        return JWT::encode($payload, config('jwt.token_secret'));
    }

    public function simple_logistics(Request $request)
    {
        if($request->isMethod('POST')){
            if($request->file('file')->isValid() && $request->hasFile('file')){
                $csv = new Parser($request->file('file'));
                //$consumers = Consumer::where('email', ) $consumers->pluck('email')
               $Emails = collect($csv->all())->pluck('Email')->all();
               $users = User::whereIn('email', $Emails)->get();
               $consumers = Consumer::whereIn('user_id', $users->pluck('id'))->get();
               OutboundInventory::whereIn('consumer_id', $consumers->pluck('id'))->update(['status'=>'CLAIMED']);

               $mailables = [];
               foreach($consumers as $consumer){
                    //$feedback = New ConsumerCampaign();
                    $check = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
                    if($check){
                        $outbound = OutboundInventory::where('consumer_id', $consumer->id)->update(['status'=>'CLAIMED']);
                       // $feedback->consumer_id = $consumer->id;
                       // $feedback->campaign_id = $consumer->id;
                        $mailables[] = $consumer->email;
                    }

               }
              // Mail::to($mailables)->send(new NotifyConsumers());

               //Mail::to([['email'=>'michael@hazina.ng'], ['email'=>'vcordukandu@gmail.com'], ['email'=> 'titilola@hazina.ng']])->send(new NotifyConsumers());
               return redirect()->back()->with('success', 'Inventory has been confirmed and email has been sent to respective consumers');
            }
        }
        return view('logistics.index');
    }

    public function all_users(){
        $consumer = Consumer::with('user')->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($consumer) {
            $consumer->username = $consumer->username;
            $consumer->firstname = $consumer->firstname;
            $consumer->lastname = $consumer->lastname;
            $consumer->age = $consumer->age;
            $consumer->gender = $consumer->gender;
            $consumer->created_on = \Carbon\Carbon::parse($consumer->created)->toFormattedDateString();
        });
        $csvExporter->build($consumer, ['username' => 'Username', 'firstname' => 'Firstname', 'lastname'=> 'Lastname', 'age' => 'Age', 'gender' => 'Gender', 'created_on'=> 'Date Joined'])->download("Consumers.csv");

    }

    public function all_ranks(){
        // $consumers = DB::select("SELECT consumers.firstname, consumers.lastname, consumers.wk1_points AS 'points', consumers.photo, users.username FROM consumers LEFT JOIN users on consumers.user_id=users.id ORDER BY wk1_points DESC LIMIT 10;");
        $consumers = Consumer::where('wk3_points', '>', 0)->orderBy('wk3_points', 'DESC')->limit(10)->get();
        return response()->json($consumers, 200);
    }

    public function maker(Request $request){
        $campaigns = array(
            array('id' => '16', 'title' => 'Cadbury 3in1 Chocolate','status' => 'OPEN', 'qty'=> 175),
            array('id' => '19', 'title' => 'Sunvita Choco Crunch 40G', 'status' => 'OPEN', 'qty'=> 120),
            array('id' => '20','title' => 'Molfix Newborn Baby Diaper', 'status' => 'OPEN', 'qty'=> 379),
            array('id' => '22', 'title' => 'Family Custard Vanilla', 'status' => 'OPEN', 'qty'=> 246),
            array('id' => '23', 'title' => 'Nestle Golden Morn', 'status' => 'OPEN', 'qty'=> 167),
            array('id' => '24', 'title' => 'Golden Penny Instant Noodles', 'status' => 'OPEN', 'qty'=> 74),
            array('id' => '25', 'title' => 'Gino TOMATO - Magic Pepper & Onion Tomato','status' => 'OPEN', 'qty'=> 220),
            array('id' => '26', 'title' => 'Tasty Tom Tomato Mix', 'status' => 'OPEN', 'qty'=> 237),
            array('id' => '27','title' => 'Munch It Crunchy Snacks - Sweet Surprsie','status' => 'OPEN', 'qty'=> 223),
            array('id' => '28','title' => 'Mentos Mint Roll','status' => 'OPEN', 'qty'=> 264),
            array('id' => '29','title' => 'Durex Sensation','status' => 'OPEN', 'qty'=> 2),
            array('id' => '30','created_at' => '2020-10-30 15:32:50','updated_at' => '2020-12-18 12:25:53','title' => 'Durex Extra Safe','status' => 'OPEN', 'qty'=> 2),
            array('id' => '31','title' => 'Durex Performa','status' => 'OPEN', 'qty'=> 3)
          );

          foreach($campaigns as $campaign){
              $bb = Campaign::where('id', $campaign['id'])->first();
              $bb->status = 'OPEN';
              $bb->created_at = Carbon::now();
              $bb->updated_at = Carbon::now();
              $bb->save();
              $inb = InboundInventory::where('campaign_id', $bb->id)->first();
              $inb->quantity = $campaign['qty'];
              $inb->in_stock = true;
              $inb->save();
          }

    }

    public function grind(Request $request){
        // $consumers = Consumer::get();
        // foreach($consumers as $consumer){
        //     $status = json_decode($consumer->registration_status, true);
        //     $stages = ["BASIC","PHONE","ACCOUNT","SURVEY"];
        //     //$res = array_intersect($stages, $status);
        //     if(count(array_diff($stages, $status)) == 0){
        //         $consumer->is_reg_complete = true;
        //         $consumer->save();
        //     }
        //     User::where([['id', $consumer->user_id]])->update(['ref'=> Str::random(10)]);
        // }
      // $users = User::where('ref', null)->update(['ref'=> Str::random(10)]);
    }
}
