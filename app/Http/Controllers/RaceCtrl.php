<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Consumer;
use Carbon\Carbon;
use App\OutboundInventory;
use App\ShippingAddress;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class RaceCtrl extends Controller
{
    public function race(Request $request){
        $groups = OutboundInventory::has('consumer')->orderBy('created_at', 'desc')
                    ->get()
                    ->groupBy('consumer_id');
                    // ->groupBy(function($q){
                    //     return Carbon::parse($q->created_at)->format('F Y');
                    // });
        $data = [];
        $keys = [
            'firstname', 'lastname', 'email', 'phone', 'address'
        ];
      foreach($groups as $userId => $group){
        $consumer = Consumer::where('id', $userId)->first();
        $address = ShippingAddress::where('user_id', $consumer->user_id)->first();
        $dd = [
            'firstname'=> $consumer->firstname,
            'lastname'=> $consumer->lastname,
            'email'=> $consumer->email,
            'phone'=> $consumer->phone,
            'address'=> !$address ? "" : $address->full_address,
        ];

        $products = collect($group)->groupBy(function($q){
            return Carbon::parse($q->created_at)->format('F Y');
        });
        foreach($products as $date => $product){
            $pp = Campaign::whereIn('id', collect($product)->pluck('campaign_id')->all())->get();
            $dd[Str::slug($date, '_')] = $pp->implode('title', ',');
            $dd[Str::slug($date, '_') . '_status'] = collect($product)->implode('status', ',');
            $keys[] = Str::slug($date, '_');
            $keys[] = Str::slug($date, '_') . '_status';
        }
        $data[] = $dd;
      }

      $sortKeys = collect($keys)->unique();
    //   header('Content-Type: application/json');
    //   echo json_encode($sortKeys->values()->all(), JSON_PRETTY_PRINT);

    //   header('Content-Type: application/json');
    //   echo json_encode($data, JSON_PRETTY_PRINT);
    $csvExporter = new \Laracsv\Export();
    $csvExporter->build(collect($data), $sortKeys->values()->all())->download("INVENTORY_FROM_BEGINNING.csv");

    }

    public function dump_db(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1800');
        \Spatie\DbDumper\Databases\MySql::create()
        ->setDbName(config('database.connections.mysql.database'))
        ->setUserName(config('database.connections.mysql.username'))
        ->setPassword(config('database.connections.mysql.password'))
        ->dumpToFile(public_path('database.sql'));
        return response()->download(public_path('database.sql'))->deleteFileAfterSend(true);
    }
}
