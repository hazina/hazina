<?php

namespace App\Http\Controllers;

use App\User;
use App\Consumer;
use App\Location;
use Carbon\Carbon;
use App\MatchingTag;
use App\ShippingAddress;
use App\OutboundInventory;
use App\ConsumerDailySurvey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConsumerInfoCtrl extends Controller
{
    public function info(Request $request){
       // ini_set('memory_limit', '-1');
       // ini_set('max_execution_time', '1800');
        $data = [];
        $consumers = Consumer::has('user')->where('id', '>', 4174)->limit(1000)->get();

        foreach ($consumers as $consumer) {
            $occ = MatchingTag::where('name', 'employment_status')->first();
            $occc = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $occ->id)->first();

            $mar = MatchingTag::where('name', 'marital_status')->first();
            $marr = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $mar->id)->first();

            $academic = MatchingTag::where('name', 'academic_level')->first();
            $academy = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $academic->id)->first();

            $address = ShippingAddress::where('user_id', $consumer->user_id)->first();
            $location = $address ? Location::where('id', $address->state)->first() : null;

            $getRef = User::where('id', $consumer->user_id)->first();
            $refCount = User::where('referrer', $getRef->ref)->count();

            $data[] = [
                'user_id' => $consumer->id,
                'firstname' => $consumer->firstname,
                'lastname' => $consumer->lastname,
                'email' => $consumer->email,
                'phone' => $consumer->phone,
                'age' => $consumer->age,
                'sex' => $consumer->gender,
                'state' => is_null($location) ? "" : $location->name,
                'address' => !$address ? "" : $address->full_address,
                'points' => $consumer->points,
                'created_on' => Carbon::parse($consumer->created_at)->toDateString(),
                'occupation' => $occc ? $occc->matching_data->title : "",
                'marital_status' => $marr ? $marr->matching_data->title : "",
                'academic_level' => $academy ? $academy->matching_data->title : "",
                'claimed' => OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'QUEUED']])->count(),
                'recieved' => OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->count(),
                'daily_surveys_taken' => ConsumerDailySurvey::where('consumer_id', $consumer->id)->count(),
                'ref_count' => $refCount,
            ];
        }
        $csvExporter = new \Laracsv\Export();
        $csvExporter->build(collect($data), ['user_id' => 'ID', 'email' => 'Email', 'phone' => 'Phone', 'firstname' => 'Firstname', 'lastname' => 'Lastname', 'age' => 'Age', 'sex' => 'Gender','state'=> 'State', 'address'=> 'Address', 'points'=> 'Points', 'created_on'=> 'Date Joined', 'occupation' => 'Occupation', 'marital_status' => 'Relationship', 'academic_level'=> 'Academic Level', 'claimed' => 'Claimed', 'recieved' => 'Recieved Products', 'daily_surveys_taken' => 'Number Of Fun Surveys Taken', 'ref_count'=> 'Number Of Referrals'])->download("CONSUMERS_PROFILE.csv");
    }
}
