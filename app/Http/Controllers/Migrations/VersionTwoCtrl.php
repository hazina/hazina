<?php

namespace App\Http\Controllers\Migrations;

use ZipArchive;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class VersionTwoCtrl extends Controller
{
    public function  handler(Request $request){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '1800');
        $files = [
    //     'locations',
    // // //     'api_clients',
        // 'users',
        // 'brands',
        // 'consumers',
    //     'shipping_addresses',
    //     'categories',
    //     'campaigns',
        // 'freebies',
        // 'surveys',
        // 'matching_collections',
    //     'matching_tags',
    //     'feedback_surveys',
    //     'daily_surveys',
    //     'campaign_matching_tags',
        'consumer_matching_tags',
        // 'consumer_campaigns',
        // 'consumer_daily_surveys',
        // 'consumer_feedback_surveys',
        // 'consumer_freebies',
        // 'inbound_inventories',
        // 'fulfillments',
        // 'boxes',
        // 'box_products',
        // 'delivery_agents',
        // 'delivery_agent_boxes',
        // 'deliveries'
        ];


       // $taker = $request->query('take');
        //$limit = $request->query('limit');
        // $take = intval($taker) > 4 ? collect($files)->skip(intval($taker)) : collect($files)->take(intval($taker));
        $zipFiles = [];

        // if(file_exists(base_path('stubs/exports'))){
        //     $this->delTree(base_path('stubs/exports'));
        //     mkdir(base_path('stubs/exports'));
        // }else{
        //     mkdir(base_path('stubs/exports'));
        // }

        foreach($files as $file){
            if(file_exists(base_path('stubs/exports/' . $file . '.json'))){
                $zipFiles[] = base_path('stubs/exports/' . $file . '.json');
            }else{
                $rr = str_replace("_", "", Str::title($file)) . 'Data';
                $fd = '\App\Http\Controllers\Migrations\Data\\' . $rr;
                if(class_exists($fd)){
                    $dd = new $fd();
                    $dd->handle('stubs/exports/' . $file . '.json');
                    $zipFiles[] = base_path('stubs/exports/' . $file . '.json');
                }
            }
        }

        //zip folder up
        $folder = base_path('stubs/exports');
        $zip = new \ZipArchive();
        $timeName = time();
        $fileName = public_path('stubs/'. $timeName .'.zip');
        if ($zip->open($fileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)== TRUE)
        {
            //$files = Storage::allFiles($folder);

            foreach ($zipFiles as $key => $value){
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }
        // if(ob_get_length() > 0) {
        //     ob_clean();
        // }
       //return response()->download($fileName)->deleteFileAfterSend(true);
       // $ffd = response()->download($fileName);
       // print_r($zipFiles);
       //Storage::move($fileName, public_path('/'));
      // return redirect('api.gethazina.com/stubs/' . $timeName . '.zip');
    }


    public static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
         foreach ($files as $file) {
           (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
         }
         return rmdir($dir);
       }



}
