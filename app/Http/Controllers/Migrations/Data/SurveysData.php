<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Survey;

class SurveysData{
    public function handle($filename){
        $surveys = Survey::get();
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build($surveys, ['id', 'title', 'success_message','number_of_questions', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $data = [];
        foreach($surveys as $survey){
            $data[] = [
                'id'=> $survey->id,
                'title'=> $survey->title,
                'number_of_questions'=> $survey->number_of_questions,
                'success_message'=> null,
                'created_at'=> $survey->created_at,
                'updated_at'=> $survey->updated_at,
            ];
        }
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
