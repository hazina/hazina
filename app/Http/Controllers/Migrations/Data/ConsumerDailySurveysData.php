<?php
namespace App\Http\Controllers\Migrations\Data;

use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;
use App\ConsumerDailySurvey;

class ConsumerDailySurveysData{
    public function handle($filename){
        $consumer_daily_surveys = ConsumerDailySurvey::with('consumer')->has('consumer')->get();

        $daily_surveys = collect(ResolveJson::decodeFile(base_path('stubs/exports/daily_surveys.json')));
        $users = collect(ResolveJson::decodeFile(base_path('stubs/exports/users.json')));

        $data = [];
        foreach($consumer_daily_surveys as $consumer_daily_survey){
            $daily_survey = $daily_surveys->firstWhere('id', $consumer_daily_survey->daily_survey_id);
            $user = $users->firstWhere('id', $consumer_daily_survey->consumer_id);
            if($daily_survey && $user){
                $data[] = [
                    'id'=> $consumer_daily_survey->id,
                    'daily_survey_id'=> $consumer_daily_survey->daily_survey_id,
                    'user_id'=> $user['id'],
                    'created_at'=> $consumer_daily_survey->created_at,
                    'updated_at'=> $consumer_daily_survey->updated_at,
                ];
            }

        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'daily_survey_id', 'user_id', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
