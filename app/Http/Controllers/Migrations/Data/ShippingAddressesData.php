<?php
namespace App\Http\Controllers\Migrations\Data;

use App\ShippingAddress;
use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;

class ShippingAddressesData{
    public function handle($filename){
        $shippings = ShippingAddress::get();
        $data = [];

        $users = collect(ResolveJson::decodeFile(base_path('stubs/exports/users.json')));

        foreach($shippings as $shipping){
            $user = $users->where('id', $shipping->user_id)->first();
            if($user){
                $data[] = [
                    'id'=> $shipping->id,
                    'user_id'=> $shipping->user_id,
                    'state'=> $shipping->state,
                    'country'=> $shipping->country,
                    'lga'=> $shipping->lga,
                    'area'=> $shipping->area,
                    'street_number'=> $shipping->street_number,
                    'landmark'=> $shipping->landmark,
                    'full_address'=> $shipping->full_address,
                    'created_at'=> $shipping->created_at,
                    'updated_at'=> $shipping->updated_at,
                ];
            }
        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id','user_id','state', 'country','lga','area', 'street_number', 'street_name', 'landmark', 'full_address', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
