<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Applets\ResolveJson;
use App\Consumer;
use App\ConsumerMatchingData;
use App\OutboundInventory;
use App\User;
use JamesGordo\CSV\Parser;

class ConsumersData{
    public function handle($filename){
        $data = [];
        $getUsers = ResolveJson::decodeFile(base_path('stubs/exports/users.json'));
        $users = collect($getUsers);
        $consumers = Consumer::get();

        foreach($consumers as $consumer){
            $user = $users->where('id', $consumer->user_id)->first();
            if($user){
                $data[] = [
                    'id'=> $consumer->id,
                    'user_id'=> $consumer->user_id,
                    'firstname'=> $consumer->firstname,
                    'lastname'=> $consumer->lastname,
                    'registration_status'=> !$consumer->registration_status ? null : $consumer->registration_status,
                    'is_reg_complete'=> !$consumer->is_reg_complete ? 0 : boolval($consumer->is_reg_complete),
                    'points'=> intval($consumer->points),
                    'photo'=> $consumer->photos,
                    //'is_complete'=> $consumer->is_complete,
                    'created_at'=> $consumer->created_at,
                    'updated_at'=> $consumer->updated_at,
                    // 'outcount'=> OutboundInventory::where([['status', 'CLAIMED'], ['consumer_id', $consumer->id]])->count(),
                    // 'apps'=> User::where('phone', $user->phone)->count(),
                ];
            }

        }

        $ddd = collect($data);
        // $fred = $ddd->reject(function($value, $key){
        //     return ($value['outcount'] < 1 && $value['apps'] > 1) ? true : false;
        // });

        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build($ddd, ['user_id','firstname', 'lastname', 'registration_status', 'points', 'photo','is_reg_complete','created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, $ddd->toJson());
    }
}
