<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Survey;
use App\MatchingTag;
use Illuminate\Support\Str;
use App\Applets\ResolveJson;
use Illuminate\Http\Request;

class MatchingCollectionsData{
    public function handle($filename){
        $matching_conds = MatchingTag::has('matching_data')->get();
        $surveys = collect(ResolveJson::decodeFile(base_path('stubs/exports/surveys.json')));

        $data = [];
        foreach ($matching_conds as $matching_cond) {
            $surve = $surveys->firstWhere('id', $matching_cond->survey_id);
            if ($surve) {
                $survey = Survey::where('id', $matching_cond->survey_id)->first();
                $dd = json_decode($survey->survey, true);
                $fed = collect($dd['pages'][0]['elements'])->where('title', $matching_cond->title)->first();

                $data[] = [
                    'id' => $matching_cond->id,
                    'survey_id' => $matching_cond->survey_id,
                    'title' => $matching_cond->title,
                    'name' => Str::slug($matching_cond->title, '_'),
                    'input_type' => isset($fed['type']) ? $fed['type'] : '',
                    'display' => !$matching_cond->display ? $matching_cond->title : $matching_cond->display,
                    'created_at' => $matching_cond->created_at,
                    'updated_at' => $matching_cond->updated_at,
                ];
            }

        }
        // $csvExporter = new \Laracsv\Export();
        // $frodo = collect($data);
        // $csvExporter->build($frodo, ['id', 'survey_id', 'title', 'input_type', 'display', 'name']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
