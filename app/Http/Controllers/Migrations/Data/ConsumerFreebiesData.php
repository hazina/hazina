<?php
namespace App\Http\Controllers\Migrations\Data;

use App\ConsumerFreeby;
use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;

class ConsumerFreebiesData{
    public function handle($filename){
        $campaigns = ConsumerFreeby::with('consumer')->has('consumer')->get();
        $users = collect(ResolveJson::decodeFile(base_path('stubs/exports/users.json')));
        $freebies = collect(ResolveJson::decodeFile(base_path('stubs/exports/freebies.json')));
        $data = [];
        foreach($campaigns as $campaign){
            $user = $users->firstWhere('id', $campaign->consumer_id);
            $freebie = $freebies->firstWhere('id', $campaign->daily_survey_id);
            if($user && $freebie){
                $data[] = [
                    'id'=> $campaign->id,
                    'freebie_id'=> $campaign->daily_survey_id,
                    'user_id'=> $campaign->consumer->user_id,
                    'created_at'=> $campaign->created_at,
                    'updated_at'=> $campaign->updated_at,
                ];
            }
        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'freebie_id', 'user_id', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
