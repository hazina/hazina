<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Campaign;
use App\CampaignPage;
use App\InboundInventory;
use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;

class InboundInventoriesData
{
    public function handle($filename)
    {
        $inbound_inventories = InboundInventory::with('campaign')->get();
        $campaigns = collect(ResolveJson::decodeFile(base_path('stubs/exports/campaigns.json')));

        $data = [];
        foreach ($inbound_inventories as $inbound_inventory) {
            $campaign = $campaigns->firstWhere('id', $inbound_inventory->campaign_id);
            if ($campaign) {
                $pages = CampaignPage::where('campaign_id', $inbound_inventory->campaign->id)->first();
                $page_data = collect(json_decode($pages->data, true));
                $data[] = [
                    'id' => $inbound_inventory->id,
                    'user_id' => $campaign['user_id'],
                    'campaign_id' => $inbound_inventory->campaign_id,
                    'quantity' => intval($inbound_inventory->quantity),
                    'rejected' => 0,
                    'warehouse' => 'Hazina HQ',
                    'status' => 'DELIVERED',
                    'sample_expectation_date' => $inbound_inventory->created_at,
                    'name' => $inbound_inventory->campaign->title,
                    'description' => $page_data['short_description'],
                    'created_at' => $inbound_inventory->created_at,
                    'updated_at' => $inbound_inventory->updated_at,
                ];
            }
        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), array_keys($data));
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
