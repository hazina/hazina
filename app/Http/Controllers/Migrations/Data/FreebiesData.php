<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Freebie;

class FreebiesData{
    public function handle($filename){
        $freebies = Freebie::get();
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build($freebies, ['id', 'type','amount', 'points', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $data = [];
        foreach($freebies as $freebie){
            $data[] = [
                'id'=> $freebie->id,
                'amount'=> $freebie->amount,
                'type'=> strtoupper($freebie->type),
                'level'=> intval($freebie->level),
                'points'=> $freebie->points,
                'quantity'=> !$freebie->qunatity ? 0 : $freebie->qunatity,
                'created_at'=> $freebie->created_at,
                'updated_at'=> $freebie->updated_at,
            ];
        }
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
