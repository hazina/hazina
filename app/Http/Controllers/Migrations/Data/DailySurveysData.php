<?php
namespace App\Http\Controllers\Migrations\Data;

use App\DailySurvey;
use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;

class DailySurveysData{
    public function handle($filename){
        $daily_surveys = DailySurvey::get();
        $surveys = collect(ResolveJson::decodeFile(base_path('stubs/exports/surveys.json')));
        $data = [];
        foreach($daily_surveys as $daily_survey){
            $survey = $surveys->firstWhere('id', $daily_survey->survey_id);
            if($survey){
                $data[] = [
                    'id'=> $daily_survey->id,
                    'level'=> $daily_survey->level,
                    'photo'=> $daily_survey->photo,
                    'points'=> $daily_survey->points,
                    'survey_id'=> $daily_survey->survey_id,
                    'title'=> $daily_survey->title,
                    'created_at'=> $daily_survey->created_at,
                    'updated_at'=> $daily_survey->updated_at,
                ];
            }

        }

        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'level', 'photo', 'points', 'survey_id', 'title' ,'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
