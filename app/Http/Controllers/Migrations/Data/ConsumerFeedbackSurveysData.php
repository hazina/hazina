<?php
namespace App\Http\Controllers\Migrations\Data;

use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;
use App\ConsumerFeedbackSurvey;

class ConsumerFeedbackSurveysData{
    public function handle($filename){
        $campaigns = ConsumerFeedbackSurvey::with('consumer')->get();
        $users = collect(ResolveJson::decodeFile(base_path('stubs/exports/consumers.json')));
        $feedback_surveys = collect(ResolveJson::decodeFile(base_path('stubs/exports/feedback_surveys.json')));

        $data = [];
        foreach($campaigns as $campaign){
            $user = $users->firstWhere('id', $campaign->consumer_id);
            $feedback_survey = $feedback_surveys->firstWhere('id', $campaign->feedback_survey_id);
            if($user && $feedback_survey){
                $data[] = [
                    'id'=> $campaign->id,
                    'feedback_survey_id'=> $campaign->feedback_survey_id,
                    'user_id'=> $user['user_id'],
                    'created_at'=> $campaign->created_at,
                    'updated_at'=> $campaign->updated_at,
                ];
            }

        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'feedback_survey_id', 'user_id', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
