<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Location;

class LocationsData{
    public function handle($filename){
        $locations = Location::get();
        $csvExporter = new \Laracsv\Export();
        $allLocations = collect($locations)->map(function($item, $key){
            $item->parent_id = is_null($item->parent_id) ? 0 : $item->parent_id;
            return $item;
        });
        // $csvExporter->beforeEach(function($location){
        //     $location->parent_id = is_null($location->parent_id) ? 0 : $location->parent_id;
        // });
        // $csvExporter->build($locations, ['id', 'parent_id', 'name','is_visible', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, $allLocations->toJson());
    }
}
