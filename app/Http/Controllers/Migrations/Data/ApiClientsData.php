<?php
namespace App\Http\Controllers\Migrations\Data;

use App\ApiClient;

class ApiClientData{
    public function handle($filename){
        $locations = ApiClient::get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->build($locations, ['id', 'name', 'public_key','host', 'has_verified_email', 'has_verified_phone', 'created_at', 'updated_at']);
        $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, $csvWriter);
    }
}
