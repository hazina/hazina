<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Applets\ResolveJson;
use App\OutboundInventory;
use Carbon\Carbon;
use Illuminate\Support\Str;
use JamesGordo\CSV\Parser;

class FulfillmentsData
{
    public function handle($filename)
    {
        $fulfillments = [];
        $boxes = [];
        $box_products = [];
        $fullfilmentsID = 0;
        $BoxID = 0;
        // $consumers = collect(json_decode(base_path('stubs/exports/consumers.json'), true));
        $consumers = collect(ResolveJson::decodeFile(base_path('stubs/exports/consumers.json')));
        $shipping_addresses = collect(ResolveJson::decodeFile(base_path('stubs/exports/shipping_addresses.json')));
        $campaigns = collect(ResolveJson::decodeFile(base_path('stubs/exports/campaigns.json')));
        $boxes_by_month = OutboundInventory::whereIn('consumer_id', $consumers->pluck('id')->all())->get()->groupBy(function ($q) {
            return Carbon::parse($q->created_at)->format('F Y');
        });

        foreach ($boxes_by_month as $date => $box_by_month) {
            $clusters = collect($box_by_month)->where('status', 'CLAIMED')->groupBy(function ($item, $key) use ($shipping_addresses) {
                $sh = $shipping_addresses->firstWhere('id', $item->shipping_address_id);
                return $sh['area'];
            })->keys()->count();
            $box_count = collect($box_by_month)->where('status', 'CLAIMED')->count();
            $fullfilment_status = 'DISPATCHED';
            $fullfilment_date = Carbon::parse($date)->toDateString();
            $fullfilmentsID += 1;
            $fulfillments[] = [
                'id' => $fullfilmentsID,
                'clusters' => $clusters,
                'boxes' => $box_count,
                'status' => $fullfilment_status,
                'fulfillment_date' => $fullfilment_date,
            ];

            //group for individual consumer
            $MonthlyProducts = collect($box_by_month)->where('status', 'CLAIMED')->groupBy('consumer_id');
            foreach ($MonthlyProducts as $consumerID => $MonthlyProductBox) {
                $getUser = $consumers->firstWhere('id', $consumerID);
                if ($getUser) {
                    $BoxID += 1;
                    $getUserddress = $shipping_addresses->firstWhere('user_id', $getUser['user_id']);
                    $lastProduct = collect($MonthlyProducts)->sortBy('id', true)->first();
                    $boxes[] = [
                        'id' => $BoxID,
                        'user_id' => $getUser['user_id'],
                        'shipping_address_id' => $getUserddress['id'],
                        'fulfillment_id' => $fullfilmentsID,
                        'ref' => Str::random(10),
                        'opened_on' => is_null(collect($MonthlyProductBox)->get('created_at')) ? now() : collect($MonthlyProductBox)->get('created_at'),
                        'closed_on' => is_null($lastProduct->get('created_at')) ? now(): $lastProduct->get('created_at'),
                        'status' => 'CLOSED',
                        'delivery_status' => 'DELIVERED',
                    ];

                    foreach ($MonthlyProductBox as $pro) {
                        $getCampaign = $campaigns->firstWhere('id', $pro['campaign_id']);
                        if ($getCampaign) {
                            $box_products[] = [
                                'box_id' => $BoxID,
                                'campaign_id' => $getCampaign['id'],
                                'category_id' => $getCampaign['category_id'],
                            ];
                        }
                    }
                }
            }
        }

        // $csvExporter3 = new \Laracsv\Export();
        // $csvExporter3->build(collect($fulfillments), ['id', 'clusters', 'boxes', 'status', 'date']);
        // $csvWriter3 = $csvExporter3->getWriter();

        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($boxes), ['id', 'user_id', 'shipping_address_id', 'fulfillment_id', 'ref', 'opened_on', 'closed_on', 'status', 'delivery_status']);
        // $csvWriter = $csvExporter->getWriter();

        // $csvExporter2 = new \Laracsv\Export();
        // $csvExporter2->build(collect($box_products), ['box_id', 'campaign_id', 'category_id']);
        // $csvWriter2 = $csvExporter2->getWriter();

        $path3 = base_path('stubs/exports/fulfillments.json');
        file_put_contents($path3, collect($fulfillments)->toJson());

        $path = base_path('stubs/exports/boxes.json');
        file_put_contents($path, collect($boxes)->toJson());

        $path2 = base_path('stubs/exports/box_products.json');
        file_put_contents($path2, collect($box_products)->toJson());

    }
}
