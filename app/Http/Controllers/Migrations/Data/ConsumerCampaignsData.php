<?php
namespace App\Http\Controllers\Migrations\Data;

use App\ConsumerCampaign;
use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;

class ConsumerCampaignsData{
    public function handle($filename){
        $campaigns = ConsumerCampaign::with('consumer')->has('consumer')->get();
        $campaigns_d = collect(ResolveJson::decodeFile(base_path('stubs/exports/campaigns.json')));
        $users = collect(ResolveJson::decodeFile(base_path('stubs/exports/users.json')));
        $data = [];
        foreach($campaigns as $campaign){
            $campaigns_dd = $campaigns_d->firstWhere('id', $campaign->campaign_id);
            $user = $users->firstWhere('id', $campaign->consumer->user_id);
            if($campaigns_dd && $user){
                $data[] = [
                    'id'=> $campaign->id,
                    'campaign_id'=> $campaign->campaign_id,
                    'user_id'=> $campaign->consumer->user_id,
                    'has_feedback'=> $campaign->has_feedback,
                    'created_at'=> $campaign->created_at,
                    'updated_at'=> $campaign->updated_at,
                ];
            }
        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'campaign_id', 'user_id', 'has_feedback','created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
