<?php
namespace App\Http\Controllers\Migrations\Data;

use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;
use App\CampaignMatchingData;

class CampaignMatchingTagsData{
    public function handle($filename){
        $camp_matching_tags = CampaignMatchingData::get();
        $data = [];

        $campaigns = collect(ResolveJson::decodeFile(base_path('stubs/exports/campaigns.json')));
        $matching_collections = collect(ResolveJson::decodeFile(base_path('stubs/exports/matching_collections.json')));
        $matching_tags = collect(ResolveJson::decodeFile(base_path('stubs/exports/matching_tags.json')));

        foreach($camp_matching_tags as $camp_matching_tag){
            $campaign = $campaigns->firstWhere('id', $camp_matching_tag->campaign_id);

            $matching_collection = $matching_collections->firstWhere('id', $camp_matching_tag->matching_tag_id);

            $matching_tag = $matching_tags->firstWhere('id', $camp_matching_tag->matching_data_id);

            if($campaign && $matching_collection && $matching_tag){
                $data[] = [
                    'id'=> $camp_matching_tag->id,
                    'campaign_id'=> $camp_matching_tag->campaign_id,
                    'matching_collection_id'=> $camp_matching_tag->matching_tag_id,
                    'matching_tag_id'=> $camp_matching_tag->matching_data_id,
                    'created_at'=> $camp_matching_tag->created_at,
                    'updated_at'=> $camp_matching_tag->updated_at,
                ];
            }
        }

        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'campaign_id', 'matching_collection_id','matching_tag_id', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
