<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Survey;
use App\MatchingTag;
use App\MatchingTagData;
use Illuminate\Support\Str;

class MatchingTagsData{
    public function handle($filename){
        $matching_conds = MatchingTagData::has('matching_tag')->get();
        $data = [];
        foreach ($matching_conds as $matching_cond) {
            $matchingTag = MatchingTag::where('id', $matching_cond->matching_tag_id)->first();
            $survey = Survey::where('id', $matchingTag->survey_id)->first();
            if ($survey) {
                $dd = json_decode($survey->survey, true);
                $fed = collect($dd['pages'][0]['elements'])->where('title', $matchingTag->title)->first();
                if (isset($fed['choices'])) {
                    //$jj = $dd['pages'][0]['elements'][0]['choices'];
                    $val = collect($fed['choices'])->where('text', $matching_cond->title)->first();

                    $data[] = [
                        'id' => $matching_cond->id,
                        'matching_collection_id' => $matching_cond->matching_tag_id,
                        'title' => $matching_cond->title,
                        'value' => !isset($val['value']) ? Str::slug($matching_cond->title) : $val['value'],
                        'image_link' => (isset($fed['type']) && $fed['type'] == 'imagepicker') ? (isset($val['imageLink']) ? $val['imageLink'] : "") : "",
                        'type' => isset($fed['type']) ? $fed['type'] : "",
                        'created_at' => $matching_cond->created_at,
                        'updated_at' => $matching_cond->updated_at,
                    ];
                }
            }
        }
        // $csvExporter = new \Laracsv\Export();
        // $frodo = collect($data);
        // $csvExporter->build($frodo, ['id', 'matching_collection_id', 'title', 'value', 'type', 'image_link']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
