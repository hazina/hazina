<?php
namespace App\Http\Controllers\Migrations\Data;

use JamesGordo\CSV\Parser;
use App\Applets\ResolveJson;
use App\ConsumerMatchingData;

class ConsumerMatchingTagsData{
    public function handle($filename){
        $consumer_matching_data = ConsumerMatchingData::has('consumer')->get();
        $users = collect(ResolveJson::decodeFile(base_path('stubs/exports/users.json')));
        $matching_tags = collect(ResolveJson::decodeFile(base_path('stubs/exports/matching_tags.json')));
        $data = [];
        foreach($consumer_matching_data as $matching){
            $user = $users->firstWhere('id', $matching->consumer->user_id);
            $matching_tag = $matching_tags->firstWhere('id', $matching->matching_data_id);
            if($user && $matching_tag){
                $data[] = [
                    'id'=> $matching->id,
                    'user_id'=> $matching->consumer->user_id,
                    'matching_tag_id'=> $matching->matching_data_id,
                    'created_at'=> $matching->created_at,
                    'updated_at'=> $matching->updated_at,
                ];
            }
        }

        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id','user_id','matching_tag_id','created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
