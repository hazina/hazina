<?php
namespace App\Http\Controllers\Migrations\Data;

use App\User;
use App\Brand;
use App\Campaign;
use Carbon\Carbon;
use App\CampaignPage;
use JamesGordo\CSV\Parser;
use Illuminate\Support\Str;
use App\Applets\ResolveJson;

class BrandsData
{
    public function handle($filename)
    {
        $users = [];
        $main = [];
        $brands = [];
        $getConsumer = ResolveJson::decodeFile(base_path('stubs/exports/users.json'));
        $consumer = collect($getConsumer)->sortByDesc('id')->first();
        // $consumer = User::orderBy('id', 'desc')->first();
        $brandAll = Brand::get();
        $phones = [
            "+2348063905313", "+2348068370377", "+2348065760101", "+2348064792641", "+2348063368471", "+2348069622712", "+2348068492442", "+2348063058182", "+2348069223713", "+2348065825653", "+2348067535783", "+2348065968124", "+2348060656654", "+2348064545294", "+2348063031525", "+2348064654365", "+2348064109695", "+2348069856436", "+2348067123566", "+2348063892207", "+2348061232737", "+2348062093277", "+2348066324308", "+2348065750648", "+2348065835678", "+2348063776519", "+2348066343749", "+2348063816789", "+2348060316101", "+2348064106501", "+2348064981801", "+2348065096211", "+2348066805511", "+2348065832911", "+2348067694221", "+2348064171621", "+2348067731921", "+2348066201331", "+2348067892631", "+2348063218041", "+2348068796341", "+2348068822741", "+2348067571051", "+2348069935451", "+2348067524751", "+2348061136161", "+2348064026461", "+2348063292861", "+2348065372171", "+2348065635571",
        ];

        foreach ($brandAll as $key => $brand) {
            $iddd = intval($brand->id) + intval(collect($consumer)->get('id'));
            $users[] = [
                'id' => $iddd,
                'username' => Str::slug($brand->name),
                'email' => Str::slug($brand->name) . '@app.com',
                'password' => bcrypt('password'),
                'phone' => $phones[$key],
                'old_id' => $brand->id,
                'email_verified'=> false,
                'phone_verified'=> false,
            ];
            $main[] = [
                'id' => $iddd,
                'username' => Str::slug($brand->name),
                'email' => Str::slug($brand->name) . '@app.com',
                'phone' => $phones[$key],
                'password' => bcrypt('password'),
                'created_at'=> now(),
                'updated_at'=> now(),
                'email_verified'=> false,
                'phone_verified'=> false,
            ];

            $brands[] = [
                'id' => $brand->id,
                'user_id' => $iddd,
                'name' => $brand->name,
                'description' => $brand->description,
                'photo' => $brand->photo,
            ];
        }

        $data = [];
        $campaigns = Campaign::get();

        foreach ($campaigns as $campaign) {
            $brand = collect($users)->where('old_id', $campaign->brand_id)->first();
            $pages = CampaignPage::where('campaign_id', $campaign->id)->first();
            $page_data = collect(json_decode($pages->data, true));
            //$text = $page_data['long_description_content'] . ' <br>' . implode("," ,$page_data['feature_list']);
            $text = $page_data['long_description_content'];
            $data[] = [
                'id' => $campaign->id,
                'user_id' => $brand['id'],
                'category_id' => $campaign->category_id,
                'title' => $campaign->title,
                'ref' => Str::random(10),
                'short_description' => stripslashes($page_data['short_description']),
                'description' => stripslashes($text),
                'terms' => "",
                'banner' => $page_data->get('page_banner', ""),
                'display_picture' => $campaign->photo,
                'product_pictures' => json_encode($page_data['page_pictures']),
                'starts_on' => $campaign->starts_on,
                'ends_on' => $campaign->ends_on,
                'status' => $campaign->status,
                'amount' => 0,
                'created_at' => $campaign->created_at,
                'updated_at' => $campaign->updated_at,
            ];
        }

        // $csvExporter = new \Laracsv\Export();
        // $frodo = collect($data);
        // $csvExporter->build($frodo, ['id', 'user_id', 'category_id', 'title', 'ref', 'short_description', 'description', 'terms', 'banner', 'display_picture', 'product_pictures', 'starts_on', 'ends_on', 'status', 'amount', 'created_at', 'updated_at']);

        // $csvExporter2 = new \Laracsv\Export();
        // $frodo2 = collect($users);
        // $csvExporter2->build($frodo2, ['id', 'username', 'phone', 'email', 'password', 'email_verified', 'phone_verified'], [
        //     'header' => false,
        // ]);

        // $csvExporter3 = new \Laracsv\Export();
        // $frodo3 = collect($brands);
        // $csvExporter3->build($frodo3, ['id', 'user_id', 'name', 'description']);

        // $csvWriter = $csvExporter->getWriter();
        // $path = base_path('stubs/exports/campaigns.csv');
        // file_put_contents($path, $csvWriter);

        // $csvWriter2 = $csvExporter2->getWriter();
        // $path2 = base_path('stubs/exports/users.csv');
        // file_put_contents($path2, $csvWriter2, FILE_APPEND | LOCK_EX);

        // $csvWriter3 = $csvExporter3->getWriter();
        // $path3 = base_path('stubs/exports/brands.csv');
        // file_put_contents($path3, $csvWriter3);

        $JoinUsers =  array_merge($getConsumer, collect($main)->all());
        $path2 = base_path('stubs/exports/users.json');
        file_put_contents($path2, collect($JoinUsers)->toJson());

        $path_brand = base_path('stubs/exports/brands.json');
        file_put_contents($path_brand, collect($brands)->toJson());

        $path_campaign = base_path('stubs/exports/campaigns.json');
        file_put_contents($path_campaign, collect($data)->toJson());

    }

}
