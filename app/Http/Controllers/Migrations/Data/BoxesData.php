<?php

use App\Campaign;
use App\OutboundInventory;
use Illuminate\Support\Str;
use JamesGordo\CSV\Parser;

class BoxesData{
    public function handle($filename){
        $consumers_boxes = OutboundInventory::get()->groupBy('consumer_id');
        $users = collect(new Parser(base_path('stubs/exports/users.csv')));
        $shipping_addresses = collect(new Parser(base_path('stubs/exports/shipping_addresses.csv')));
        $campaigns = collect(new Parser(base_path('stubs/exports/campaigns.csv')));
        $boxes = [];
        $box_products = [];
        $ID = 0;
        foreach($consumers_boxes as $consumers_boxes_key => $consumers_box){
            $Allproducts = collect($consumers_box)->groupBy('created_at');

            foreach($Allproducts as $created_at => $Allproduct){
                //boxes
                $user = $users->firstWhere('id', $consumers_boxes_key);
                $addresses = $shipping_addresses->firstWhere('user_id', $user['id']);
                $lastProduct = collect($Allproduct)->last();
                $ID += 1;
                $fullfilmentsId = $ID;
                $boxes[] = [
                    'id'=> $ID,
                    'user_id'=> $user['id'],
                    'shipping_address_id'=> $addresses['id'],
                    'fulfillment_id'=> $fullfilmentsId,
                    'ref'=> Str::random(10),
                    'opened_on'=> $created_at,
                    'closed_on'=> $lastProduct['created_at'],
                    'status'=> 'CLOSED',
                    'delivery_status'=> 'DELIVERED' ,
                ];
                foreach($Allproducts as $product){
                    //product boxes]
                    $cam = Campaign::where('id', $product['campaign_id'])->first();
                    $box_products[] = [
                        'box_id'=> $ID,
                        'campaign_id'=> $cam->id,
                        'category_id'=> $cam->category_id,
                    ];
                }
            }
        }



    }
}
