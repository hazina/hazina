<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Applets\ResolveJson;
use App\Survey;
use App\Campaign;
use App\FeedbackSurvey;
use JamesGordo\CSV\Parser;

class FeedbackSurveysData{
    public function handle($filename){
        $feedback_surveys = FeedbackSurvey::get();
        $data = [];


        $campaigns = collect(ResolveJson::decodeFile(base_path('stubs/exports/campaigns.json')));

        $surveys = collect(ResolveJson::decodeFile(base_path('stubs/exports/surveys.json')));

        foreach($feedback_surveys as $feedback_survey){
            $campaign =  $campaigns->firstWhere('id', $feedback_survey->campaign_id);
            $survey = $surveys->firstWhere('id', $feedback_survey->survey_id);
            if($campaign && $survey){
                $data[] = [
                    'id'=> $feedback_survey->id,
                    'campaign_id'=> $feedback_survey->campaign_id,
                    'survey_id'=> $feedback_survey->survey_id,
                    'points'=> intval(collect($survey)->get('number_of_questions')) * 2,
                    'created_at'=> $feedback_survey->created_at,
                    'updated_at'=> $feedback_survey->updated_at,
                ];
            }
        }
        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build(collect($data), ['id', 'campaign_id', 'points', 'survey_id', 'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($data)->toJson());
    }
}
