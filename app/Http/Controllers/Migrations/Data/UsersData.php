<?php
namespace App\Http\Controllers\Migrations\Data;

use App\User;
use App\Consumer;
use App\OutboundInventory;
use Illuminate\Support\Str;
use App\ConsumerMatchingData;

class UsersData{
    public function handle($filename){
        $consumers = Consumer::get();
        $data = [];
        foreach($consumers as $consumer){
            $user = User::where('id', $consumer->user_id)->first();
            if($user){
                $checkPhone = User::where('phone', $user->phone)->count();
                $checkEmail = User::where('email', $user->email)->count();
                if($checkPhone < 2 && $checkEmail < 2){
                    $data[] = [
                        'id'=> $consumer->user_id,
                        //'consumer_id'=> $consumer->id,
                        'username'=> $user->username,
                        'email'=> $checkEmail > 1 ? Str::random(10) . '@yh.com' : $user->email,
                        'phone'=> $checkPhone > 1 ? Str::random(10) : strval($user->phone),
                        'password'=> $user->password,
                        'created_at'=> $consumer->created_at,
                        'updated_at'=> $consumer->updated_at,
                        'email_verified'=> is_null($user->email_verified) ? false : $user->email_verified,
                        'phone_verified'=> is_null($user->phone_verified) ? false : $user->phone_verified,
                        // 'outcount'=> OutboundInventory::where([['status', 'CLAIMED'], ['consumer_id', $consumer->id]])->count(),
                        // 'apps'=> User::where('phone', $user->phone)->count(),
                    ];
                }
            }

        }
        $ddd = collect($data);
        // $fred = $ddd->reject(function($value, $key){
        //     return ($value['outcount'] < 1 && $value['apps'] > 1) ? true : false;
        // });

        // $csvExporter = new \Laracsv\Export();
        // $csvExporter->build($ddd, ['id','username', 'phone', 'email', 'password', 'email_verified', 'phone_verified']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, $ddd->toJson());
    }
}
