<?php
namespace App\Http\Controllers\Migrations\Data;

use App\Category;

class CategoriesData{
    public function handle($filename){
        $categories = Category::get();
        $csvExporter = new \Laracsv\Export();
        // $csvExporter->build($categories, ['id','title','slug',  'created_at', 'updated_at']);
        // $csvWriter = $csvExporter->getWriter();
        $path = base_path($filename);
        file_put_contents($path, collect($categories)->toJson());
    }
}
