<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Freebie;
use App\Consumer;
use App\ConsumerFreeby;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReedemPointsCtrl extends Controller
{
    public function init_redeem_points(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_freebies = ConsumerFreeby::where('consumer_id', $consumer->id)->get();
        $data = Freebie::whereNotIn('id', $consumer_freebies->pluck('freebie_id'))->paginate(12);
        return response()->json(['data' => $data], 200);
    }

    public function redeem_points(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $freebie = Freebie::where('id', $request->id)->first();
        if ($consumer->points < $freebie->points) {
            return response()->json(['error' => 'You do not have sufficient points to get this freebie'], 500);
        } else {
            $freebie->decrement('quantity', 1);
            $consumer->decrement('points', $freebie->points);
            $consumer->freebies()->save(new ConsumerFreeby([
                'freebie_id' => $freebie->id,
                'status' => 'PENDING',
            ]));
            return response()->json(['message' => 'Freebie saved successfully!', 'user' => $consumer], 200);
        }

    }
}
