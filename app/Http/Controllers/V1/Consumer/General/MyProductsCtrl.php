<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Consumer;
use App\InventoryLog;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyProductsCtrl extends Controller
{
    public function init_my_products(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $outbound = OutboundInventory::where([['consumer_id', $consumer->id]])->get();
        $data = InventoryLog::with(['inbound_inventory' => function ($q) {
            $q->with("campaign");
        }])->whereIn('outbound_inventory_id', $outbound->pluck('id'))->paginate(20);
        return response()->json(['data' => $data], 200);
    }
}
