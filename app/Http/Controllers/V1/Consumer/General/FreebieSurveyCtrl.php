<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Consumer;
use App\DailySurvey;
use App\MatchingTagData;
use App\ConsumerDailySurvey;
use Illuminate\Http\Request;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;

class FreebieSurveyCtrl extends Controller
{
    public function load_freebie_survey(Request $request, $id)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $check = ConsumerDailySurvey::where([['daily_survey_id', $id], ['consumer_id', $consumer->id]])->first();
        $survey = DailySurvey::with('survey')->where('id', $id)->first();
        $taken = $check ? true : false;
        return response()->json(['survey' => $survey->survey, 'is_taken' => $taken], 200);
    }

    public function save_freebie_survey(Request $request)
    {
        $survey_data_entries = json_decode($request->survey, true);
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_tag_data = [];

        foreach ($survey_data_entries as $tag => $entries) {
            if (!is_array($entries)) {
                $matching_data = MatchingTagData::where('value', $entries)->first();
                if ($matching_data) {
                    $consumer_tag_data[] = [
                        'consumer_id' => $consumer->id,
                        'matching_tag_id' => $matching_data->matching_tag_id,
                        'matching_data_id' => $matching_data->id,
                    ];
                }
            } else {
                foreach ($entries as $entry) {
                    $matching_data = MatchingTagData::where('value', $entry)->first();
                    if ($matching_data) {
                        $consumer_tag_data[] = [
                            'consumer_id' => $consumer->id,
                            'matching_tag_id' => $matching_data->matching_tag_id,
                            'matching_data_id' => $matching_data->id,
                        ];
                    }
                }
            }
        }
        \App\ConsumerMatchingData::insert($consumer_tag_data);

        $consumer_surveys = new \App\ConsumerDailySurvey([
            'daily_survey_id' => $request->id,
        ]);

        $consumer->daily_surveys()->save($consumer_surveys);
        $point = DailySurvey::where('id', $request->id)->select('points')->first();
        $consumer->increment('points', $point->points);

        return response()->json(['user' => $consumer, 'points' => $point->points], 200);
    }

}
