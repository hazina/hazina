<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Consumer;
use App\Location;
use Carbon\Carbon;
use App\ShippingAddress;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Shipping\EditShippingAddress;

class ShippingCtrl extends Controller
{
    public function init_shipping(Request $request, $path)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        if ($path === "awaiting") {
            $outbound_inventories = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])
                ->orderBy('created_at', 'desc')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('F Y');
                });
                return response()->json(['data' => $outbound_inventories], 200);

        } elseif ($path === "delivered") {
            $outbound_inventories = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])
                ->orderBy('created_at', 'desc')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('F Y');
                });
                return response()->json(['data' => $outbound_inventories], 200);

        } elseif ($path === "profile") {
            $fData = [];
            $fData['shipping_details'] = ShippingAddress::where('user_id', $request->user->id)->first();
            $fData['locations'] = Location::with('children')->where([['parent_id', '!=', null],['is_visible', true]])->get();
            return response()->json(['data' => $fData], 200);
        }
    }

    public function update_shipping_address(EditShippingAddress $request)
    {
        if ($request->validated()) {
            $consumer = Consumer::where('user_id', $request->user->id)->first();
            $shipping_address = ShippingAddress::where('user_id', $request->user->id)->first();
            $country = Location::where('name', 'Nigeria')->first();

            $set_address = $request->street_number  . ' ' . $request->street_name . ' ' . $this->resolveLocation($request->area) . ', ' . $this->resolveLocation($request->lga) . ' L.G.A, ' .  $this->resolveLocation($request->state) . ' State ' . $country->name;

            $shipping_address->state = $request->state;
            $shipping_address->full_address = $set_address;
            $shipping_address->street_name = $request->street_name;
            $shipping_address->street_number = $request->street_number;
            $shipping_address->lga = !$request->lga ? $shipping_address->lga : $request->lga;
            $shipping_address->landmark = !$request->landmark ? $shipping_address->landmark : $request->landmark;
            $shipping_address->area = !$request->area ? $shipping_address->area : $request->area;
            $shipping_address->is_home_address =  $request->is_home_address;
            $shipping_address->save();
            return response()->json(['shipping_detail' => $shipping_address, 'state'=> $this->resolveLocation($request->state)], 200);
        }
    }

    public function resolveLocation($id){
        $loc = Location::where('id', $id)->first();
        return $loc->name;
    }
}
