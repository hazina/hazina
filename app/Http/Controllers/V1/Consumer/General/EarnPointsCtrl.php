<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Consumer;
use Carbon\Carbon;
use App\DailySurvey;
use App\FeedbackSurvey;
use App\ConsumerCampaign;
use App\ConsumerDailySurvey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OutboundInventory;

class EarnPointsCtrl extends Controller
{
    public function init_earn_points(Request $request, $path)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        if($path === "daily"){
            $data = [];
            $consumer_daily = ConsumerDailySurvey::with('daily_survey')->where('consumer_id', $consumer->id)->latest()->first();
            if (!$consumer_daily) {
                $data['daily_surveys'] = DailySurvey::with('survey:id,title')->orderBy('level', 'asc')->limit(3)->get();
                $data['can_play'] = true;
                $data['expiry'] = null;
            } else {
                $getLevel = intval($consumer_daily->daily_survey->level);
                $now = Carbon::now();
                $exp = Carbon::parse($consumer_daily->created_at)->addMinutes(4);
                if ($exp->gt($now)) {
                    $data['can_play'] = false;
                    $data['expiry'] = $exp->toDateTimeString();
                    $data['start'] = $now->toDateTimeString();
                } else {
                    $data['can_play'] = true;
                    $data['expiry'] = null;
                }
                $data['daily_surveys'] = DailySurvey::with('survey:id,title')->where('level', '>', $getLevel)->orderBy('level', 'asc')->limit(3)->get();
            }
            return response()->json(['data'=> $data], 200);
        }elseif($path === "feedback"){
            $outboundinventories = OutboundInventory::where([['consumer_id', $consumer->id],['status', '=', 'CLAIMED']])->get();
            $my_campaigns_feedback = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', false]])->whereIn('campaign_id', $outboundinventories->pluck('campaign_id'))->get();
            //$data['feedback_surveys'] = FeedbackSurvey::with(['survey:id', 'campaign'])->whereIn('campaign_id', $my_campaigns_feedback->pluck('campaign_id'))->get();
            $data['feedback_surveys'] = FeedbackSurvey::with(['survey:id,number_of_questions', 'campaign'])->whereIn('campaign_id', $my_campaigns_feedback->pluck('campaign_id'))->get();
            return response()->json(['data'=> $data], 200);
        }
    }
}
