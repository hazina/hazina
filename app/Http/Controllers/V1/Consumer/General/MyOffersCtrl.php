<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Campaign;
use App\Consumer;
use App\InventoryLog;
use App\ShippingAddress;
use App\ConsumerCampaign;
use App\InboundInventory;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyOffersCtrl extends Controller
{

    public function load_offer_page(Request $request, $id)
    {
        $campaign = Campaign::with(['page', 'brand'])->where('id', $id)->first();
        logger($request);
        $check = ShippingAddress::where('user_id', $request->user->id)->first();
        $qq = is_null($check->full_address) ? false : true;
        return response()->json(['data' => $campaign, 'can_accept'=> $qq], 200);
    }

    public function fetch_my_offers(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        //$my_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', true]])->get();
        $my_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id]])->get();
        $data = Campaign::whereIn('id', $my_campaigns->pluck('campaign_id'))->get();
        return response()->json(['data' => $data], 200);
    }



}
