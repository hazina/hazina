<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Survey;
use App\Freebie;
use App\BannerAd;
use App\Campaign;
use App\Category;
use App\Consumer;
use Carbon\Carbon;
use App\InventoryLog;
use App\ConsumerFreeby;
use App\FeedbackSurvey;
use App\ConsumerCampaign;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;

class DashboardCtrl extends Controller
{
    public function get_matched_campaigns($consumer)
    {
        /**
         * Lets get matching campaign
         */
        $consumer_matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        $matched_campaign = Campaign::with(['matching_data'])->where('status', 'OPEN')->get()->filter(function ($value, $key) use ($consumer_matching_data) {
            //group by matching tag
            $get_tag_group_count = $value->matching_data->groupBy('matching_tag_id')->count();
            $group_by_campaign_matching_tag = $value->matching_data->groupBy('matching_tag_id')->all();
            $consumer = $consumer_matching_data->pluck('matching_data_id')->all();
            $matches = [];
            $match_count = 0;
            foreach (collect($group_by_campaign_matching_tag) as $gg) {
                $intersect = array_intersect($gg->pluck('matching_data_id')->all(), $consumer);
                if (count($intersect) > 0) {
                    // logger($gg->pluck('matching_data_id')->all());
                    // logger($consumer);
                    // logger($intersect);
                    $matches[] = 1;
                    $match_count += 1;
                }
            }
            // logger(count($matches));
            // logger($match_count);
            return $match_count === $get_tag_group_count;
        });

        // $matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        // $campaign_matching_data = CampaignMatchingData::whereIn('matching_data_id', $matching_data->pluck('matching_data_id'))->get();
        // $matched_campaign = Campaign::with('brand')->whereIn('id', $campaign_matching_data->pluck('campaign_id'))->where('status', 'OPEN');

        /**
         * Lets filter out the campaigns the user has not taken before
         */
        $consumer_campaigns = ConsumerCampaign::where('consumer_id', $consumer->id)->get();
        $filter_not_taken = $matched_campaign->whereNotIn('id', $consumer_campaigns->pluck('campaign_id'));

        /**
         *  Lets get the campaign categories the user has not claimed
         */
        $user_inventories = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
        $logs = InventoryLog::whereIn('outbound_inventory_id', $user_inventories->pluck('id'))->get();
        $final_filter = $filter_not_taken->whereNotIn('category_id', $logs->pluck('category_id'));
        $cc = Campaign::with('brand')->whereIn('id', $final_filter->pluck('id'))->get();
        return $cc;
    }

    public function load_home(Request $request)
    {
        $data = [];
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $get_claimed_count = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->count();
        $data_categories = $this->get_matched_campaigns($consumer)->pluck('category_id');
        $data['new_offers'] = $get_claimed_count >= 3 ? [] : Category::whereIn('id', $data_categories)->get();

        //My Offers
        $my_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id]])->get();
        $data['my_offers'] = Campaign::with('brand')->whereIn('id', $my_campaigns->pluck('campaign_id'))->limit(5)->get();

        //Feedback surveys
        // $data['feedback_surveys'] = Survey::whereIn('id', $feedback_surveys->pluck('survey_id'))->get();
        // $my_campaigns_feedback = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', false]])->get();
        $get_accesible_feedback = OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->get();
        $without_feedbacks = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', false]])->whereIn('campaign_id', $get_accesible_feedback->pluck('campaign_id'))->get();
        $data['feedback_surveys'] = FeedbackSurvey::with('survey')->whereIn('campaign_id', $without_feedbacks->pluck('campaign_id'))->limit(4)->get();

        //Latest Shipping
        // $outbound = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->first();
        $outbounds = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', '==', 'CLAIMED']])
            ->orderBy('created_at', 'desc')
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->created_at)->format('F Y');
            });
        $data['latest_shipping'] = $outbounds;

        //Awaiting Delivery
        $awaiting_outbound = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
        $data['awaiting_deliveries'] = $awaiting_outbound;

        //Reedem Points
        // $consumer_freebies = ConsumerFreeby::whereIn('consumer_id',  $user_surveys_taken->pluck('survey_id'))->select('survey_id')->get();
        $data['freebies'] = Freebie::limit(3)->get();
        $data['user_acl'] = $this->setAcl($request);
        $data['banner_ads'] = BannerAd::get();
        if ($request->query('action') && $request->query('catId')) {
            $repD = Campaign::with('brand')->where('category_id', $request->query('catId'))->get();
            $repDD = [
                'category' => Category::where('id', $request->query('catId'))->first(),
                'campaigns' => $repD,
            ];
            return response()->json(['data' => $repDD], 200);
        }
        return response()->json(['data' => $data], 200);
    }

    public function setAcl($request)
    {
        $user = Consumer::where('user_id', $request->user->id)->first();
        // $Als = ['BASIC','PHONE', 'SURVEY', 'EMAIL']; //PHONE
        // $status = array_search($user->registration_status, $Als);
        // return $status;
        return $user->registration_status;
    }
}
