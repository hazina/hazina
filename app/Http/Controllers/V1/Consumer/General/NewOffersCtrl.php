<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Campaign;
use App\Consumer;
use App\InventoryLog;
use App\ConsumerCampaign;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;

class NewOffersCtrl extends Controller
{
    public function fetch_new_offers(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $id = $this->get_matched_campaigns($consumer);
        $data = Campaign::with('brand')->whereIn('id', $id->pluck('id'))->get();
        return response()->json(['data' => $data], 200);
    }

    public function get_matched_campaigns($consumer)
    {
        /**
         * Lets get matching campaign
         */
        $consumer_matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        $matched_campaign = Campaign::with(['matching_data'])->where('status', 'OPEN')->get()->filter(function ($value, $key) use ($consumer_matching_data) {
            //group by matching tag
            $get_tag_group_count = $value->matching_data->groupBy('matching_tag_id')->count();
            $group_by_campaign_matching_tag = $value->matching_data->groupBy('matching_tag_id')->all();
            $consumer = $consumer_matching_data->pluck('matching_data_id')->all();
            $matches = [];
            $match_count = 0;
            foreach (collect($group_by_campaign_matching_tag) as $gg) {
                $intersect = array_intersect($gg->pluck('matching_data_id')->all(), $consumer);
                if (count($intersect) > 0) {
                    // logger($gg->pluck('matching_data_id')->all());
                    // logger($consumer);
                    // logger($intersect);
                    $matches[] = 1;
                    $match_count += 1;
                }
            }
            // logger(count($matches));
            // logger($match_count);
            return $match_count === $get_tag_group_count;
        });

        // $matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        // $campaign_matching_data = CampaignMatchingData::whereIn('matching_data_id', $matching_data->pluck('matching_data_id'))->get();
        // $matched_campaign = Campaign::with('brand')->whereIn('id', $campaign_matching_data->pluck('campaign_id'))->where('status', 'OPEN');

        /**
         * Lets filter out the campaigns the user has not taken before
         */
        $consumer_campaigns = ConsumerCampaign::where('consumer_id', $consumer->id)->get();
        $filter_not_taken = $matched_campaign->whereNotIn('id', $consumer_campaigns->pluck('campaign_id'));

        /**
         *  Lets get the campaign categories the user has not claimed
         */
        $user_inventories = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
        $logs = InventoryLog::whereIn('outbound_inventory_id', $user_inventories->pluck('id'))->get();
        $final_filter = $filter_not_taken->whereNotIn('category_id', $logs->pluck('category_id'));
        $cc = Campaign::with('brand')->whereIn('id', $final_filter->pluck('id'))->get();
        return $cc;
    }
}
