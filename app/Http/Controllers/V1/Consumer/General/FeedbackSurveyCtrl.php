<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Consumer;
use App\FeedbackSurvey;
use App\MatchingTagData;
use App\ConsumerCampaign;
use Illuminate\Http\Request;
use App\ConsumerMatchingData;
use App\ConsumerFeedbackSurvey;
use App\Http\Controllers\Controller;

class FeedbackSurveyCtrl extends Controller
{

    public function load_feedback_survey(Request $request, $id)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $check = ConsumerFeedbackSurvey::where([['feedback_survey_id', $id], ['consumer_id', $consumer->id]])->first();
        $survey = FeedbackSurvey::with('survey')->where('id', $id)->first();
        $taken = $check ? true : false;
        return response()->json(['survey' => $survey->survey, 'is_taken' => $taken], 200);
    }

    public function save_feedback_survey(Request $request)
    {
        $survey_data_entries = json_decode($request->survey, true);
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_tag_data = [];

        foreach ($survey_data_entries as $tag => $entries) {
            if (!is_array($entries)) {
                $matching_data = MatchingTagData::where('value', $entries)->first();
                if ($matching_data) {
                    $consumer_tag_data[] = [
                        'consumer_id' => $consumer->id,
                        'matching_tag_id' => $matching_data->matching_tag_id,
                        'matching_data_id' => $matching_data->id,
                    ];
                }
            } else {
                foreach ($entries as $entry) {
                    $matching_data = MatchingTagData::where('value', $entry)->first();
                    if ($matching_data) {
                        $consumer_tag_data[] = [
                            'consumer_id' => $consumer->id,
                            'matching_tag_id' => $matching_data->matching_tag_id,
                            'matching_data_id' => $matching_data->id,
                        ];
                    }
                }
            }
        }
        \App\ConsumerMatchingData::insert($consumer_tag_data);

        $consumer_surveys = new \App\ConsumerFeedbackSurvey([
            'feedback_survey_id' => $request->id,
        ]);

        $feedback = FeedBackSurvey::with('survey')->where('id', $request->id)->first();

        $consumer_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id], ['campaign_id', $feedback->campaign_id]])->first();

        $consumer_campaigns->has_feedback = true;
        $consumer_campaigns->save();

        $consumer->feedback_surveys()->save($consumer_surveys);
        // $feedback->decrement('quantity', 1);
        $points = intval($feedback->survey->number_of_questions) * 2;
        $consumer->increment('points', $points);

        return response()->json(['user' => $consumer, 'points' => $points], 200);
    }
}
