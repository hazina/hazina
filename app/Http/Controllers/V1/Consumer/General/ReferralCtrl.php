<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\User;
use App\Consumer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferralCtrl extends Controller
{
    public function load_referrals(Request $request){
        $user = User::find($request->user->id);
        $referrals = User::where('referrer', $user->ref)->get();
        $All = Consumer::whereIn('user_id', $referrals->pluck('id'))->get();
        if(!$request->page){
            // $Allrefs = $All->filter(function($q){
            //     $stages = collect(["BASIC","PHONE","ACCOUNT","SURVEY"]); //no EMAIL
            //     $user_reg_status = json_decode($q->registration_status, true);
            //     $res = $stages->diff($user_reg_status);
            //     return count($res->all()) == 0;
            // });
            $Allrefs = $All->where('is_reg_complete', true)->count();
            // $Pendingrefs = $All->filter(function($q){
            //     $stages = collect(["BASIC","PHONE","ACCOUNT","SURVEY"]); //no EMAIL
            //     $user_reg_status = json_decode($q->registration_status, true);
            //     $res = $stages->diff($user_reg_status);
            //     return count($res->all()) > 0;
            // });
            $Pendingrefs = $All->where('is_reg_complete', false)->count();

            $data = [];
            $data['total_ref'] = $Allrefs;
            $data['pending_points'] = $Pendingrefs * 20;
            $data['number_ref_points'] = $Allrefs * 20;
            return response()->json(['data' => $data], 200);
        }else{
            $All = Consumer::whereIn('user_id', $referrals->pluck('id'))->paginate(15);
            return response()->json(['data' => $All], 200);
        }

    }
}
