<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\Campaign;
use App\Consumer;
use App\InventoryLog;
use App\ConsumerCampaign;
use App\InboundInventory;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcceptOfferCtrl extends Controller
{
    public function accept_offer(Request $request, $id)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $campaign = Campaign::with(['inbound_inventories'])->where([['id', $id], ['status', 'OPEN']])->first();
        if ($campaign) {
            $check = ConsumerCampaign::where([['consumer_id', $consumer->id], ['campaign_id', $campaign->id]])->first();
            $quantityCheck = InboundInventory::where('campaign_id', $campaign->id)->sum('quantity');
            if(!$check && $quantityCheck > 0){
            $consumer_campaign = new ConsumerCampaign();
            $consumer_campaign->consumer_id = $consumer->id;
            $consumer_campaign->campaign_id = $campaign->id;
            $consumer_campaign->save();
            //save outbound
            $outbound = new OutboundInventory();
            $outbound->consumer_id = $consumer->id;
            $outbound->shipping_address_id = $consumer->id;
            $outbound->last_sample_added_on = now();
            $outbound->status = "QUEUED";
            $outbound->campaign_id = $campaign->id;
            $outbound->save();
            //save inventory log
            $inventory_log = new InventoryLog();
            $inventory_log->outbound_inventory_id = $outbound->id;
            $inventory_log->inbound_inventory_id = $campaign->inbound_inventories->id;
            $inventory_log->category_id = $campaign->category_id;
            $inventory_log->save();
            InboundInventory::where('campaign_id', $campaign->id)->decrement('quantity', 1);
            return response()->json(['data' => $campaign], 200);
            }elseif($quantityCheck < 1){
                $campaign->status = "ENDED";
                $campaign->save();
                return response()->json(['error' => 'Oops! This Campaign is closed!'], 500);
            }else{
                return response()->json(['error' => 'You have already accepted this offer!'], 500);
            }

        } else {
            return response()->json(['error' => 'Campaign does not exist or is expired'], 500);
        }

    }
}
