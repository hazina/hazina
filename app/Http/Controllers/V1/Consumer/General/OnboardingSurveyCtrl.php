<?php

namespace App\Http\Controllers\V1\Consumer\General;

use App\User;
use App\Campaign;
use App\Consumer;
use App\MatchingTagData;
use App\OnboardingSurvey;
use Illuminate\Http\Request;
use App\ConsumerMatchingData;
use App\ConsumerOnboardSurvey;
use App\Events\Onboard\Notify;
use App\Http\Controllers\Controller;

class OnboardingSurveyCtrl extends Controller
{
    public function load_onboard_survey(Request $request)
    {
        $survey = OnboardingSurvey::with('survey')->where('id', 1)->inRandomOrder()->first();
        return response()->json(['data' => $survey], 200);
    }

    public function save_onboarding_survey(Request $request)
    {
        $survey_data_entries = json_decode($request->survey, true);
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_tag_data = [];

        foreach ($survey_data_entries as $tag => $entries) {
            if (!is_array($entries)) {
                $matching_data = MatchingTagData::where('value', $entries)->first();
                if ($matching_data) {
                    $consumer_tag_data[] = [
                        'consumer_id' => $consumer->id,
                        'matching_tag_id' => $matching_data->matching_tag_id,
                        'matching_data_id' => $matching_data->id,
                    ];
                }
            } else {
                foreach ($entries as $entry) {
                    $matching_data = MatchingTagData::where('value', $entry)->first();
                    if ($matching_data) {
                        $consumer_tag_data[] = [
                            'consumer_id' => $consumer->id,
                            'matching_tag_id' => $matching_data->matching_tag_id,
                            'matching_data_id' => $matching_data->id,
                        ];
                    }
                }
            }
        }
        \App\ConsumerMatchingData::insert($consumer_tag_data);

        $consumer->onboard_survey()->save(new ConsumerOnboardSurvey([
            'onboarding_survey_id' => $request->id,
        ]));

        $reg_stat = collect(json_decode($consumer->registration_status, true));
        $consumer->registration_status = $reg_stat->push('SURVEY')->toJson();
        $consumer->is_reg_complete = true; //update
        $consumer->save();

        $id = $this->get_matched_campaigns($consumer);
        $data = Campaign::with('brand')->whereIn('id', $id->pluck('id'))->get();
        $isMatched = $data->isEmpty() ? false : true;
        if(!is_null($request->user->referrer)){
            $ref = User::where('ref', $request->user->referrer)->first();
            Consumer::where('user_id', $ref->id)->increment('points', 20);
            Consumer::where('user_id', $ref->id)->increment('wk2_points', 20);
        }
        event(new Notify($consumer, $data,  $isMatched));
        return response()->json(['message' => 'Survey Saved!'], 200);
    }
}
