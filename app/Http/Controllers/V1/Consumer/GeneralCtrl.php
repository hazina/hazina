<?php

namespace App\Http\Controllers\V1\Consumer;

use App\BannerAd;
use App\User;
use App\Survey;
use App\Freebie;
use App\Campaign;
use App\Category;
use App\Consumer;
use Carbon\Carbon;
use App\DailySurvey;
use App\InventoryLog;
use App\ConsumerFreeby;
use App\FeedbackSurvey;
use App\MatchingTagData;
use App\ShippingAddress;
use App\ConsumerCampaign;
use App\InboundInventory;
use App\OnboardingSurvey;
use App\OutboundInventory;
use App\ConsumerDailySurvey;
use Illuminate\Http\Request;
use App\ConsumerMatchingData;
use App\ConsumerOnboardSurvey;
use App\Events\Onboard\Notify;
use App\ConsumerFeedbackSurvey;
use App\Http\Controllers\Controller;
use App\Http\Requests\Shipping\EditShippingAddress;
use App\Location;

class GeneralCtrl extends Controller
{

    public function get_matched_campaigns($consumer)
    {
        /**
         * Lets get matching campaign
         */
        $consumer_matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        $matched_campaign = Campaign::with(['matching_data'])->where('status', 'OPEN')->get()->filter(function ($value, $key) use ($consumer_matching_data) {
            //group by matching tag
            $get_tag_group_count = $value->matching_data->groupBy('matching_tag_id')->count();
            $group_by_campaign_matching_tag = $value->matching_data->groupBy('matching_tag_id')->all();
            $consumer = $consumer_matching_data->pluck('matching_data_id')->all();
            $matches = [];
            $match_count = 0;
            foreach (collect($group_by_campaign_matching_tag) as $gg) {
                $intersect = array_intersect($gg->pluck('matching_data_id')->all(), $consumer);
                if (count($intersect) > 0) {
                    // logger($gg->pluck('matching_data_id')->all());
                    // logger($consumer);
                    // logger($intersect);
                    $matches[] = 1;
                    $match_count += 1;
                }
            }
            // logger(count($matches));
            // logger($match_count);
            return $match_count === $get_tag_group_count;
        });

        // $matching_data = ConsumerMatchingData::where('consumer_id', $consumer->id)->get();
        // $campaign_matching_data = CampaignMatchingData::whereIn('matching_data_id', $matching_data->pluck('matching_data_id'))->get();
        // $matched_campaign = Campaign::with('brand')->whereIn('id', $campaign_matching_data->pluck('campaign_id'))->where('status', 'OPEN');

        /**
         * Lets filter out the campaigns the user has not taken before
         */
        $consumer_campaigns = ConsumerCampaign::where('consumer_id', $consumer->id)->get();
        $filter_not_taken = $matched_campaign->whereNotIn('id', $consumer_campaigns->pluck('campaign_id'));

        /**
         *  Lets get the campaign categories the user has not claimed
         */
        $user_inventories = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
        $logs = InventoryLog::whereIn('outbound_inventory_id', $user_inventories->pluck('id'))->get();
        $final_filter = $filter_not_taken->whereNotIn('category_id', $logs->pluck('category_id'));
        $cc = Campaign::with('brand')->whereIn('id', $final_filter->pluck('id'))->get();
        return $cc;
    }

    public function load_home(Request $request)
    {
        $data = [];
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $get_claimed_count = OutboundInventory::where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->count();
        $data_categories =  $this->get_matched_campaigns($consumer)->pluck('category_id');
        $data['new_offers'] = $get_claimed_count >= 3 ? [] : Category::whereIn('id', $data_categories)->get();

        //My Offers
        $my_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id]])->get();
        $data['my_offers'] = Campaign::with('brand')->whereIn('id', $my_campaigns->pluck('campaign_id'))->limit(5)->get();

        //Feedback surveys
        // $data['feedback_surveys'] = Survey::whereIn('id', $feedback_surveys->pluck('survey_id'))->get();
       // $my_campaigns_feedback = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', false]])->get();
       $get_accesible_feedback = OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->get();
        $data['feedback_surveys'] = FeedbackSurvey::with('survey')->whereIn('campaign_id', $get_accesible_feedback->pluck('campaign_id'))->limit(4)->get();

        //Latest Shipping
       // $outbound = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->first();
        $outbounds = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', '==', 'CLAIMED']])
                ->orderBy('created_at', 'desc')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('F Y');
                });
        $data['latest_shipping'] = $outbounds;

        //Awaiting Delivery
        $awaiting_outbound = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])->get();
        $data['awaiting_deliveries'] = $awaiting_outbound;

        //Reedem Points
        // $consumer_freebies = ConsumerFreeby::whereIn('consumer_id',  $user_surveys_taken->pluck('survey_id'))->select('survey_id')->get();
        $data['freebies'] = Freebie::limit(3)->get();
        $data['user_acl'] = $this->setAcl($request);
        $data['banner_ads'] = BannerAd::get();
        if($request->query('action') && $request->query('catId')){
            $repD = Campaign::with('brand')->where('category_id', $request->query('catId'))->get();
            $repDD = [
                'category'=> Category::where('id', $request->query('catId'))->first(),
                'campaigns'=> $repD
            ];
            return response()->json(['data' => $repDD], 200);
        }
        return response()->json(['data' => $data], 200);
    }

    public function fetch_new_offers(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $id = $this->get_matched_campaigns($consumer);
        $data = Campaign::with('brand')->whereIn('id', $id->pluck('id'))->get();
        return response()->json(['data' => $data], 200);
    }

    public function fetch_my_offers(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        //$my_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', true]])->get();
        $my_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id]])->get();
        $data = Campaign::whereIn('id', $my_campaigns->pluck('campaign_id'))->get();
        return response()->json(['data' => $data], 200);
    }

    public function init_earn_points(Request $request, $path)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        if($path === "daily"){
            $data = [];
            $consumer_daily = ConsumerDailySurvey::with('daily_survey')->where('consumer_id', $consumer->id)->latest()->first();
            if (!$consumer_daily) {
                $data['daily_surveys'] = DailySurvey::with('survey:id,title')->orderBy('level', 'asc')->limit(3)->get();
                $data['can_play'] = true;
                $data['expiry'] = null;
            } else {
                $getLevel = intval($consumer_daily->daily_survey->level);
                $now = Carbon::now();
                $exp = Carbon::parse($consumer_daily->created_at)->addMinutes(4);
                if ($exp->gt($now)) {
                    $data['can_play'] = false;
                    $data['expiry'] = $exp->toDateTimeString();
                    $data['start'] = $now->toDateTimeString();
                } else {
                    $data['can_play'] = true;
                    $data['expiry'] = null;
                }
                $data['daily_surveys'] = DailySurvey::with('survey:id,title')->where('level', '>', $getLevel)->orderBy('level', 'asc')->limit(3)->get();
            }
            return response()->json(['data'=> $data], 200);
        }elseif($path === "feedback"){
            $my_campaigns_feedback = ConsumerCampaign::where([['consumer_id', $consumer->id], ['has_feedback', false]])->get();

            $data['feedback_surveys'] = FeedbackSurvey::with('survey')->whereIn('campaign_id', $my_campaigns_feedback->pluck('campaign_id'))->paginate(10);
            return response()->json(['data'=> $data], 200);
        }
    }

    public function init_my_products(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $outbound = OutboundInventory::where([['consumer_id', $consumer->id]])->get();
        $data = InventoryLog::with(['inbound_inventory' => function ($q) {
            $q->with("campaign");
        }])->whereIn('outbound_inventory_id', $outbound->pluck('id'))->paginate(20);
        return response()->json(['data' => $data], 200);
    }

    public function init_redeem_points(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_freebies = ConsumerFreeby::where('consumer_id', $consumer->id)->get();
        $data = Freebie::whereNotIn('id', $consumer_freebies->pluck('freebie_id'))->paginate(12);
        return response()->json(['data' => $data], 200);
    }

    public function redeem_points(Request $request)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $freebie = Freebie::where('id', $request->id)->first();
        if ($consumer->points < $freebie->points) {
            return response()->json(['error' => 'You do not have sufficient points to get this freebie'], 500);
        } else {
            $freebie->decrement('quantity', 1);
            $consumer->decrement('points', $freebie->points);
            $consumer->freebies()->save(new ConsumerFreeby([
                'freebie_id' => $freebie->id,
                'status' => 'PENDING',
            ]));
            return response()->json(['message' => 'Freebie saved successfully!', 'user' => $consumer], 200);
        }

    }

    public function init_shipping(Request $request, $path)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        if ($path === "awaiting") {
            $outbound_inventories = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', '!=', 'CLAIMED']])
                ->orderBy('created_at', 'desc')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('F Y');
                });
                return response()->json(['data' => $outbound_inventories], 200);

        } elseif ($path === "delivered") {
            $outbound_inventories = OutboundInventory::with('campaign')->where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])
                ->orderBy('created_at', 'desc')
                ->get()
                ->groupBy(function ($val) {
                    return Carbon::parse($val->created_at)->format('F Y');
                });
                return response()->json(['data' => $outbound_inventories], 200);

        } elseif ($path === "profile") {
            $fData = [];
            $fData['shipping_details'] = ShippingAddress::where('user_id', $request->user->id)->first();
            $fData['locations'] = Location::with('children')->where([['parent_id', '!=', null],['is_visible', true]])->get();
            return response()->json(['data' => $fData], 200);
        }
    }

    public function update_shipping_address(EditShippingAddress $request)
    {
        if ($request->validated()) {
            $consumer = Consumer::where('user_id', $request->user->id)->first();
            $shipping_address = ShippingAddress::where('user_id', $request->user->id)->first();
            $country = Location::where('name', 'Nigeria')->first();

            $set_address = $request->street_number  . ' ' . $request->street_name . ' ' . $this->resolveLocation($request->area) . ', ' . $this->resolveLocation($request->lga) . ' L.G.A, ' .  $this->resolveLocation($request->state) . ' State ' . $country->name;

            $shipping_address->state = $request->state;
            $shipping_address->full_address = $set_address;
            $shipping_address->street_name = $request->street_name;
            $shipping_address->street_number = $request->street_number;
            $shipping_address->lga = !$request->lga ? $shipping_address->lga : $request->lga;
            $shipping_address->landmark = !$request->landmark ? $shipping_address->landmark : $request->landmark;
            $shipping_address->area = !$request->area ? $shipping_address->area : $request->area;
            $shipping_address->save();
            return response()->json(['shipping_detail' => $shipping_address, 'state'=> $this->resolveLocation($request->state)], 200);
        }
    }

    public function resolveLocation($id){
        $loc = Location::where('id', $id)->first();
        return $loc->name;
    }

    public function setAcl($request)
    {
        $user = Consumer::where('user_id', $request->user->id)->first();
        // $Als = ['BASIC','PHONE', 'SURVEY', 'EMAIL']; //PHONE
        // $status = array_search($user->registration_status, $Als);
        // return $status;
        return $user->registration_status;
    }

    public function load_onboard_survey(Request $request)
    {
        $survey = OnboardingSurvey::with('survey')->where('id', 1)->inRandomOrder()->first();
        return response()->json(['data' => $survey], 200);
    }

    public function save_onboarding_survey(Request $request)
    {
        $survey_data_entries = json_decode($request->survey, true);
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_tag_data = [];

        foreach ($survey_data_entries as $tag => $entries) {
            if (!is_array($entries)) {
                $matching_data = MatchingTagData::where('value', $entries)->first();
                if ($matching_data) {
                    $consumer_tag_data[] = [
                        'consumer_id' => $consumer->id,
                        'matching_tag_id' => $matching_data->matching_tag_id,
                        'matching_data_id' => $matching_data->id,
                    ];
                }
            } else {
                foreach ($entries as $entry) {
                    $matching_data = MatchingTagData::where('value', $entry)->first();
                    if ($matching_data) {
                        $consumer_tag_data[] = [
                            'consumer_id' => $consumer->id,
                            'matching_tag_id' => $matching_data->matching_tag_id,
                            'matching_data_id' => $matching_data->id,
                        ];
                    }
                }
            }
        }
        \App\ConsumerMatchingData::insert($consumer_tag_data);

        $consumer->onboard_survey()->save(new ConsumerOnboardSurvey([
            'onboarding_survey_id' => $request->id,
        ]));

        $reg_stat = collect(json_decode($consumer->registration_status, true));
        $consumer->registration_status = $reg_stat->push('SURVEY')->toJson();
        $consumer->is_reg_complete = true; //update
        $consumer->save();

        $id = $this->get_matched_campaigns($consumer);
        $data = Campaign::with('brand')->whereIn('id', $id->pluck('id'))->get();
        $isMatched = $data->isEmpty() ? false : true;
        if(!is_null($request->user->referrer)){
            $ref = User::where('ref', $request->user->referrer)->first();
            Consumer::where('user_id', $ref->id)->increment('points', 20);
            Consumer::where('user_id', $ref->id)->increment('wk2_points', 20);
        }
        event(new Notify($consumer, $data,  $isMatched));
        return response()->json(['message' => 'Survey Saved!'], 200);
    }

    public function save_freebie_survey(Request $request)
    {
        $survey_data_entries = json_decode($request->survey, true);
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_tag_data = [];

        foreach ($survey_data_entries as $tag => $entries) {
            if (!is_array($entries)) {
                $matching_data = MatchingTagData::where('value', $entries)->first();
                if ($matching_data) {
                    $consumer_tag_data[] = [
                        'consumer_id' => $consumer->id,
                        'matching_tag_id' => $matching_data->matching_tag_id,
                        'matching_data_id' => $matching_data->id,
                    ];
                }
            } else {
                foreach ($entries as $entry) {
                    $matching_data = MatchingTagData::where('value', $entry)->first();
                    if ($matching_data) {
                        $consumer_tag_data[] = [
                            'consumer_id' => $consumer->id,
                            'matching_tag_id' => $matching_data->matching_tag_id,
                            'matching_data_id' => $matching_data->id,
                        ];
                    }
                }
            }
        }
        \App\ConsumerMatchingData::insert($consumer_tag_data);

        $consumer_surveys = new \App\ConsumerDailySurvey([
            'daily_survey_id' => $request->id,
        ]);

        $consumer->daily_surveys()->save($consumer_surveys);
        $point = DailySurvey::where('id', $request->id)->select('points')->first();
        $consumer->increment('points', $point->points);

        return response()->json(['user' => $consumer, 'points' => $point->points], 200);
    }

    public function save_feedback_survey(Request $request)
    {
        $survey_data_entries = json_decode($request->survey, true);
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $consumer_tag_data = [];

        foreach ($survey_data_entries as $tag => $entries) {
            if (!is_array($entries)) {
                $matching_data = MatchingTagData::where('value', $entries)->first();
                if ($matching_data) {
                    $consumer_tag_data[] = [
                        'consumer_id' => $consumer->id,
                        'matching_tag_id' => $matching_data->matching_tag_id,
                        'matching_data_id' => $matching_data->id,
                    ];
                }
            } else {
                foreach ($entries as $entry) {
                    $matching_data = MatchingTagData::where('value', $entry)->first();
                    if ($matching_data) {
                        $consumer_tag_data[] = [
                            'consumer_id' => $consumer->id,
                            'matching_tag_id' => $matching_data->matching_tag_id,
                            'matching_data_id' => $matching_data->id,
                        ];
                    }
                }
            }
        }
        \App\ConsumerMatchingData::insert($consumer_tag_data);

        $consumer_surveys = new \App\ConsumerFeedbackSurvey([
            'feedback_survey_id' => $request->id,
        ]);

        $feedback = FeedBackSurvey::where('id', $request->id)->first();

        $consumer_campaigns = ConsumerCampaign::where([['consumer_id', $consumer->id], ['campaign_id', $feedback->campaign_id]])->first();

        $consumer_campaigns->has_feedback = true;
        $consumer_campaigns->save();

        $consumer->feedback_surveys()->save($consumer_surveys);
        // $feedback->decrement('quantity', 1);
        $consumer->increment('points', $feedback->points);

        return response()->json(['user' => $consumer, 'points' => $feedback->points], 200);
    }

    public function load_freebie_survey(Request $request, $id)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $check = ConsumerDailySurvey::where([['daily_survey_id', $id], ['consumer_id', $consumer->id]])->first();
        $survey = DailySurvey::with('survey')->where('id', $id)->first();
        $taken = $check ? true : false;
        return response()->json(['survey' => $survey->survey, 'is_taken' => $taken], 200);
    }

    public function load_feedback_survey(Request $request, $id)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $check = ConsumerFeedbackSurvey::where([['feedback_survey_id', $id], ['consumer_id', $consumer->id]])->first();
        $survey = FeedbackSurvey::with('survey')->where('id', $id)->first();
        $taken = $check ? true : false;
        return response()->json(['survey' => $survey->survey, 'is_taken' => $taken], 200);
    }

    public function load_offer_page(Request $request, $id)
    {
        $campaign = Campaign::with(['page', 'brand'])->where('id', $id)->first();
        logger($request);
        $check = ShippingAddress::where('user_id', $request->user->id)->first();
        $qq = is_null($check->full_address) ? false : true;
        return response()->json(['data' => $campaign, 'can_accept'=> $qq], 200);
    }

    public function accept_offer(Request $request, $id)
    {
        $consumer = Consumer::where('user_id', $request->user->id)->first();
        $campaign = Campaign::with(['inbound_inventories'])->where([['id', $id], ['status', 'OPEN']])->first();
        if ($campaign) {
            $check = ConsumerCampaign::where([['consumer_id', $consumer->id], ['campaign_id', $campaign->id]])->first();
            $quantityCheck = InboundInventory::where('campaign_id', $campaign->id)->sum('quantity');
            if(!$check && $quantityCheck > 0){
            $consumer_campaign = new ConsumerCampaign();
            $consumer_campaign->consumer_id = $consumer->id;
            $consumer_campaign->campaign_id = $campaign->id;
            $consumer_campaign->save();
            //save outbound
            $outbound = new OutboundInventory();
            $outbound->consumer_id = $consumer->id;
            $outbound->shipping_address_id = $consumer->id;
            $outbound->last_sample_added_on = now();
            $outbound->status = "QUEUED";
            $outbound->campaign_id = $campaign->id;
            $outbound->save();
            //save inventory log
            $inventory_log = new InventoryLog();
            $inventory_log->outbound_inventory_id = $outbound->id;
            $inventory_log->inbound_inventory_id = $campaign->inbound_inventories->id;
            $inventory_log->category_id = $campaign->category_id;
            $inventory_log->save();
            InboundInventory::where('campaign_id', $campaign->id)->decrement('quantity', 1);
            return response()->json(['data' => $campaign], 200);
            }elseif($quantityCheck < 1){
                $campaign->status = "ENDED";
                $campaign->save();
                return response()->json(['error' => 'Oops! This Campaign is closed!'], 500);
            }else{
                return response()->json(['error' => 'You have already accepted this offer!'], 500);
            }

        } else {
            return response()->json(['error' => 'Campaign does not exist or is expired'], 500);
        }

    }

    public function load_referrals(Request $request){
        $user = User::find($request->user->id);
        $referrals = User::where('referrer', $user->ref)->get();
        $All = Consumer::whereIn('user_id', $referrals->pluck('id'))->get();
        if(!$request->page){
            // $Allrefs = $All->filter(function($q){
            //     $stages = collect(["BASIC","PHONE","ACCOUNT","SURVEY"]); //no EMAIL
            //     $user_reg_status = json_decode($q->registration_status, true);
            //     $res = $stages->diff($user_reg_status);
            //     return count($res->all()) == 0;
            // });
            $Allrefs = $All->where('is_reg_complete', true)->count();
            // $Pendingrefs = $All->filter(function($q){
            //     $stages = collect(["BASIC","PHONE","ACCOUNT","SURVEY"]); //no EMAIL
            //     $user_reg_status = json_decode($q->registration_status, true);
            //     $res = $stages->diff($user_reg_status);
            //     return count($res->all()) > 0;
            // });
            $Pendingrefs = $All->where('is_reg_complete', false)->count();

            $data = [];
            $data['total_ref'] = $Allrefs;
            $data['pending_points'] = $Pendingrefs * 20;
            $data['number_ref_points'] = $Allrefs * 20;
            return response()->json(['data' => $data], 200);
        }else{
            $All = Consumer::whereIn('user_id', $referrals->pluck('id'))->paginate(15);
            return response()->json(['data' => $All], 200);
        }

    }

}
