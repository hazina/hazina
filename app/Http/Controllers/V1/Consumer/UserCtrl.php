<?php

namespace App\Http\Controllers\V1\Consumer;

use Image;
use App\User;
use App\Consumer;
use Carbon\Carbon;
use App\MatchingTag;
use App\MatchingTagData;
use App\ShippingLocation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\ConsumerMatchingData;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendUpdateEmailVerify;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\BasicEdit;
use App\Http\Requests\User\ResetEmail;
use App\Http\Requests\User\ChangeEmail;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\User\ResetPassword;
use App\Http\Requests\User\OnboardAccountUpdate;

class UserCtrl extends Controller
{
    /**
     * This endpoint fetchs user details
     *
     * @param Request $request
     * @return void
     */
    public function profile(Request $request)
    {
        $user = Consumer::where('user_id', $request->user->id)->first();
        return response()->json(['user' => $user], 200);
    }

    /**
     * This Endpoint handles consumer's basic settings
     */

    public function basic_settings(BasicEdit $request)
    {
        $user = User::find($request->user->id);

        $user->username = $request->username;

        $user->save();

        $consumer = Consumer::where('user_id', $user->id)->first();
        $consumer->lastname = $request->lastname;
        $consumer->firstname = $request->firstname;
        // $consumer->account_number = $request->account_number;
        // $consumer->account_name = $request->account_name;
        // $consumer->bank = $request->bank;
        $consumer->birthdate = Carbon::parse($request->age);

        if (request()->hasFile('photo') && request()->file('photo')->isValid()) {
            $path = request()->photo->store('consumers/' . $user->id, 's3');
            //$path = Storage::putFile('consumers', $request->photo, 'public');
            //$this->createThumbnail(public_path('storage/' . $path), 100, 100);
            if ($consumer->photo != null) {
                Storage::disk('s3')->delete($consumer->photo);
            }
            $consumer->photo = $path;
        }

        $consumer->save();

         $age = (string)Carbon::parse($request->age)->age;
         $state = $request->state;

        $get_gender = MatchingTag::with(['matching_data'=>function($q) use($request){
            $q->where('value', strtolower($request->gender))->first();
        }])->where('name', 'gender')->first();
        $get_state = MatchingTag::with(['matching_data'=>function($q) use($state){
            $q->where('id', $state)->first();
        }])->where('name', 'state')->first();
        $get_country = MatchingTag::with(['matching_data'=>function($q){
            $q->where('value', 'nigeria')->first();
        }])->where('name', 'country')->first();
        $get_age = MatchingTag::with(['matching_data'=>function($q) use($age){
            $q->where('value', $age)->first();
        }])->where('name', 'age')->first();

        $consumer->matching_data()->whereIn('matching_tag_id', [$get_gender->id, $get_state->id, $get_age->id])->delete();

        $consumer->matching_data()->saveMany([
            new ConsumerMatchingData(['matching_tag_id'=> $get_gender->id, 'matching_data_id'=> $get_gender->matching_data->first()->id]),
            new ConsumerMatchingData(['matching_tag_id'=> $get_state->id, 'matching_data_id'=> $get_state->matching_data->first()->id]),
            new ConsumerMatchingData(['matching_tag_id'=> $get_age->id, 'matching_data_id'=> $get_age->matching_data->first()->id]),
            new ConsumerMatchingData(['matching_tag_id'=> $get_country->id, 'matching_data_id'=> $get_country->matching_data->first()->id]),
        ]);

        return response()->json(['user' => $consumer], 200);
    }

    /**
     * This endpoint handles consumer's onboarding account update
     *
     * @param BasicEdit $request
     * @return void
     */
    public function onboard_account_update(OnboardAccountUpdate $request)
    {
        $user = User::find($request->user->id);

        $user->username = $request->username;

        $user->save();

        $consumer = Consumer::where('user_id', $user->id)->first();
        $consumer->lastname = $request->lastname;
        $consumer->firstname = $request->firstname;
        $consumer->birthdate = Carbon::parse($request->age);
        //$consumer->registration_status = 'SURVEY';
        $consumer->save();

        // $fetch_tags = \App\MatchingTag::whereIn('name', ['gender', 'age', 'state'])->select('id', 'name')->get();
        $age = (string)Carbon::parse($request->age)->age;
        $state = $request->state;

        $get_gender = MatchingTag::with(['matching_data'=>function($q) use($request){
            $q->where('value', strtolower($request->gender))->first();
        }])->where('name', 'gender')->first();
        $get_state = MatchingTag::with(['matching_data'=>function($q) use($state){
            $q->where('id', $state)->first();
        }])->where('name', 'state')->first();
        $get_age = MatchingTag::with(['matching_data'=>function($q) use($age){
            $q->where('value', $age)->first();
        }])->where('name', 'age')->first();

        $consumer->matching_data()->saveMany([
            new ConsumerMatchingData(['matching_tag_id'=> $get_gender->id, 'matching_data_id'=> $get_gender->matching_data->first()->id]),
            new ConsumerMatchingData(['matching_tag_id'=> $get_state->id, 'matching_data_id'=> $get_state->matching_data->first()->id]),
            new ConsumerMatchingData(['matching_tag_id'=> $get_age->id, 'matching_data_id'=> $get_age->matching_data->first()->id]),
        ]);

        $reg_stat = collect(json_decode($consumer->registration_status, true));
        $consumer->registration_status = $reg_stat->push('ACCOUNT')->toJson();
        $consumer->save();

        return response()->json(['user' => $consumer], 200);
    }

    /**
     * This Endpoint fetches user states
     *
     * @param Request $request
     * @return void
     */
    public function fetch_states(Request $request)
    {
        $state = MatchingTag::where('name', 'state')->first();
        $states = MatchingTagData::where('matching_tag_id', $state->id)->get();
        return response()->json(['states' => $states], 200);
    }

    /**
     * This endpoint handles user email update
     *
     * @param ChangeEmail $request
     * @return void
     */
    public function change_email(ChangeEmail $request)
    {
        $token = sha1(Str::random(10));
        DB::table('email_resets')->insert([
            'user_id' => $request->user->id,
            'email' => $request->email,
            'token' => $token,
            // 'model' => \App\User::class,
            'created_at' => now(),
        ]);
        $when = now()->addSeconds(3);
        // Mail::to($request->email)->send(new AppChangeEmail($request->user, $token))
        $user = User::find($request->user->id);
        SendUpdateEmailVerify::dispatch($request->email, $user, $token);
        return response()->json(['user' => $user], 200);
    }

    /**
     * This endpoint handles Email verification
     *
     * @param ResetEmail $request
     * @return void
     */
    public function verify_email(ResetEmail $request)
    {
        $query = DB::table('email_resets')->where([['token', $request->token]]);
        $data = $query->first();
        $user = User::where('id', $data->user_id)->first();
        $user->email = $data->email;
        $user->save();
        $query->delete();
        $user = User::find($request->user->id);
        return response()->json(['user' => $user], 200);
    }

    /**
     * This handles password update
     *
     * @param ResetPassword $request
     * @return void
     */
    public function change_password(ResetPassword $request)
    {
        $user = User::find(request()->user->id);
        $user->password = bcrypt($request->new_password);
        $user->save();
        return response(['success' => true], 200);
    }

    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($path);
        //$img = Image::make($path)->resize($width, $height)->save($path);
    }
}
