<?php

namespace App\Http\Controllers\V1\Consumer;

use App\User;
use App\Consumer;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use App\ShippingAddress;
use App\Jobs\VerifyEmail;
use App\Jobs\ResendSmsCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Events\Account\Created;
use App\Events\ApiClient\Created as AppCreated;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\NewAccount;
use App\Http\Requests\Auth\ResetPassword;
use App\Http\Requests\Auth\ForgotPassword;
use App\Http\Requests\Auth\ResendVerification;
use App\Http\Requests\Auth\ClientCreatePasswordVerifiedEmail;
use App\Http\Requests\Auth\ClientCreatePasswordUnverifiedEmail;
use App\Events\ApiClientVerifiedEmail\Created as ApiVerifiedEmailEvent;
use App\Http\Requests\Auth\ClientCreateConsumer as AppClientCreateConsumer;
use App\Jobs\AddToIntercom;
use App\Jobs\ForgotPassword as AppForgotPassword;
use App\Jobs\SendWhatSapp;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthCtrl extends Controller
{

    /**
     * Handles User Login
     *
     * @param Request $request
     * @return Response json web token
     */
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $user = User::where([['email', $email], ['role_id', 3]])->first();
        if (!$user) {
            return response()->json(['errors' => 'Invalid username or password'], 500);
        } else {
            if (Hash::check($password, $user->password) && $user) {
                // if (!$user->email_verified) {
                //     return response()->json(['message' => 'Your email is not verified, please fill out resend verification form to resend verification link', 'id' => $user->id, 'type' => 'em'], 200);
                // }else
                if (!$user->phone_verified) {
                    return response()->json(['message' => 'Your phone is not verified, please click the resend link to get phone verification code.', 'id' => $user->id, 'type' => 'ph'], 200);
                } else {
                    unset($user->password);
                    return response()->json(['token' => $this->createToken($user), 'user_id'=> $user->id], 200);
                }
            } else {
                return response()->json(['errors' => 'Invalid username or password'], 500);
            }
        }

    }

    /**
     * Login with user ID
     *
     * @param Request $request
     * @param string $id
     * @return void
     */
    public function authy(Request $request, $id){
        try{
            $user = User::where('id', $id)->firstOrFail();
            return response()->json(['token' => $this->createToken($user)], 200);
        }catch(ModelNotFoundException $e){
            return response()->json(['errors' => 'Unbale to fetch user'], 500);
        }
    }

    /**
     * Handles consumer registration
     *
     * @param NewAccount $request
     * @return User user id
     */
    public function register(NewAccount $request)
    {
        if ($request->validated()) {
            //First we generate an email verification token and mail it
            $email_token = sha1(Str::random(10));
            $ref_message = "";
            $user = new User();
            $getUser = explode("@", $request->email);
            $user->username = $getUser[0];
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->role_id = 3;
            $user->ref = Str::random(10);
            if($request->ref_code){
                $referr = User::where('ref', $request->ref_code)->first();
                if($referr){
                    $user->referrer = !$request->ref_code ? "" : $request->ref_code;
                }else{
                    $ref_message = "Invalid referral link";
                }
            }
            $user->password = bcrypt($request->password);
            $user->save();
            $consumer = new Consumer();
            $consumer->user_id = $user->id;
            $consumer->api_client_id = $request->api_client->id;
            //$consumer->photo = 'stub/photo/user/user.png';
            $consumer->registration_status = json_encode(['BASIC']);
            $consumer->points = 0;
            $consumer->save();
            ShippingAddress::where('email', $request->email)->delete();
            $user->shipping_address()->save(new ShippingAddress([
                'user_id' => $user->id,
                'shipping_location_id' => 2,
                'email' => $request->email,
            ]));

            event(new Created($user, $request->all(), $email_token));
           // AddToIntercom::dispatch($user, $consumer)->delay(now()->addSeconds(3));
            return response()->json(['data' => ['id' => $user->id, 'ref_message'=> $ref_message]], 200);
        }
    }

    /**
     * Handles Consumer phone number verification
     *
     * @param Request $request
     * @return void
     */
    public function verify_phone(Request $request)
    {
        try {
            $user = User::where('id', $request->id)->firstOrFail();
            $getCode = DB::table('phone_verify')->where([['code', $request->code], ['phone', $user->phone]])->first();
            if (!$getCode) {
                return response()->json(['errors' => 'Invalid code, please check the code and try again or <br>Call to verify</b>'], 500);
            } else {
                $now = Carbon::now();
                $exp = Carbon::parse($getCode->expiry);
                if ($exp->lessThan($now)) {
                    return response()->json(['errors' => 'Your code is expired! resend code or <br>Call to verify</b>'], 500);
                } else {
                    $user->phone_verified = true;
                    $user->phone_verified_at = now();
                    //$user->registration_status = 'PHONE';
                    $user->save();
                    $consumer = Consumer::where('user_id', $user->id)->first();
                    $reg_stat = collect(json_decode($consumer->registration_status, true));
                    $consumer->registration_status = $reg_stat->push('PHONE')->toJson();
                    $consumer->save();

                    DB::table('phone_verify')->where([['code', $request->code], ['phone', $user->phone]])->delete();

                    return response()->json(['token' => $this->createToken($user), 'id'=> $user->id], 200);
                }
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors' => 'You are not Authorized!'], 500);
        }
    }

    public function phone_code_verify(Request $request){
        try {
            $getCode = DB::table('phone_verify')->where([['alt_code', $request->code]])->first();
            $user = User::where('phone', $getCode->phone)->firstOrFail();
            if (!$getCode) {
                return response()->json(['errors' => 'Invalid code, please check the code and try again or <br>Call to verify</b>'], 500);
            } else {
                $now = Carbon::now();
                $exp = Carbon::parse($getCode->expiry);
                if ($exp->lessThan($now)) {
                    return response()->json(['errors' => 'Your code is expired! resend code or <br>Call to verify</b>'], 500);
                } else {
                    $user->phone_verified = true;
                    $user->phone_verified_at = now();
                    //$user->registration_status = 'PHONE';
                    $user->save();
                    $consumer = Consumer::where('user_id', $user->id)->first();
                    $reg_stat = collect(json_decode($consumer->registration_status, true));
                    $consumer->registration_status = $reg_stat->push('PHONE')->toJson();
                    $consumer->save();

                    DB::table('phone_verify')->where([['alt_code', $request->code]])->delete();
                    return response()->json(['id'=> $user->id], 200);
                }
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors' => 'You are not Authorized!'], 500);
        }

    }

    public function whatsapp(Request $request){
        $user = User::where('id', $request->id)->firstOrFail();
        SendWhatSapp::dispatch($user)->delay(now()->addSeconds(3));
    }

    /**
     * Sends Phone number verification code to consumer
     * This is particularly useful when resending verification code
     *
     * @param Request $request
     * @return void
     */
    public function send_code(Request $request)
    {
        try {
            $user = User::where('id', $request->id)->firstOrFail();
            if (!is_null($request->phone)) {
                $user->phone = $request->phone;
                $user->save();
            }
            DB::table('phone_verify')->where([['phone', $user->phone]])->delete();
            ResendSmsCode::dispatch($user)->delay(now()->addSeconds(3));
            //ResendSmsCode::dispatch($user);
            return response()->json(['message' => 'A new code has been sent to ypur phone.'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors' => 'You are not Authorized!'], 500);
        }
    }

    /**
     * A one time endpoint to get consumer's phone number
     *
     * @param Request $request
     * @return void
     */
    public function get_phone_number(Request $request)
    {
        try {
            $user = User::where('id', $request->id)->firstOrFail();
            return response()->json(['phone' => $user->phone, 'reference' => Str::random(20)], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors' => 'You are not Authorized!'], 500);
        }
    }

    /**
     * This is endpoint verifies user's phone number after a successfull call to
     * verify phone number
     *
     * @param Request $request
     * @return void
     */
    public function call_verify(Request $request)
    {
        try {
            $user = User::where('id', $request->id)->firstOrFail();
            $user->phone_verified = true;
            $user->phone_verified_at = now();
            $user->save();
            DB::table('phone_verify')->where([['phone', $user->phone]])->delete();
            $consumer = Consumer::where('user_id', $user->id)->first();
            $reg_stat = collect(json_decode($consumer->registration_status, true));
            $consumer->registration_status = $reg_stat->push('PHONE')->toJson();
            $consumer->save();

            return response()->json(['token' => $this->createToken($user), 'id'=> $user->id], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors' => 'You are not Authorized!'], 500);
        }
    }

    /**
     * This endpoint resends email verification link
     *
     * @param ResendVerification $request
     * @return void
     */
    public function resend_verification(ResendVerification $request)
    {
        $email_token = sha1(Str::random(10));
        $user = User::where('email', $request->email)->first();
        DB::table('email_resets')->where('email', $request->email)->delete();
        DB::table('email_resets')->insert([
            'user_id' => $user->id,
            'email' => $request->email,
            'token' => $email_token,
            'expires_at' => Carbon::now()->addHours(24),
        ]);
        VerifyEmail::dispatch($user->email, $request->all(), $email_token)->delay(now()->addSeconds(3));
        return response()->json(['success' => true], 200);
    }

    /**
     * This endpoint sends password reset link
     *
     * @param ForgotPassword $request
     * @return void
     */
    public function forgot_password(ForgotPassword $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();
        $forgot_password = DB::table('password_resets');
        $token = sha1(Str::random(10));
        try {
            $forgot_password->insert([
                'email' => $user->email,
                'token' => $token,
                'created_at' => now(),
            ]);
            /// Mail::to($request->email)->send(new ForgotPassword($user, $token));
            // ResendSmsCode::dispatch($user)->delay(now()->addSeconds(3));
            AppForgotPassword::dispatch($user, $token, $request->email)->delay(now()->addSeconds(3));
            //AppJobsForgotPassword::dispatch($user, $token, $request->email)->delay(now()->addSeconds(3));
            return response()->json(['success' => 'SUCCESS'], 200);

        } catch (ModelNotFoundException $e) {
            return response()->json(['errors' => 'Email is invalid or does not exist.'], 500);
        }

    }

    /**
     * This endpoint resets consumer's password
     *
     * @param ResetPassword $request
     * @return void
     */
    public function reset_password(ResetPassword $request)
    {
        $user = User::where('email', $request->email)->first();
        $query = DB::table('password_resets')->where([['token', $request->token]]);
        $user->password = bcrypt($request->password);
        $user->save();
        DB::table('password_resets')->where([['email', $request->email]])->delete();
        return response()->json(['success' => true], 200);
    }

    /**
     * This endpoint verifies user's email address
     *
     * @param Request $request
     * @return void
     */
    public function verify_email(Request $request)
    {
        $token = $request->email_token;
        // $user = User::where('emailtoken', $token)->first();
        $checkToken = DB::table('email_resets')->where('token', $token)->first();
        if (!$checkToken) {
            return response()->json(['errors' => 'Your email token is either expired or invalid.'], 500);
        } else {
            $now = Carbon::now();
            $exp = Carbon::parse($checkToken->expires_at);
            if ($exp->lessThan($now)) {
                return response()->json(['errors' => 'Your email token is either expired or invalid.'], 500);
            } else {
                $user = User::where('id', $checkToken->user_id)->first();
                $user->email_verified = true;
                $user->save();
                $consumer = Consumer::where('user_id', $user->id)->first();
                $reg_stat = collect(json_decode($consumer->registration_status, true));
                $consumer->registration_status = $reg_stat->push('EMAIL')->toJson();
                $consumer->save();

                DB::table('email_resets')->where('token', $token)->delete();
                return response()->json(['success' => true], 200);
            }
        }
    }

    /**
     * This Endpoint handles user registration by third party clients. Request client public key
     *
     * @param AppClientCreateConsumer $request
     * @return void
     */
    public function client_create_user(AppClientCreateConsumer $request)
    {

        if ($request->validated()) {
            $email_token = sha1(Str::random(10));

            $user = new User();
            $getUser = explode("@", $request->email);
            $user->username = $getUser[0];
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->role_id = 3;
            $user->password = bcrypt('no-password');
            $user->ref = Str::random(10);
            $user->save();
            $consumer = new Consumer();
            $consumer->user_id = $user->id;
            $consumer->api_client_id = $request->api_client->id;
            $consumer->registration_status = json_encode(['BASIC']);
            $consumer->points = 0;
            $consumer->save();
            ShippingAddress::where('email', $request->email)->delete();
            $user->shipping_address()->save(new ShippingAddress([
                'user_id' => $user->id,
                'shipping_location_id' => 2,
                'email' => $request->email,
            ]));
            if($request->api_client->has_verified_email && $request->api_client->has_verified_phone){
                event(new ApiVerifiedEmailEvent($user));
            }
            if(!$request->api_client->has_verified_email && !$request->api_client->has_verified_phone){
                event(new AppCreated($user, $request->all(), $email_token));
            }
            //AddToIntercom::dispatch($user, $consumer)->delay(now()->addSeconds(3));
            $redirect_url = config('site.front_url') . '/welcome/account/setup/' . $user->id;
            return response()->json(['message' => 'Account created successfully!', 'redirect_url'=> $redirect_url], 200);
        }

    }

    /**
     * This Endpoint handles password creation for consumers coming from third party clients
     *
     * @param ClientCreatePasswordUnverifiedEmail $request
     * @return void
     */
    public function create_password_non_email(ClientCreatePasswordUnverifiedEmail $request)
    {
        if ($request->validated()) {
            $code = $request->code;
            $user = User::where('id', $request->user_id)->first();
            $user->password = bcrypt($request->password);
            $user->phone_verified = true;
            $user->save();
            $consumer = Consumer::where('user_id', $user->id)->first();
            $reg_stat = collect(json_decode($consumer->registration_status, true));
            $consumer->registration_status = $reg_stat->push('PHONE')->toJson();
            $consumer->save();
            DB::table('phone_verify')->where('code', $code)->delete();
            return response()->json(['token' => $this->createToken($user), 'id'=> $user->id], 200);
        }
    }

    /**
     * This Endpoint handles password creation for consumers coming from third party clients
     *
     * @param ClientCreatePasswordVerifiedEmail $request
     * @return void
     */
    public function create_password_email(ClientCreatePasswordVerifiedEmail $request)
    {
        if ($request->validated()) {
            $user = User::where('id', $request->user_id)->first();
            $user->password = bcrypt($request->password);
            $user->phone_verified = true;
            $user->email_verified = true;
            $user->save();
            $consumer = Consumer::where('user_id', $user->id)->first();
            $reg_stat = collect(json_decode($consumer->registration_status, true));
            $consumer->registration_status = $reg_stat->push('PHONE', 'EMAIL')->toJson();
            $consumer->save();
            return response()->json(['token' => $this->createToken($user), 'id'=> $user->id], 200);
        }
    }

    public function createToken($user)
    {
        $payload = [
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60),
        ];
        return JWT::encode($payload, config('jwt.token_secret'));
    }
}
