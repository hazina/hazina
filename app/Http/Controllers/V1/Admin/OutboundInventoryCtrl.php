<?php

namespace App\Http\Controllers\V1\Admin;

use Carbon\Carbon;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ShippingAddress;

class OutboundInventoryCtrl extends Controller
{
    public function index(Request $request){
       // config()->set('database.connections.mysql.strict', false);
        $data = ShippingAddress::with(['outbound'=>function($q){
            $q->with(['campaign'])->where('status', '!=', 'CLAIMED')->get();
        }])->whereHas('outbound')->get()->groupBy('area');
        return response()->json(['data' => $data], 200);
    }
}
