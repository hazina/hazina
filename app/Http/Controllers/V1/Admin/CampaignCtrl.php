<?php

namespace App\Http\Controllers\V1\Admin;

use App\Brand;
use App\Survey;
use App\Campaign;
use App\Category;
use App\Consumer;
use Carbon\Carbon;
use App\MatchingTag;
use App\CampaignPage;
use App\MatchingTagData;
use App\InboundInventory;
use App\ShippingLocation;
use App\OutboundInventory;
use Illuminate\Http\Request;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Campaign\Edit;
use App\Http\Requests\Admin\Campaign\Create;

class CampaignCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $campaigns = Campaign::with(['feedbacks' => function ($q) {
            $q->count();
        }, 'brand'])->whereHas('brand', function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        })->search($search)->latest()->paginate(15);
        return response()->json(['data' => $campaigns], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Create $request)
    {
        $data = $request->validated();
        $campaign = new Campaign();
        //save campaign
        $campaign->title = $data['campaign_name'];
        $campaign->brand_id = $data['brand'];
        $campaign->status = $data['status'];
        $campaign->starts_on = Carbon::parse($data['starts_on']);
        $campaign->ends_on = Carbon::parse($data['ends_on']);
        $campaign->category_id = $data['categories'];
        // $dirCam = 'campaign/' . $campaign->id;
        // if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
        //     $path = $request->photo->store($dirCam, 's3');
        //     $campaign->photo = $path;
        // }
        $dirCam = 'campaign/' . $campaign->id;
        if ($request->hasFile('page_banner') && $request->file('page_banner')->isValid()) {
            $path = $request->file('page_banner')->store($dirCam, 's3');
            $campaign->photo = $path;
        }

        $campaign->save();

        $tag_data = [];
        //SAVE MATCHING DATA
        foreach(explode(",", request()->matching_tag_data) as $matching_tag){
            $dd = MatchingTagData::where('id', $matching_tag)->first();
           // if($dd){
                $tag_data[] = [
                    'campaign_id'=> $campaign->id,
                    'matching_tag_id'=> $dd->matching_tag_id,
                    'matching_data_id'=> $dd->id,
                ];
           // }

        }

        CampaignMatchingData::insert($tag_data);
        //$campaign->matching_data()->save();

        $pageData = json_decode($data['page'], true);
        $dir = 'campaign/' . $campaign->id . '/page';
        if ($request->hasFile('page_banner') && $request->file('page_banner')->isValid()) {
            $path = $request->file('page_banner')->store($dir, 's3');
            $pageData['page_banner'] = Storage::disk('s3')->url($path);
        }

        // if ($request->hasFile('footer_banner') && $request->file('footer_banner')->isValid()) {
        //     $path = $request->file('footer_banner')->store($dir, 's3');
        //     $pageData['footer_banner'] = Storage::disk('s3')->url($path);
        // }

        $pageData['page_pictures'] = [];
        if ($request->hasFile('page_pictures')) {
            //$pageData['page_pictures'] = [];
            foreach ($request->file('page_pictures') as $file) {
                $path = $file->store($dir, 's3');
                $pageData['page_pictures'][] = Storage::disk('s3')->url($path);
            }
        }
        $pg = collect($pageData)->except(['pictures', 'uploads', 'footer.banner', 'footer.time_and_sample']);

        $campaign->page()->save(new CampaignPage(['data' => $pg->toJson()]));

        //foreach($pageData['page_pictures'] as $photo){
            $campaign->inbound_inventories()->save(new InboundInventory([
                'brand_id' => $data['brand'],
                'quantity' => $data['total_products'],
                'in_stock' => true,
                'dispatched' => 0,
                'claimed' => 0,
                'photo' => collect($pageData['page_pictures'])->first,
            ]));


        // // $campaign->survey()->save(new Survey([
        // //     'survey_id' => $data['survey'],
        // // ]));

        $res = [
            'status' => true,
            'object' => $campaign,
        ];
        return response()->json(['data' => $res], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        $data = [];
        $data['conditions'] = Campaign::with(['categories', 'brand', 'matching_data'=>function($q){
            $q->with(['matching_data'=>function($qq){
                $qq->with('matching_tag');
            }])->get();
        }])->where('id', $campaign->id)->first();

        //Consumers Analytics
        //logger($this->getMatched($campaign));
        $all_consumers = $this->getMatched($campaign);
        $male_tag_id = MatchingTagData::where('value', 'male')->first();
        $male_consumer_tag = ConsumerMatchingData::where('matching_data_id', $male_tag_id->id)->whereIn('consumer_id', $this->getMatched($campaign)->pluck('id'))->count();
        $female_tag_id = MatchingTagData::where('value', 'female')->first();
        $female_consumer_tag = ConsumerMatchingData::where('matching_data_id', $female_tag_id->id)->whereIn('consumer_id', $this->getMatched($campaign)->pluck('id'))->count();
        $other_tag_id = MatchingTagData::where('value', 'others')->first();
        $other_consumer_tag = ConsumerMatchingData::where('matching_data_id', $other_tag_id->id)->whereIn('consumer_id', $this->getMatched($campaign)->pluck('id'))->count();
         $male = ($male_consumer_tag / $all_consumers) * 100;
         $getfemale = ($female_consumer_tag / $all_consumers) * 100;
         $getOther = ($other_consumer_tag / $all_consumers) * 100;

        $data['consumers'] = [
            'total' => $all_consumers,
            'male' => round($male),
            'female' => round($getfemale),
            'others' => round($getOther),
        ];

        $samples = OutboundInventory::where('campaign_id', $campaign->id)->get();
        $qty = InboundInventory::where('campaign_id', $campaign->id)->first();
        $data['samples'] = [
            'total' => $qty->quantity,
            'claimed' => $samples->where('status', 'CLAIMED')->count(),
            'matched' => $this->getMatched($campaign)->count(),
        ];
        return response()->json(['data' => $data], 200);
    }

    // public function getMatched($campaign){
    //     $consum = [];
    //     Consumer::chunk(100, function($consumers)use($campaign, &$consum){
    //         $fed = $consumers->filter(function($value, $key)use($campaign){
    //             $matches = [];
    //             $match_count = 0;
    //             $get_tag_group_count = 0;
    //             ConsumerMatchingData::where('consumer_id', $value->id)->chunk(10, function($consumer_matching_data)use($campaign, &$matches, &$match_count, &$get_tag_group_count){
    //                 $campaign_matching_data = CampaignMatchingData::where('campaign_id', $campaign->id)->cursor();
    //                 $get_tag_group_count = $campaign_matching_data->groupBy('matching_tag_id')->count();
    //                 $group_by_campaign_matching_tag = $campaign_matching_data->groupBy('matching_tag_id')->all();
    //                 $consumer_data_ids = $consumer_matching_data->pluck('matching_data_id')->all();
    //                 foreach (collect($group_by_campaign_matching_tag) as $gg) {
    //                     $intersect = array_intersect($gg->pluck('matching_data_id')->all(), $consumer_data_ids);
    //                     if (count($intersect) > 0) {
    //                         $matches[] = 1;
    //                         $match_count += 1;
    //                     }
    //                 }
    //             });
    //            return $match_count === $get_tag_group_count;
    //         });
    //         $consum = $fed;
    //     });
    //     return collect($consum);
    // }

    public function getMatched($campaign){
        // $campaignID=4;
        $campaign = CampaignMatchingData::where('campaign_id', $campaign->id)->get()->groupBy('matching_tag_id');
        $consumers = [];
        ConsumerMatchingData::chunk(100, function($chunk)use(&$consumers, $campaign){
            $cc = $chunk->groupBy('consumer_id');
            foreach (collect($cc) as $gg_key => $gg) {
                $match_count = 0;
                $get_tag_group_count = $campaign->count();
                foreach($campaign as $camp_key => $camp_value){
                    $intersect = array_intersect($camp_value->pluck('matching_data_id')->all(), $gg->pluck('matching_data_id')->all());
                    if (count($intersect) > 0) {
                        //$matches[] = 1;
                        $match_count += 1;
                    }
                }
                if($get_tag_group_count == $match_count){
                    $consumers[] = $gg_key;
                }

            }
        });
        return count($consumers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Edit $request, Campaign $campaign)
    {
        $data = $request->validated();
        CampaignMatchingData::where('campaign_id', $campaign->id)->delete();
        $campaignn = Campaign::where('id', $campaign->id)->first();
        //save campaign
        $campaignn->title = $data['campaign_name'];
        $campaignn->brand_id = $data['brand'];
        $campaignn->status = $data['status'];
        $campaignn->starts_on = Carbon::parse($data['starts_on']);
        $campaignn->ends_on = Carbon::parse($data['ends_on']);
        $campaignn->category_id = $data['categories'];
        $campaignn->save();
        $tag_data = [];
        //SAVE MATCHING DATA
        foreach(explode(",", request()->matching_tag_data) as $matching_tag){
            $dd = MatchingTagData::where('id', $matching_tag)->first();
           // if($dd){
                $tag_data[] = [
                    'campaign_id'=> $campaign->id,
                    'matching_tag_id'=> $dd->matching_tag_id,
                    'matching_data_id'=> $dd->id,
                ];
           // }

        }

        CampaignMatchingData::insert($tag_data);
        //foreach($pageData['page_pictures'] as $photo){
            $campaignn->inbound_inventories()->save(new InboundInventory([
                'brand_id' => $data['brand'],
                'quantity' => $data['total_products'],
                'in_stock' => true,
                'dispatched' => 0,
                'claimed' => 0,
                //'photo' => collect($pageData['page_pictures'])->first,
            ]));

        $res = [
            'status' => true,
            'object' => $campaignn,
        ];
        return response()->json(['data' => $res], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Campaign $campaign)
    {
        $id = $campaign->id;
        $campaign = Campaign::where('id', $id)->first();
        $campaign->inbound_inventories()->delete();
        $campaign->survey()->delete();
        $campaign->feedbacks()->delete();
        $campaign->page()->delete();
        //$campaign->categories()->delete();
        $campaign->matching_data()->delete();
        $campaign->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function bulk_delete(Request $request)
    {
        $ids = $request->ids;
        $query = Campaign::whereIn('id', $ids);
        $query->get()->each(function ($campaign, $key) {
            $campaign->inbound_inventories()->delete();
            $campaign->survey()->delete();
            Storage::disk('s3')->delete("campaign/" . $campaign->id);
            $campaign->page()->delete();
           // $campaign->categories()->delete();
           $campaign->matching_data()->delete();
        });
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

    public function load_create(Request $request){
        $data = [];
        // $data['states'] = ShippingLocation::where('id', '>', 1)->select('name')->get();
        // $data['country'] = ['Nigeria'];
        $data['categories'] = Category::select('id', 'slug', 'title')->get();
        $query = MatchingTag::with('matching_data')->get();
        $data['brands'] = Brand::get();
        $data['matching_tags'] = $query;
        return response()->json(['data' => $data], 200);
    }

    public function load_edit(Request $request, $id){
        $data = [];
        // $data['states'] = ShippingLocation::where('id', '>', 1)->select('name')->get();
        // $data['country'] = ['Nigeria'];
        $data['categories'] = Category::select('id', 'slug', 'title')->get();
        $query = MatchingTag::with('matching_data')->get();
        $data['brands'] = Brand::get();
        $data['matching_tags'] = $query;
        $data['campaign'] = Campaign::with(['categories', 'brand', 'matching_data', 'inbound_inventories'])->where('id', $id)->first();
        $dd = CampaignMatchingData::where('campaign_id', $id)->get();
        $matching_data = MatchingTagData::whereIn('id', $dd->pluck('matching_data_id'))->get();
        // $matching_data = [];
         $selected_matching_tags = MatchingTag::with('matching_data')->whereIn('id', $dd->pluck('matching_tag_id'))->get();
        // foreach($selected_matching_tags as $selected_matching_tag){
        //     $matching_data[] = $selected_matching_tag->matching_data;
        // }
         $data['selected_matching_tags'] = $selected_matching_tags;
        $data['selected_matching_data'] = $matching_data;

        return response()->json(['data' => $data], 200);
    }
}
