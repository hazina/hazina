<?php

namespace App\Http\Controllers\V1\Admin;

use App\Consumer;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ConsumerCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $data = Consumer::with(['api_client'])->whereHas('api_client', function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        })->search($search)->paginate(15);

        return response()->json(['data'=> $data], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consumer  $consumer
     * @return \Illuminate\Http\Response
     */
    public function show(Consumer $consumer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consumer  $consumer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consumer $consumer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consumer  $consumer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $consumer = Consumer::where('id', $request->id)->first();
        Storage::disk('public')->delete($consumer->photo);
        $consumer->campaigns()->delete();
        $consumer->matching_data()->delete();
        $consumer->daily_surveys()->delete();
        User::where('id', $consumer->user_id)->delete();
        $consumer->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function bulk_delete(Request $request)
    {
        $query = Consumer::whereIn('id', $request->ids);
        $query->get()->each(function($consumer, $key){
            Storage::disk('public')->delete($consumer->photo);
            $consumer->campaigns()->delete();
            $consumer->matching_data()->delete();
            $consumer->daily_surveys()->delete();
            User::where('id', $consumer->user_id)->delete();
        });
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

}
