<?php

namespace App\Http\Controllers\V1\Admin;

use App\Freebie;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FreebieCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $freebies = Freebie::latest()->paginate(15);
        return response()->json(['data'=> $freebies], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $freebie = new Freebie();
        $freebie->type = $request->type;
        $freebie->template = $request->type;
       // $freebie->level = $request->level;
        $freebie->points = $request->points;
        $freebie->quantity = $request->quantity;
        $freebie->amount = $request->amount;
        //$freebie->starts_on = null;
       // $freebie->ends_on = null;
        $freebie->save();
        return response()->json(['data'=> $freebie], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Freebie  $freebie
     * @return \Illuminate\Http\Response
     */
    public function show(Freebie $freebie)
    {
        return response()->json(['data'=> $freebie], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Freebie  $freebie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Freebie $freebie)
    {
        //$freebie = new Freebie();
        $freebie->type = $request->type;
        $freebie->template = $request->type;
        //$freebie->level = $request->level;
        $freebie->points = $request->points;
        $freebie->quantity = $request->quantity;
        $freebie->amount = $request->amount;
        // $freebie->starts_on = null;
        // $freebie->ends_on = null;
        $freebie->save();
        return response()->json(['data'=> $freebie], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Freebie  $freebie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Freebie $freebie)
    {
        $freebie->delete();
        return response()->json(['message'=> 'Message deleted'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function bulk_delete(Request $request)
    {
        $query = Freebie::whereIn('id', $request->ids);
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

}
