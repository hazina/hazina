<?php

namespace App\Http\Controllers\V1\Admin;

use App\Survey;
use App\DailySurvey;
use Illuminate\Http\Request;
use App\Applets\SurveyResolver;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DailySurveyCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=> DailySurvey::with('survey:id,title')->paginate(12)], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $survey = new Survey();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $survey->save();
            //Save Matching Tags
            $survey_resolver = new SurveyResolver($request->survey);
            $Built = $survey_resolver->rebuild_survey();
            $survey_resolver->build_tags();
            $survey->survey = json_encode($Built);
            $survey->save();
            $survey_resolver->save_tags($survey);
            $dir = 'daily-survey';
            $photo = "";
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            //Storage::disk('public')->delete(storage_path($brandy->photo));
            $path = $request->file('photo')->store($dir, 's3');
            //Storage::disk('s3')->setVisibility($path, 'public');
            $photo = $path;
        }

            $survey->daily_survey()->save(new DailySurvey([
                'points'=> 2 * intval($survey->number_of_questions),
                'title'=> $request->title,
                'level'=> $request->level,
                'photo'=> $photo
            ]));

            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DailySurvey  $dailySurvey
     * @return \Illuminate\Http\Response
     */
    public function show(DailySurvey $dailySurvey)
    {
        $survey = DailySurvey::with('survey')->where('id', $dailySurvey->id)->first();
        return response()->json(['data'=> $survey], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DailySurvey  $dailySurvey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailySurvey $dailySurvey)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $daily_surveys = DailySurvey::where('id', $dailySurvey->id)->first();
            $survey_id = $daily_surveys->survey_id;
            $survey = Survey::where('id', $survey_id)->first();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $survey->save();
            //Save Matching Tags
            $survey_resolver = new SurveyResolver($request->survey);
            $Built = $survey_resolver->rebuild_survey();
            $survey_resolver->build_tags();
            $survey->survey = json_encode($Built);
            $survey->save();
            $survey_resolver->save_tags($survey);
            // $survey->daily_survey()->update(new DailySurvey([
            //     'level'=> $request->level
            // ]));
            $dir = "daily-survey";
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                Storage::disk('s3')->delete($dailySurvey->photo);
                $path = $request->file('photo')->store($dir, 's3');
                //Storage::disk('s3')->setVisibility($path, 'public');
                $photo = $path;
            }

            DailySurvey::where('id', $dailySurvey->id)->update([
                'points'=> 2 * intval($survey->number_of_questions),
                    'title'=> $request->title,
                    'level'=> $request->level,
                    'photo'=> $photo
            ]);
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DailySurvey  $dailySurvey
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailySurvey $dailySurvey)
    {
        $survey = DailySurvey::where('id', $dailySurvey->id);
        Storage::disk('s3')->delete($dailySurvey->photo);
        $survey->first()->survey()->delete();
        $survey->delete();
        return response()->json(['data' => 'DONE'], 200);
    }



}
