<?php

namespace App\Http\Controllers\V1\Admin;

use App\CampaignPage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=> CampaignPage::with('campaign')->paginate(18)], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignPage  $campaignPage
     * @return \Illuminate\Http\Response
     */
    public function show(CampaignPage $page)
    {
        $pagee = CampaignPage::with('campaign')->where('id', $page->id)->first();
        return response()->json(['data'=> $pagee], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignPage  $campaignPage
     * @return \Illuminate\Http\Response
     */
    public function edit(CampaignPage $page)
    {
        $pagee = CampaignPage::where('id', $page->id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignPage  $campaignPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CampaignPage $campaignPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CampaignPage  $campaignPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(CampaignPage $campaignPage)
    {
        //
    }
}
