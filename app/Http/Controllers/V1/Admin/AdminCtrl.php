<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $brands = User::search($search)->where('role_id', 1)->latest()->paginate(15);
        return response()->json(['data'=> $brands], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255|min:4|unique:users,username',
            'password' => 'required',
            'email' => 'required|email|unique:users,email',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
        $user = new User();
        $user->username = $request->username;
        $user->email = $request->email;
        $user->role_id = 1;
        $user->password = bcrypt($request->password);
        $user->email_verified = true;
        $user->phone_verified = true;
        $user->save();
        return response()->json(['data'=> $user], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $admin_user)
    {
        return response()->json(['data'=> $admin_user], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin_user)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255|min:4|unique:users,username,' . $admin_user->id,
            'password' => 'sometimes',
            'email' => 'required|email|unique:users,email,' . $admin_user->id,
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
        $user = User::where('id', $admin_user->id)->first();
        $user->username = $request->username;
        if(!empty($request->password)){
            $user->password = bcrypt($request->password);
        }
        $user->email = $request->email;
        $user->save();
        return response()->json(['data'=> $user], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $user->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function bulk_delete(Request $request)
    {
        $query = User::whereIn('id', $request->ids);
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

}
