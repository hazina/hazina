<?php

namespace App\Http\Controllers\V1\Admin;

use App\Survey;
use App\Campaign;
use App\FeedbackSurvey;
use Illuminate\Http\Request;
use App\Applets\SurveyResolver;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FeedbackSurveyCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=> FeedbackSurvey::with('survey:title,id')->paginate(12)], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'campaign_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $survey = new Survey();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $survey->save();
            //Save Matching Tags
            $survey_resolver = new SurveyResolver($request->survey);
            $Built = $survey_resolver->rebuild_survey();
            $survey_resolver->build_tags();
            $survey->survey = json_encode($Built);
            $survey->save();
            $survey_resolver->save_tags($survey);

            $survey->feedback_survey()->save(new FeedbackSurvey([
                'campaign_id'=> $request->campaign_id
            ]));

            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeedbackSurvey  $feedbackSurvey
     * @return \Illuminate\Http\Response
     */
    public function show(FeedbackSurvey $feedbackSurvey)
    {
        $survey = FeedbackSurvey::with('survey')->where('id', $feedbackSurvey->id)->first();
        return response()->json(['data'=> $survey, 'campaigns'=> Campaign::get()], 200);
    }

    public function get_campaigns(Request $request){
        $campaigns = Campaign::get();
        return response()->json(['data'=> $campaigns], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeedbackSurvey  $feedbackSurvey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeedbackSurvey $feedbackSurvey)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'campaign_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $daily_surveys = FeedbackSurvey::where('id', $feedbackSurvey->id)->first();
            $survey_id = $daily_surveys->survey_id;
            $survey = Survey::where('id', $survey_id)->first();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $survey->save();
            //Save Matching Tags
            $survey_resolver = new SurveyResolver($request->survey);
            $Built = $survey_resolver->rebuild_survey();
            $survey_resolver->build_tags();
            $survey->survey = json_encode($Built);
            $survey->save();
            $survey_resolver->save_tags($survey);

            FeedbackSurvey::where('id', $feedbackSurvey->id)->update([
                'campaign_id'=> $request->campaign_id
            ]);
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeedbackSurvey  $feedbackSurvey
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeedbackSurvey $feedbackSurvey)
    {
        $survey = FeedbackSurvey::where('id', $feedbackSurvey->id);
       // $survey->first()->survey()->delete();
        $survey->delete();
        return response()->json(['data' => 'DONE'], 200);
    }
}
