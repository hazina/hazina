<?php

namespace App\Http\Controllers\V1\Admin;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InsightCtrl extends Controller
{
    public function index(Request $request){
        $data = Brand::paginate(15);
        return response()->json(['data'=> $data], 200);
    }
}
