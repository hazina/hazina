<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserCtrl extends Controller
{
    public function profile(Request $request)
    {
        $user = User::find($request->user->id);
        return response()->json(['user' => $user], 200);
    }


}
