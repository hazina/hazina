<?php

namespace App\Http\Controllers\V1\Admin;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BrandCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $brands = Brand::search($search)->latest()->paginate(15);
        return response()->json(['data'=> $brands], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->description = $request->description;
        $brand->user_id = request()->user->id;
        $dir = 'brands';
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->file('photo')->store($dir, 's3');
            $brand->photo = $path;
        }
        $brand->save();
        return response()->json(['data'=> $brand], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return response()->json(['data'=> $brand], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brandy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $brandy  = Brand::where('id', $brand->id)->first();
        $brandy->name = $request->name;
        $brandy->description = $request->description;
        $dir = 'brands';
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            Storage::disk('s3')->delete($brandy->photo);
            $path = $request->file('photo')->store($dir, 's3');
            $brandy->photo = $path;
        }
        $brandy->save();
         return response()->json(['data'=> $brand], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
            $brand = Brand::where('id', $request->id)->first();
            Storage::disk('s3')->delete($brand->photo);
            $brand->campaigns()->delete();
            $brand->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function bulk_delete(Request $request)
    {
        $query = Brand::whereIn('id', $request->ids);
        $query->get()->each(function($brand, $key){
            Storage::disk('s3')->delete($brand->photo);
            $brand->campaigns()->delete();
        });
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }
}
