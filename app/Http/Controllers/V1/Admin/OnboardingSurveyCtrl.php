<?php

namespace App\Http\Controllers\V1\Admin;

use App\Applets\SurveyResolver;
use App\Survey;
use App\OnboardingSurvey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OnboardingSurveyCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $onboarding_surveys = OnboardingSurvey::with('survey:id,title')->search($search)->latest()->paginate(15);
        return response()->json(['data'=> $onboarding_surveys], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $survey = new Survey();
            $survey->title = $request->title;
            $survey->number_of_questions = intval($request->questions);
            $survey_resolver = new SurveyResolver($request->survey);
            $Built = $survey_resolver->rebuild_survey();
            $survey_resolver->build_tags();
            $survey->survey = json_encode($Built);
            $survey->save();
            $survey_resolver->save_tags($survey);
            $survey->onboarding_survey()->save(new OnboardingSurvey());
            return response()->json(['status' => 'SUCCESS'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OnboardingSurvey  $onboardingSurvey
     * @return \Illuminate\Http\Response
     */
    public function show(OnboardingSurvey $onboardingSurvey)
    {
        $survey = OnboardingSurvey::with('survey')->where('id', $onboardingSurvey->id)->first();
        return response()->json(['data'=> $survey], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OnboardingSurvey  $onboardingSurvey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OnboardingSurvey $onboardingSurvey)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $onboarding_surveys = OnboardingSurvey::where('id', $onboardingSurvey->id)->first();
            $survey_id = $onboarding_surveys->survey_id;
            $survey = Survey::where('id', $survey_id)->first();
            $survey->title = $request->title;
            $survey->number_of_questions = intval($request->questions);
            $survey_resolver = new SurveyResolver($request->survey);
            $Built = $survey_resolver->rebuild_survey();
            $survey_resolver->build_tags();
            $survey->survey = json_encode($Built);
            $survey->save();
            $survey_resolver->update_tags($survey);
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OnboardingSurvey  $onboardingSurvey
     * @return \Illuminate\Http\Response
     */
    public function destroy(OnboardingSurvey $onboardingSurvey)
    {
        $survey = OnboardingSurvey::where('id', $onboardingSurvey->id);
        $survey->first()->survey()->delete();
        $survey->delete();
        return response()->json(['data' => 'DONE'], 200);
    }
}
