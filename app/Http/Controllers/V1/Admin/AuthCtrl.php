<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPassword as AppForgotPassword;
use App\Http\Requests\Auth\ResetPassword;
use App\Mail\User\ForgotPassword;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthCtrl extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $user = User::where('email', $email)->first();
        if (!$user) {
            return response()->json(['errors' => 'Invalid username or password'], 500);
        } else {
            if (Hash::check($password, $user->password) && $user) {
                unset($user->password);
                return response()->json(['token' => $this->createToken($user), 'user'=> $user], 200);
            } else {
                return response()->json(['errors' => 'Invalid username or password'], 500);
            }
        }
    }

    public function createToken($user)
    {
        $payload = [
            'sub' => $user->id,
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60),
        ];
        return JWT::encode($payload, config('jwt.token_secret'));
    }
}
