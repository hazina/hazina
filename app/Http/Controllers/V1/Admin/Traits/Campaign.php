<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\Brand;
use App\Survey;
use App\Campaign as AppCampaign;
use App\Category;
use Carbon\Carbon;
use App\MatchingTag;
use App\CampaignPage;
use App\InboundInventory;
use App\ShippingLocation;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Campaign\Create;

trait Campaign
{
    public function load_create_campaign(Request $request)
    {
        $data = [];
        // $data['states'] = ShippingLocation::where('id', '>', 1)->select('name')->get();
        // $data['country'] = ['Nigeria'];
        $data['categories'] = Category::select('id', 'slug', 'title')->get();
        $query = MatchingTag::get();
        $data['state'] = $query->where('name', 'state')->first();
        $data['country'] = $query->where('name', 'country')->first();
        $data['demography'] = $query->whereIn('name', ['gender', 'age'])->all();
        $data['marital_status'] = $query->whereIn('name', ['marital_status'])->first();
        $data['education'] = $query->whereIn('name', ['academic_level'])->first();
        $data['employment'] = $query->whereIn('name', ['employment_status'])->first();
        $data['brands'] = Brand::get();
        return response()->json(['data' => $data], 200);
    }

    public function create_campaign(Create $request)
    {
        $data = $request->validated();
        $campaign = new AppCampaign();
        //save campaign
        $campaign->title = $data['campaign_name'];
        $campaign->brand_id = $data['brand'];
        $campaign->status = $data['status'];
        $campaign->starts_on = Carbon::parse($data['starts_on']);
        $campaign->ends_on = Carbon::parse($data['ends_on']);
        $campaign->category_id = $data['category'];
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->photo->store('campaigns');
            $campaign->photo = $path;
        }
        $campaign->save();

        // $campaign->categories()->attach(json_decode($data['categories'], true));

        $tags = collect(collect(json_decode($data['matching_tags'], true))->toArray());
        //logger($tags->toArray());
        $dd = [];
        $tags->each((function ($value, $key) use ($dd) {
            //  $tag = MatchingTag::where('id', $value['id'])->first();
            $dd[$value['id']] = ['value' => json_encode($value['value'])];
        }));
        // $matching_tags = MatchingTag::whereIn('name', $tags)->get();

        $campaign->matching_tags()->attach($dd);

        $pageData = json_decode($data['page'], true);
        $dir = 'campaign/' . $campaign->id . '/page';
        if ($request->hasFile('page_banner') && $request->file('page_banner')->isValid()) {
            $path = $request->file('page_banner')->store($dir);
            $pageData['page_banner'] = $path;
        }

        if ($request->hasFile('footer_banner') && $request->file('footer_banner')->isValid()) {
            $path = $request->file('page_banner')->store($dir);
            $pageData['footer_banner'] = $path;
        }

        if ($request->hasFile('page_pictures')) {
            $pageData['page_pictures'] = [];
            foreach ($request->file('page_pictures') as $file) {
                $path = $file->store($dir);
                $pageData['page_pictures'][] = $path;
            }
        }
        $pg = collect($pageData)->except(['pictures', 'uploads', 'footer.banner', 'footer.time_and_sample']);

        $campaign->page()->save(new CampaignPage(['data' => $pg->toJson()]));

        $campaign->inbound_inventories()->save(new InboundInventory([
            'brand_id' => $data['brand'],
            'quantity' => $data['total_products'],
            'in_stock' => true,
            'dispatched' => 0,
            'claimed' => 0,
        ]));

        // // $campaign->survey()->save(new Survey([
        // //     'survey_id' => $data['survey'],
        // // ]));

        $res = [
            'status' => true,
            'object' => $campaign,
        ];
        return response()->json(['data' => $res], 200);

    }

    public function get_campaigns(Request $request)
    {
        $search = $request->search;
        $campaigns = AppCampaign::with(['feedbacks' => function ($q) {
            $q->count();
        }, 'brand'])->whereHas('brand', function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        })->search($search)->paginate(15);

        return response()->json(['data' => $campaigns], 200);
    }

    public function delete_campaign(Request $request)
    {
        $id = $request->id;
        $campaign = AppCampaign::find($id);
        $campaign->inbound_inventories()->delete();
        $campaign->survey()->delete();
        $campaign->feedbacks()->delete();
        $campaign->page()->delete();
        //$campaign->categories()->delete();
        $campaign->matching_tags()->detach();
        $campaign->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

    public function bulk_delete_campaign(Request $request)
    {
        $ids = $request->ids;
        $query = AppCampaign::whereIn('id', $ids);
        $query->get()->each(function ($campaign, $key) {
            $campaign->inbound_inventories()->delete();
            $campaign->survey()->delete();
            $campaign->page()->delete();
            //$campaign->categories()->delete();
            $campaign->matching_tags()->detach();
        });
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }

}
