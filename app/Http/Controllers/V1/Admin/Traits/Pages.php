<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\CampaignPage;
use Illuminate\Http\Request;

trait Pages{
    public function pages(Request $request)
    {
        $pages = CampaignPage::with(['campaign' => function ($q) {
            $q->with('brand:id,name')->select('id', 'brand_id', 'title');
        }])->get();
        return response()->json(['data' => $pages], 200);
    }
}
