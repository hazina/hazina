<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\ConsumerFreeby;
use App\InboundInventory as AppInboundInventory;
use Illuminate\Http\Request;

trait InboundInventory
{

    public function inbound_inventory_awaiting(Request $request)
    {
        $data = AppInboundInventory::with(['brand:id,name', 'campaign:id,title'])->where('awaiting_goods', true)->get();
        return response()->json(['data' => $data], 200);
    }

    public function inbound_inventory_in_stock(Request $request)
    {
        $data = AppInboundInventory::with(['brand:id,name', 'campaign:id,title'])->where('awaiting_goods', false)->get();
        return response()->json(['data' => $data], 200);
    }

    public function airtime_rewards(Request $request)
    {
        $data = ConsumerFreeby::with(['consumer', 'freebie'])->get()->filter(function ($value, $key) {
            return $value->freebie->type == 'airtime';
        });
        return response()->json(['data' => $data], 200);
    }

    public function cash_rewards(Request $request)
    {
        $data = ConsumerFreeby::with(['consumer', 'freebie'])->get()->filter(function ($value, $key) {
            return $value->freebie->type == 'cash';
        });
        return response()->json(['data' => $data], 200);
    }

    public function coupons_rewards(Request $request)
    {
        $data = ConsumerFreeby::with(['consumer', 'freebie'])->get()->filter(function ($value, $key) {
            return $value->freebie->type == 'coupons';
        });
        return response()->json(['data' => $data], 200);
    }

}
