<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\Brand;
use Illuminate\Http\Request;

trait Insights{
    public function insights(Request $request){
        $data = Brand::paginate(15);
        return response()->json(['data'=> $data], 200);
    }
}
