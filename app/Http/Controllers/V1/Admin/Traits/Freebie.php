<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\Survey;
use Carbon\Carbon;
use App\FreebieSurvey;
use Illuminate\Http\Request;
use App\Freebie as FreebieModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

trait Freebie
{
    public function freebie_surveys(Request $request)
    {
        $data = FreebieSurvey::with(['survey:id,title,photo,number_of_questions', 'freebie'])->paginate(12);
        return response()->json(['data' => $data], 200);
    }

    public function create_freebie_survey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'photo' => 'required|image',
            'amount' => 'required|numeric',
            'type' => 'required',
            'level' => 'required',
            'points' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $freebie = new FreebieModel();
            $freebie->amount = $request->amount;
            $freebie->type = $request->type;
            $freebie->template = $request->type;
            $freebie->level = $request->level;
            $freebie->points = $request->points;
            $freebie->quantity = $request->quantity;
            $freebie->starts_on = Carbon::parse($request->starts_on);
            $freebie->ends_on = Carbon::parse($request->starts_on);
            $dir = 'freebies';
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                Storage::disk('public')->delete($freebie->photo);
                $path = $request->file('photo')->store($dir, 'public');
                $freebie->photo = $path;
            }
            $freebie->save();

            $survey = new Survey();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $survey->save();

            // $survey->freebie_survey()->save(new FreebieSurvey([
            //     'freebie_id'=> $freebie->id,
            //     'points'=> $freebie->points,
            // ]));

            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    public function edit_freebie_survey(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'photo' => '',
            'amount' => 'required|numeric',
            'type' => 'required',
            'level' => 'required',
            'points' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $freebie = FreebieModel::where('id', $id)->first();
            $freebie->amount = $request->amount;
            $freebie->type = $request->type;
            $freebie->template = $request->type;
            $freebie->level = $request->level;
            $freebie->points = $request->points;
            $freebie->quantity = $request->quantity;
            $freebie->starts_on = Carbon::parse($request->starts_on);
            $freebie->ends_on = Carbon::parse($request->starts_on);
            $dir = 'freebies';
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                Storage::disk('public')->delete($freebie->photo);
                $path = $request->file('photo')->store($dir, 'public');
                $freebie->photo = $path;
            }
            $freebie->save();

            $freebie_survey = FreebieSurvey::where('freebie_id', $freebie->id)->first();

            $survey = Survey::where('id', $freebie_survey->survey_id)->first();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $survey->save();

            // $survey->freebie_survey()->save(new FreebieSurvey([
            //     'freebie_id'=> $freebie->id,
            //     'points'=> $freebie->points,
            // ]));

            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    public function fetch_freebie_survey(Request $request, $id)
    {
        $data = FreebieSurvey::with(['survey', 'freebie'])->where('id', $id)->first();
        return response()->json(['data' => $data], 200);
    }

    public function delete_freebie_survey(Request $request, $id)
    {
        $o_survey = FreebieSurvey::where('id', $id)->first();
        $survey = Survey::where('id', $o_survey->survey_id)->first();
        Storage::disk('public')->delete($survey->photo);
        FreebieModel::where('id', $o_survey->freebie_id)->delete();
        $survey->delete();
        $o_survey->delete();
        return response()->json(['data' => 'DONE'], 200);
    }
}
