<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\Campaign;
use App\Consumer;
use App\DailySurvey;
use App\InventoryLog;
use App\FeedbackSurvey;
use App\InboundInventory;
use App\OnboardingSurvey;
use Illuminate\Http\Request;

trait Dashboard
{
    public function dashboard(Request $request)
    {
        $data = collect([]);

        //Consumers Analytics
        $all_consumers = Consumer::count();
        $male_consumers = Consumer::with(['matching_tags' => function ($q) {
            $q->where('name', 'gender')->wherePivot('value', 'MALE')->get();
        }])->get()->filter(function ($q) {
            return count($q->matching_tags) >= 1;
        });
        $male = ($male_consumers->count() / $all_consumers) * 100;
        $getfemale = $all_consumers - $male_consumers->count();
        $female = ($getfemale / $all_consumers) * 100;

        $data['consumers'] = [
            'total' => $all_consumers,
            'male' => round($male),
            'female' => round($female),
        ];
        $data['surveys'] = [
            [
                'title' => 'Feedback Survey',
                'value' => FeedbackSurvey::count(),
            ],
            [
                'title' => 'Freebie Survey',
                'value' => DailySurvey::count(),
            ],
            [
                'title' => 'Onboarding Survey',
                'value' => OnboardingSurvey::count(),
            ],
        ];

        $samples = InboundInventory::get();
        $data['samples'] = [
            'total' => $samples->sum('in_stock'),
            'claimed' => $samples->sum('dispatched'),
            'matched' => $samples->sum('claimed'),
        ];
        $shipping = InventoryLog::get();
        $data['shipping'] = [
            [
                'title' => 'Awaiting Delivery',
                'value' => $shipping->where('status', 'DISPATCHED')->count(),
            ],
            [
                'title' => 'Already Delivered',
                'value' => $shipping->where('status', 'CLAIMED')->count(),
            ],
        ];

        $data['campaigns'] = Campaign::where('status', 'OPEN')->get();

        return response()->json(['data' => $data], 200);
    }
}
