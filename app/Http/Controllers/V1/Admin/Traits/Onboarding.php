<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\OnboardingSurvey;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

trait Onboarding
{
    public function onboarding_surveys(Request $request)
    {
        $data = OnboardingSurvey::with('survey:id,title,photo,number_of_questions')->paginate();
        return response()->json(['data' => $data], 200);
    }

    public function create_onboarding_survey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'photo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $survey = new Survey();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $dir = 'surveys';
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                $path = $request->file('photo')->store($dir);
                $survey->photo = $path;
            }
            $survey->save();

            $survey->onboarding_survey()->save(new OnboardingSurvey());
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    public function edit_onboarding_survey(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'photo' => '',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $onboarding = OnboardingSurvey::where('id', $id)->first();
            $survey = Survey::where('id', $onboarding->survey_id)->first();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $dir = 'surveys';
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                Storage::disk('public')->delete($survey->photo);
                $path = $request->file('photo')->store($dir, 'public');
                $survey->photo = $path;
            }
            $survey->save();
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    public function fetch_onboarding_survey(Request $request, $id)
    {
        $data = OnboardingSurvey::with('survey')->where('id', $id)->first();
        return response()->json(['data' => $data], 200);
    }

    public function delete_onboarding_survey(Request $request, $id)
    {
        $o_survey = OnboardingSurvey::where('id', $id)->first();
        $survey = Survey::where('id', $o_survey->first()->survey_id)->first();
        Storage::disk('public')->delete($survey->photo);
        $survey->delete();
        $o_survey->delete();
        return response()->json(['data' => 'DONE'], 200);
    }
}
