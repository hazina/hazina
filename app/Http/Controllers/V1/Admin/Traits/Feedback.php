<?php
namespace App\Http\Controllers\V1\Admin\Traits;

use App\Survey;
use App\FeedbackSurvey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

trait Feedback{
    public function feedback_surveys(Request $request)
    {
        $data = FeedbackSurvey::with('survey:id,title,photo,number_of_questions')->paginate();
        return response()->json(['data' => $data], 200);
    }

    public function create_feedback_survey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'photo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $survey = new Survey();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $dir = 'surveys';
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                $path = $request->file('photo')->store($dir);
                $survey->photo = $path;
            }
            $survey->save();

            $survey->onboarding_survey()->save(new FeedbackSurvey());
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    public function edit_feedback_survey(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:4',
            'survey' => 'required|json',
            'photo' => '',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 500);
        } else {
            $onboarding = FeedbackSurvey::where('id', $id)->first();
            $survey = Survey::where('id', $onboarding->survey_id)->first();
            $survey->title = $request->title;
            $survey->survey = $request->survey;
            $survey->number_of_questions = intval($request->questions);
            $dir = 'surveys';
            if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
                Storage::disk('public')->delete($survey->photo);
                $path = $request->file('photo')->store($dir, 'public');
                $survey->photo = $path;
            }
            $survey->save();
            return response()->json(['status' => 'SUCCESS'], 200);
        }
    }

    public function fetch_feedback_survey(Request $request, $id)
    {
        $data = FeedbackSurvey::with('survey')->where('id', $id)->first();
        return response()->json(['data' => $data], 200);
    }

    public function delete_feedback_survey(Request $request, $id)
    {
        $o_survey = FeedbackSurvey::where('id', $id)->first();
        $survey = Survey::where('id', $o_survey->first()->survey_id)->first();
        Storage::disk('public')->delete($survey->photo);
        $survey->delete();
        $o_survey->delete();
        return response()->json(['data' => 'DONE'], 200);
    }
}
