<?php

namespace App\Http\Controllers\V1\Admin;

use App\Http\Controllers\Controller;
use App\InboundInventory;
use Illuminate\Http\Request;

class InboundInventoryCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->awiting) {
            $data = InboundInventory::with(['brand:id,name', 'campaign:id,title'])->where('awaiting_goods', false)->paginate(15);
            return response()->json(['data' => $data], 200);
        }
        if ($request->in_stock) {
            $data = InboundInventory::with(['brand:id,name', 'campaign:id,title'])->where('awaiting_goods', false)->get();
            return response()->json(['data' => $data], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inbound_inventory = new InboundInventory();
        $inbound_inventory->brand_id = $request->brand_id;
        $inbound_inventory->campaign_id = $request->campaign_id;
        $inbound_inventory->name = $request->name;
        $inbound_inventory->description = $request->description;
        $inbound_inventory->quantity = $request->quantity;
        $dir = 'inbound_inventory';
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->file('photo')->store($dir);
            $inbound_inventory->photo = $path;
        }
        $inbound_inventory->save();
        return response()->json(['inbound_inventory'=> $inbound_inventory], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InboundInventory  $inboundInventory
     * @return \Illuminate\Http\Response
     */
    public function show(InboundInventory $inboundInventory)
    {
        return response()->json(['inbound_inventory'=> $inboundInventory], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InboundInventory  $inboundInventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InboundInventory $inboundInventoryy)
    {
        $inbound_inventory = InboundInventory::where('id', $inboundInventoryy->id)->first();
        $inbound_inventory->brand_id = $request->brand_id;
        $inbound_inventory->campaign_id = $request->campaign_id;
        $inbound_inventory->name = $request->name;
        $inbound_inventory->description = $request->description;
        $inbound_inventory->quantity = $request->quantity;
        $dir = 'inbound_inventory';
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            Storage::disk('public')->delete($request->photo);
            $path = $request->file('photo')->store($dir);
            $inbound_inventory->photo = $path;
        }
        $inbound_inventory->save();
        return response()->json(['inbound_inventory'=> $inbound_inventory], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InboundInventory  $inboundInventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, InboundInventory $inboundInventory)
    {
        $query = InboundInventory::whereIn('id', $request->ids)->get()->each(function($inbound_inventory, $key){
            Storage::disk('public')->delete($inbound_inventory->photo);
            $inbound_inventory->campaigns()->delete();
        });
        $query->delete();
        return response()->json(['status' => 'DELETED'], 200);
    }
}
