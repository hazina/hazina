<?php

namespace App\Http\Controllers\V1\Admin;

use App\BannerAd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BannerAdsCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $banner_ad = BannerAd::latest()->paginate(15);
        return response()->json(['data'=> $banner_ad], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner_ad = new BannerAd();
        $banner_ad->title = $request->title;
        $dir = 'banner-ads';
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            $path = $request->file('photo')->store($dir, 's3');
            $banner_ad->photo = $path;
        }
        $banner_ad->save();
        return response()->json(['data'=> $banner_ad], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data'=> BannerAd::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner_ad = BannerAd::find($id);
        $banner_ad->title = $request->title;
        $dir = 'banner-ads';
        if ($request->hasFile('photo') && $request->file('photo')->isValid()) {
            Storage::disk('s3')->delete($banner_ad->photo);
            $path = $request->file('photo')->store($dir, 's3');
            $banner_ad->photo = $path;
        }
        $banner_ad->save();
        return response()->json(['data'=> $banner_ad], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $banner = BannerAd::where('id', $id);
        $bb = $banner->first();
        Storage::disk('s3')->delete($bb->photo);
        $banner->delete();
        return response()->json(['data' => 'DONE'], 200);
    }

    public function bulk_delete(Request $request){
        $banners = BannerAd::whereIn('id', $request->ids)->get();
        foreach($banners as $banner){
            Storage::disk('s3')->delete($banner->photo);
            $banner->delete();
        }
        return response()->json(['status' => 'DELETED'], 200);
    }

}
