<?php

namespace App\Http\Controllers\V1\Admin;

use App\Campaign;
use App\Consumer;
use App\DailySurvey;
use App\InventoryLog;
use App\FeedbackSurvey;
use App\MatchingTagData;
use App\InboundInventory;
use App\OnboardingSurvey;
use Illuminate\Http\Request;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;

class DashboardCtrl extends Controller
{
    public function index(Request $request)
    {
        $data = collect([]);

        //Consumers Analytics
        $all_consumers = Consumer::count();
        $male_tag_id = MatchingTagData::where('value', 'male')->first();
        $male_consumer_tag = ConsumerMatchingData::where('matching_data_id', $male_tag_id->id)->count();
        $female_tag_id = MatchingTagData::where('value', 'female')->first();
        $female_consumer_tag = ConsumerMatchingData::where('matching_data_id', $female_tag_id->id)->count();
        $other_tag_id = MatchingTagData::where('value', 'others')->first();
        $other_consumer_tag = ConsumerMatchingData::where('matching_data_id', $other_tag_id->id)->count();
         $male = ($male_consumer_tag / $all_consumers) * 100;
         $getfemale = ($female_consumer_tag / $all_consumers) * 100;
         $getOther = ($other_consumer_tag / $all_consumers) * 100;
        // $female = ($getfemale / $all_consumers) * 100;

        $data['consumers'] = [
            'total' => $all_consumers,
            'male' => round($male),
            'female' => round($getfemale),
            'others' => round($getOther),
        ];
        $data['surveys'] = [
            [
                'title' => 'Feedback Survey',
                'value' => FeedbackSurvey::count(),
            ],
            [
                'title' => 'Daily Survey',
                'value' => DailySurvey::count(),
            ],
            [
                'title' => 'Onboarding Survey',
                'value' => OnboardingSurvey::count(),
            ],
        ];

        $samples = InboundInventory::get();
        $data['samples'] = [
            'total' => $samples->sum('in_stock'),
            'claimed' => $samples->sum('dispatched'),
            'matched' => $samples->sum('claimed'),
        ];
        $shipping = InventoryLog::get();
        $data['shipping'] = [
            [
                'title' => 'Awaiting Delivery',
                'value' => $shipping->where('status', 'DISPATCHED')->count(),
            ],
            [
                'title' => 'Already Delivered',
                'value' => $shipping->where('status', 'CLAIMED')->count(),
            ],
        ];

        $data['campaigns'] = Campaign::where('status', 'OPEN')->get();

        return response()->json(['data' => $data], 200);
    }
}
