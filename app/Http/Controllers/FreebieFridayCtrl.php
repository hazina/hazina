<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Consumer;
use App\MatchingTag;
use App\ConsumerCampaign;
use App\OutboundInventory;
use App\ConsumerDailySurvey;
use Illuminate\Http\Request;
use App\CampaignMatchingData;
use App\ConsumerMatchingData;
use App\Http\Controllers\Controller;
use App\InboundInventory;

class FreebieFridayCtrl extends Controller
{
    public function  index(Request $request){
        if($request->query('manua')){
            $campaignids = [51, 47, 50];
        $count = 1;
        $products = 2;
        $consumers = Consumer::get()->filter(function($value, $key)use($count){
            $products = OutboundInventory::where('consumer_id', $value->id)->count();
            return $products == $count;
        });

        $ffd = $consumers->filter(function($value, $key){
            $products = OutboundInventory::where([['consumer_id', $value->id], ['status', 'CLAIMED']])->count();
            return $products < 1;
        });

        $ffd->each(function($consumer, $key)use($campaignids, $products){
            $selected = collect($campaignids);
            $matching_conds = [];
            $myCamps = ConsumerCampaign::where('consumer_id', $consumer->id)->get();
            $cc = Campaign::whereIn('id', $myCamps->pluck('campaign_id'))->get();
            $finalCamp = Campaign::whereNotIn('category_id', $cc->pluck('category_id'))->whereIn('id', $campaignids)->inRandomOrder()->limit($products)->get();
            $campaign_matchings = CampaignMatchingData::whereIn('campaign_id', $finalCamp->pluck('id'))->get();
            foreach ($campaign_matchings as $campaign_matching) {
                $matching_conds[] = [
                    'matching_tag_id' => $campaign_matching->matching_tag_id, 'matching_data_id' => $campaign_matching->matching_data_id,
                ];
            }
            $consumer->matching_data()->createMany($matching_conds);
        });
        }
        if($request->query('count')){
            $cam_count = intval($request->query('count'));
            $consumers = Consumer::get()->filter(function($value, $key)use($cam_count){
                $products = OutboundInventory::where([['consumer_id', $value->id], ['status', 'QUEUED']])->count();
                return $products == $cam_count;
            });
            $data = [];
            $ff = $consumers->filter(function($value, $key){
                $products = OutboundInventory::where([['consumer_id', $value->id], ['status', 'CLAIMED']])->count();
                return $products < 1;
            });

            foreach ($ff as $consumer) {
                $occ = MatchingTag::where('name', 'employment_status')->first();
                $occc = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $occ->id)->first();

                $mar = MatchingTag::where('name', 'marital_status')->first();
                $marr = $consumer->matching_data()->with('matching_data')->where('matching_tag_id', $mar->id)->first();

                $data[] = [
                    'user_id' => $consumer->id,
                    'firstname' => $consumer->firstname,
                    'lastname' => $consumer->lastname,
                    'email' => $consumer->email,
                    'phone' => $consumer->phone,
                    'age' => $consumer->age,
                    'sex' => $consumer->gender,
                    'occupation' => $occc ? $occc->matching_data->title : "",
                    'marital_status' => $marr ? $marr->matching_data->title : "",
                    'claimed'=> OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'QUEUED']])->count(),
                    'recieved'=> OutboundInventory::where([['consumer_id', $consumer->id], ['status', 'CLAIMED']])->count(),
                    'daily_surveys_taken'=> ConsumerDailySurvey::where('consumer_id', $consumer->id)->count()
                ];
            }
            $csvExporter = new \Laracsv\Export();
            $fileName = 'CONSUMERS_WITH_' . $cam_count . '_CAMPAIGNS.csv';
            $csvExporter->build(collect($data), ['user_id' => 'ID','email'=> 'Email', 'phone'=> 'Phone', 'firstname' => 'Firstname', 'lastname' => 'Lastname', 'age' => 'Age', 'sex' => 'Gender', 'occupation' => 'Occupation', 'marital_status' => 'Relationship', 'claimed'=> 'Claimed', 'recieved'=> 'Recieved Products', 'daily_surveys_taken'=> 'Number Of Daily Surveys Taken'])->download($fileName);

        }

    }

    public function list(Request $request){
        if($request->query('prod') && $request->query('qty')){
            $dd = InboundInventory::where('campaign_id', $request->query('prod'))->first();
            $dd->quantity = intval($request->query('qty'));
            $dd->save();
        }else{
            $campaigns = Campaign::get();
        foreach($campaigns as $campaign){
            echo $campaign->id . '-' . $campaign->title . '<br/>';
        }
        }

    }
}
