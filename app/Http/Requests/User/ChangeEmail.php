<?php

namespace App\Http\Requests\User;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Http\FormRequest;

class ChangeEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                'unique:email_resets',
                // Rule::exists('users')->where(function ($query) {
                //     $query->where('email', request()->email);
                // }),
                function($attribute, $value, $fail){
                    $query = DB::table('users')->where([['email', $value], ['id', '!=', request()->user->id]])->first();
                    if($query){
                        $fail($attribute . ' already exists');
                    }

                },
                function($attribute, $value, $fail){
                    $query = DB::table('users')->where([['email', $value], ['id', request()->user->id]])->first();
                    if($query){
                        $fail( 'This ' .$attribute . ' address is similar to your old one!');
                    }

                },
            ],
        ];
    }

    public function messages()
    {
        return [
            'email.unique'=> 'An email verification link has already been sent to this email',
            //'email.exists'=> 'This email is not registered, please check and try again.'
        ];
    }

}
