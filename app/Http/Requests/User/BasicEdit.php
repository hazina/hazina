<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class BasicEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=> 'required|min:2',
            'firstname'=> 'required|min:1',
            'lastname'=> 'required|min:1',
            'age'=>'required|date',
            'gender'=> 'required',
            'state'=> 'required',
           // 'photo'=> 'sometimes|image|between:1,10240'
            'photo'=> 'sometimes',
            // 'account_number'=> 'required|digits:10',
            // 'account_name'=> 'required',
            // 'bank'=> 'required',
        ];
    }

    public function attributes()
    {
        return [
            'photo'=> 'Profile Picture'
        ];
    }
}
