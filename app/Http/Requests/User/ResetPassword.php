<?php

namespace App\Http\Requests\User;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;

class ResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password'=> [
                'required',
                'min:6',
                function($attribute, $value, $fail){
                    $user = User::find(request()->user->id);
                    if(!Hash::check($value, $user->password)){
                        $fail('This password does not match your old password.');
                    }
                }
            ],
            'new_password'=>[
                'required',
                'min:6',
                function($attribute, $value, $fail){
                    $user = User::find(request()->user->id);
                    if(Hash::check($value, $user->password)){
                        $fail('Your new password cannot be thesame with your old password.');
                    }
                }
            ],
            'confirm_password'=>[
                'same:new_password',
                'required'
            ]
        ];
    }
}
