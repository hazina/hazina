<?php

namespace App\Http\Requests\User;

use App\User;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Http\FormRequest;

class ResetEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => [
                'required',
                Rule::exists('email_resets')->where(function ($q) {
                    $q->where([['token', request()->token]]);
                }),
                function ($attribute, $value, $fail) {
                    $query = DB::table('email_resets')->where('token', $value);
                    $db_token = $query->first();
                    if ($db_token) {
                        $created = Carbon::parse($db_token->created_at);
                        $expiry = $created->addHours(24);
                        if ($expiry->lessThan(now())) {
                            $query->delete();
                            $fail($attribute . ' has expired, please try sending again');
                        }
                    }
                }
            ]
        ];
    }

    public function messages()
    {
        return [
            'token.exists' => 'This token is invalid. please check and try again',
            'token.required' => 'Your must provide the token we sent to you in order to reset your email'
        ];
    }
}
