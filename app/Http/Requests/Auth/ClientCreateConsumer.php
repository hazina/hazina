<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ClientCreateConsumer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=> 'required|email|unique:users',
            'phone'=> 'required|unique:users|regex:/^\+234[0-9]{10}/'
        ];
    }

    public function attributes()
    {
        return [
            'phone'=> 'Phone Number'
        ];
    }
}
