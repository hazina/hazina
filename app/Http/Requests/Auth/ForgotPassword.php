<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ForgotPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', 'unique:password_resets', Rule::exists('users')->where(function ($query) {
                $query->where('email', request()->email);
            })],
        ];
    }

    public function messages()
    {
        return [
            'email.unique'=> 'A request link has already been sent to this email',
            'email.exists'=> 'This email is not registered, please check and try again.'
        ];
    }
}
