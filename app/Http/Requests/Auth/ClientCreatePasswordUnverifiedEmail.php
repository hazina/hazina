<?php

namespace App\Http\Requests\Auth;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Http\FormRequest;

class ClientCreatePasswordUnverifiedEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=> 'required|min:6',
            'user_id'=> 'required',
            'code'=> [
                'required',
                'min:6',
                Rule::exists('phone_verify')->where(function ($q) {
                    $q->where([['code', request()->code]]);
                }),
                function ($attribute, $value, $fail) {
                    $query = DB::table('phone_verify')->where('code', $value);
                    $db_token = $query->first();
                    if ($db_token) {
                        $created = Carbon::parse($db_token->expiry);
                        $expiry = $created->addHours(24);
                        if ($expiry->lessThan(now())) {
                            $query->delete();
                            $fail($attribute . ' has expired, please try sending again');
                        }
                    }
                }
            ]
        ];
    }

    public function messages()
    {
        return [
            'code.exists' => 'Your setup link is invalid. please check and try sending a new link',
            'code.required' => 'Your link is invalid'
        ];
    }

}
