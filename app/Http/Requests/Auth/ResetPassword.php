<?php

namespace App\Http\Requests\Auth;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'email',
                'required',
                'exists:users,email'
            ],
            'token' => [
                'required',
                Rule::exists('password_resets')->where(function ($q) {
                    $q->where([['token', request()->token]]);
                }),
                function ($attribute, $value, $fail) {
                    $query = DB::table('password_resets')->where('token', $value);
                    $db_token = $query->first();
                    if ($db_token) {
                        $created = Carbon::parse($db_token->created_at);
                        $expiry = $created->addHours(24);
                        if ($expiry->lessThan(now())) {
                            $query->delete();
                            $fail($attribute . ' has expired, please try sending again');
                        }
                    }
                }
            ],
            'password' => [
                'min:6',
                'required'
            ]
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => 'This email is not registered, please check and try again.',
            'token.exists' => 'This token is invalid. please check and try again',
            'token.required' => 'Your must provide the token we sent to you in order to reset your password'
        ];
    }
}
