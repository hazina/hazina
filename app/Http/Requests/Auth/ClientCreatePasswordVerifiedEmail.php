<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ClientCreatePasswordVerifiedEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=> 'required|min:6',
            'user_id'=> 'required'
        ];
    }

    public function attributes()
    {
        return [
            'user_id'=> 'User'
        ];
    }

}
