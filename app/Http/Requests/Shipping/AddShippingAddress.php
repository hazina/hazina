<?php

namespace App\Http\Requests\Shipping;

use Illuminate\Foundation\Http\FormRequest;

class AddShippingAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'user_id'=> 'required',
            'shipping_location_id'=> 'required',
            'firstname'=> 'required|min:4',
            'lastname'=> 'required|min:4',
            'address'=> 'required|min:6',
            'phone'=> 'required|min:6|numeric',
            'email'=> 'required|email',
        ];
    }

    public function attributes()
    {
        return [
            //'user_id'=> 'user',
            'shipping_location_id'=> 'location'
        ];
    }
}
