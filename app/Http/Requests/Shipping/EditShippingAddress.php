<?php

namespace App\Http\Requests\Shipping;

use Illuminate\Foundation\Http\FormRequest;

class EditShippingAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'state'=> 'required',
            'lga'=> 'required',
            'area'=> 'required',
            'street_number'=> 'required',
            'street_name'=> 'required',
        ];
    }

    // public function attributes()
    // {
    //     return [
    //         //'user_id'=> 'user',
    //         'state'=> 'State'
    //     ];
    // }
}
