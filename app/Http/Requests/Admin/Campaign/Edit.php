<?php

namespace App\Http\Requests\Admin\Campaign;

use Illuminate\Foundation\Http\FormRequest;

class Edit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand'=> 'required',
            'campaign_name'=> 'required',
            'status'=> 'required',
            'starts_on'=> 'required',
            'ends_on'=> 'required',
            'categories'=> 'required|json',
            'total_products'=> 'required|numeric',
        ];
    }
}
