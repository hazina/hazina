<?php

namespace App\Http\Requests\Admin\Campaign;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand'=> 'required',
            'campaign_name'=> 'required',
            'status'=> 'required',
            'starts_on'=> 'required',
            'ends_on'=> 'required',
            'categories'=> 'required|json',
            //'country'=> 'required',
            //'state'=> 'required',
            //'matching_tags'=> 'required|json',
            'page'=> 'required',
            //'survey'=> 'required',
            'total_products'=> 'required|numeric',
        ];
    }
}
