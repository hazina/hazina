<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignPage extends Model
{

    protected $fillable = ['data'];

    //protected $appends = ['display_data'];

    public function campaign(){
        return $this->belongsTo('App\Campaign');
    }

    public function getPhotoAttribute($value)
    {
        $photos = $value;
        return !$photos ? asset('images/user.png') : asset(\Illuminate\Support\Facades\Storage::disk('s3')->url($photos));
    }

    // public function getDisplayDataAttribute(){

    // }

}
