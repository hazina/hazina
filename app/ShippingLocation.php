<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingLocation extends Model
{
    public function shipping_zone(){
        return $this->belongsTo('\App\ShippingZone');
    }

    public function shipping_addresses(){
        return $this->hasMany('App\ShippingAddress');
    }

}
