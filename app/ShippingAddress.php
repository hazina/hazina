<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{

    protected $appends = ['full_name', 'state_id', 'state_name'];

    protected $fillable = ['shipping_location_id', 'user_id', 'email', 'firstname', 'address', 'phone'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function shipping_location(){
        return $this->belongsTo('App\ShippingLocation');
    }

    public function outbound(){
        return $this->hasOne('App\OutboundInventory');
    }

    public function getFullNameAttribute(){
        return !$this->user ? "" : $this->user->consumer->lastname . ' ' . $this->user->consumer->firstname;
    }

    public function getStateNameAttribute(){
        $state = Location::where('id', $this->state)->first();
        return !$state ? "" : $state;
    }

    public function getStateIdAttribute(){
        return $this->state;
    }

    public function getEmailAttribute($value){
        return !$this->user ? "" : $this->user->email;
    }

    public function getPhoneAttribute($value){
        return !$this->user ? "" : $this->user->phone;
    }


}
