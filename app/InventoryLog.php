<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class InventoryLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inventory_logs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['inbound_inventory_id', 'outbound_inventory_id', 'quantity', 'date', 'status'];

    protected $appends = ['action_date'];

    public function inbound_inventory()
    {
        return $this->belongsTo('App\InboundInventory');
    }
    public function outbound_inventory()
    {
        return $this->belongsTo('App\OutboundInventory');
    }

    public function getActionDateAttribute(){
        return Carbon::parse($this->date)->toFormattedDateString();
    }

}
