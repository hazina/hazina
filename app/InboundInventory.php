<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InboundInventory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inbound_inventories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['brand_id', 'campaign_id', 'in_stock', 'claimed', 'dispatched'];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

    public function getPhotoAttribute($value)
    {
        $photos = $value;
        return !$photos ? " " : asset(\Illuminate\Support\Facades\Storage::url($photos));
    }


}
