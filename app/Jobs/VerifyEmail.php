<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Mail\User\VerifyEmail as LVerifyEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class VerifyEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

     public $email;

     public $email_token;

     public $request;

    public function __construct($email, $request, $email_token)
    {
        $this->email = $email;
        $this->email_token = $email_token;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(new LVerifyEmail($this->email, $this->email_token));
    }
}
