<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddToIntercom implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $user;

    public $consumer;

    public function __construct($user, $consumer)
    {
        $this->user = $user;
        $this->consumer = $consumer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        //logger([$this->consumer->id, $this->user->email, $this->user->phone]);
        $response = $client->post('https://api.intercom.io/contacts', [
            'json' => [
                'role' => 'user',
                'external_id' => $this->consumer->id,
                'email' => $this->user->email,
                'phone' => $this->user->phone
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            'Authorization' => 'Bearer dG9rOjRhNzZhMjEzXzczMjVfNDRjZl85ODg5XzM5ZjBlNjMwZDJmOToxOjA',
            ],
        ]);

        if ($response->getStatusCode() == 200) {

        }
    }
}
