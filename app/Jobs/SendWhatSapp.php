<?php

namespace App\Jobs;

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendWhatSapp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $code = $this->generatePIN(6);
        // $email = "michael@hazina.ng";
        // $password = "@Naija1960";
        $alt_code = Str::random(10);
        $link = "https://gethazina.com/phone-code/verify/" . $alt_code;
        $message = "Use $code to verify your phone number or click the link below to verify $link";
        $sender_name = "Hazina";
        $recipients = $this->user->phone;
        //$forcednd = "set to 1 if you want DND numbers to ";

        $response = $client->post('https://termii.com/api/sms/send', [
            'form_params' => [
                'api_key' => 'TLFwXhakD94uvfEtsprzpMgJzCyQKRDx7XnCbUJFkKlEa12dTOCAMPBlnt7pCZ',
                'sms' => $message,
                'channel' => 'whatsapp',
                'from' => 'Hazina',
                'to' => $recipients,
                'type'=> 'plain'
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        // $response->get
        if ($response->getStatusCode() == 200) {
            //save pin to db with expiry
            DB::table('phone_verify')->where('phone', $this->user->phone)->delete();
            DB::table('phone_verify')->insert([
                'phone' => $this->user->phone,
                'code' => $code,
                'alt_code' => $alt_code,
                'expiry' => Carbon::now()->addMinutes(60),
            ]);
        }
    }

    public function generatePIN($digits = 4)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(1, 9);
            $i++;
        }
        return $pin;
    }

}
