<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerAd extends Model
{
    protected $appends = ['image'];

    protected $fillable = ['photo'];

    public function getImageAttribute()
    {
        $photos = $this->photo;
        return !$photos ? "https://hazina.s3.us-east-2.amazonaws.com/user.png" : asset(\Illuminate\Support\Facades\Storage::disk('s3')->url($photos));
    }
}
