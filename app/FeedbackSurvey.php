<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedbackSurvey extends Model
{
    protected $fillable = ['campaign_id'];

    protected $appends = ['survey_points'];

    public function survey(){
        return $this->belongsTo('App\Survey', 'survey_id');
    }

    public function campaign(){
        return $this->belongsTo('App\Campaign', 'campaign_id');
    }

    public function getSurveyPointsAttribute(){
        return intval($this->survey->number_of_questions) * 2;
    }


}
