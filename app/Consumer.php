<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Consumer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'consumers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'firstname', 'lastname', 'registration_status', 'points', 'photo'];

    protected $appends = ['username', 'email', 'email_verified', 'phone', 'age', 'gender', 'image', 'state', 'registered_on', 'state_text', 'ref', 'reg_status', 'ref_points', 'password'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function feedback_surveys()
    {
        return $this->hasMany('App\ConsumerFeedbackSurvey', 'consumer_id');
    }
    public function daily_surveys()
    {
        return $this->hasMany('App\ConsumerDailySurvey', 'consumer_id');
    }
    public function onboard_survey()
    {
        return $this->hasMany('App\ConsumerOnboardSurvey', 'consumer_id');
    }
    public function freebies()
    {
        return $this->hasMany('App\ConsumerFreeby', 'consumer_id');
    }

    public function outbound_inventories(){
        return $this->hasMany('App\OutboundInventory');
    }

    // public function shipping_address(){
    //     return $this->hasOne('App\ShippingAddress');
    // }

    public function campaigns(){
        return $this->hasMany('App\ConsumerCampaign');
    }

    public function winnings()
    {
        return $this->hasMany('App\OutboundInventory');
    }

    public function matching_data(){
        return $this->hasMany('App\ConsumerMatchingData');
    }

    public function api_client(){
        return $this->belongsTo('App\ApiClient');
    }

    public function scopeSearch($query, $string)
    {
        if ($string) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', $string . '%');
                //$query->orWhere($column, 'LIKE', '%' . $string . '%');
            }
            return $query;
        }
        return $query;
    }

    public function getImageAttribute()
    {
        $photos = $this->photo;
        return !$photos ? "https://hazina.s3.us-east-2.amazonaws.com/user.png" : asset(\Illuminate\Support\Facades\Storage::disk('s3')->url($photos));
    }

    public function getUsernameAttribute()
    {
       return !$this->user ? " " : $this->user->username;
    }

    public function getPasswordAttribute()
    {
       return !$this->user ? " " : $this->user->password;
    }

    public function getEmailAttribute()
    {
        return !$this->user ? " " : $this->user->email;
    }

    public function getEmailVerifiedAttribute()
    {
        return !$this->user ? " " : $this->user->email_verified;
    }

    public function getPhoneAttribute()
    {
       return !$this->user ? " " : $this->user->phone;
    }

    public function getRefAttribute()
    {
       return $this->user->ref;
    }

    public function getAgeAttribute()
    {
        $tag = MatchingTag::where('name', 'age')->first();
        $data = $this->matching_data()->with('matching_data')->where('matching_tag_id', $tag->id)->first();
        return $data ? $data->matching_data->value : "" ;
    }

    public function getBirthdateAttribute($value)
    {
        return is_null($value) ? now()->toDateString() : Carbon::parse($value)->toDateString();
    }

    public function getRegisteredOnAttribute()
    {
        return Carbon::parse($this->created_at)->format('D jS \\of M Y h:i:s A');
    }

    public function getStateAttribute()
    {
        $tag = MatchingTag::where('name', 'state')->first();
        $data = $this->matching_data()->with('matching_data')->where('matching_tag_id', $tag->id)->first();
        return $data ? $data->matching_data->id : "" ;
    }

    public function getStateTextAttribute()
    {
        $tag = MatchingTag::where('name', 'state')->first();
        $data = $this->matching_data()->with('matching_data')->where('matching_tag_id', $tag->id)->first();
        return $data ? $data->matching_data->title : "" ;
    }

    public function getGenderAttribute()
    {
        $tag = MatchingTag::where('name', 'gender')->first();
        $data = $this->matching_data()->with('matching_data')->where('matching_tag_id', $tag->id)->first();
        return $data ? $data->matching_data->value : "" ;
    }

    // public function getOccupationAttribute()
    // {
    //     $tag = MatchingTag::where('name', 'employment_status')->first();
    //     $data = $this->matching_data()->with('matching_data')->where('matching_tag_id', $tag->id)->first();
    //     return $data ? $data->matching_data->value : "" ;
    // }

    // public function getMaritalStatusAttribute()
    // {
    //     $tag = MatchingTag::where('name', 'marital_status')->first();
    //     $data = $this->matching_data()->with('matching_data')->where('matching_tag_id', $tag->id)->first();
    //     return $data ? $data->matching_data->value : "" ;
    // }

    public function getRegStatusAttribute()
    {
        $check = array_diff(['BASIC','PHONE', 'SURVEY', 'ACCOUNT'], json_decode($this->registration_status, true));
        return count($check) == 0 ? 'COMPLETE' : 'INCOMPLETE';
    }

    public function getRefPointsAttribute()
    {
        $user = User::find($this->user_id);
        $referrals = User::where('referrer', $user->ref)->get();
        $All = Consumer::whereIn('user_id', $referrals->pluck('id'))->get();
        $Allrefs = $All->filter(function($q){
            $stages = collect(["BASIC","PHONE","ACCOUNT","SURVEY"]); //no EMAIL
            $user_reg_status = json_decode($q->registration_status, true);
            $res = $stages->diff($user_reg_status);
            return count($res->all()) == 0;
        });
        return $Allrefs->count() * 20;
    }


}
