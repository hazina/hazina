<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerFreeby extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'consumer_freebies';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['consumer_id', 'freebie_id', 'survey_id', 'status', 'user_data'];

    public function consumer(){
        return $this->belongsTo('App\Consumer');
    }

    public function freebie(){
        return $this->belongsTo('App\Freebie');
    }

}
