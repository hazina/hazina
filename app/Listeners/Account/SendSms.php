<?php

namespace App\Listeners\Account;

use App\Applets\SendSms as AppSendSms;
use App\Events\Account\Created;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendSms implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public $delay = 3;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
       $sms = new AppSendSms($event->user);
       $sms->send();
    }

}
