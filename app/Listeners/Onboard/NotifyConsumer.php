<?php

namespace App\Listeners\Onboard;

use App\Mail\Onboard\Matched;
use App\Events\Onboard\Notify;
use App\Mail\Onboard\UnMatched;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyConsumer implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */

     public $delay = 2;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Notify  $event
     * @return void
     */
    public function handle(Notify $event)
    {
        if($event->isMatched){
            Mail::to($event->user->email)->send(new Matched($event->user, $event->campaigns));
        }else{
            Mail::to($event->user->email)->send(new UnMatched($event->user));
        }

    }
}
