<?php

namespace App\Listeners\ApiClientVerifiedEmail;

use App\Mail\User\ApiCreateConsumer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ApiClientVerifiedEmail\Created;

class SendEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $delay = 2;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
        Mail::to($event->user->email)->send(new ApiCreateConsumer($event->user));
    }
}
