<?php

namespace App\Listeners\ApiClient;

use App\Events\ApiClient\Created;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Applets\ApiClientSms;

class SendSms implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $delay = 3;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
        $sms = new ApiClientSms($event->user);
        $sms->send();
    }
}
