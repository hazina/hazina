<?php

namespace App\Listeners\ApiClient;

use Carbon\Carbon;
use App\Mail\User\VerifyEmail;
use App\Events\ApiClient\Created;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public $delay = 5;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Created  $event
     * @return void
     */
    public function handle(Created $event)
    {
        DB::table('email_resets')->insert([
            'user_id'=> $event->user->id,
            'email'=> $event->user->email,
            'token'=> $event->token,
            'expires_at'=> Carbon::now()->addHours(24),
        ]);
        Mail::to($event->user->email)->send(new VerifyEmail($event->request, $event->token, $event->user));
    }
}
