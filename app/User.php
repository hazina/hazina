<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    protected $hidden = ['password'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'email_verified', 'phone', 'phone_verified'];

    //protected $appends = ['points'];

    public function brand()
    {
        return $this->hasOne('App\Brand');
    }
    public function consumer()
    {
        return $this->hasOne('App\Consumer');
    }

    public function shipping_address(){
        return $this->hasOne('App\ShippingAddress');
    }

    public function scopeSearch($query, $string)
    {
        if ($string) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', $string . '%');
                //$query->orWhere($column, 'LIKE', '%' . $string . '%');
            }
            return $query;
        }
        return $query;

    }

}
