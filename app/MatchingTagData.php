<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchingTagData extends Model
{
    protected $fillable = ['ref', 'matching_tag_id', 'title', 'value'];

    public function matching_tag(){
        return $this->belongsTo('App\MatchingTag', "matching_tag_id");
    }
}
