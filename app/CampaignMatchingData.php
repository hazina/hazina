<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignMatchingData extends Model
{
    public function matching_data(){
        return $this->belongsTo('App\MatchingTagData', 'matching_data_id');
    }
}
