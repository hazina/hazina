<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campaigns';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['brand_id', 'title', 'description', 'photo', 'starts_on', 'ends_on', 'status'];

    protected $appends = ['matched', 'claimed', 'elipsis', "added_on"];

    public function scopeSearch($query, $string)
    {
        if ($string) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', $string . '%');
                //$query->orWhere($column, 'LIKE', '%' . $string . '%');
            }
            return $query;
        }
        return $query;

    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
    public function inbound_inventories()
    {
        return $this->hasOne('App\InboundInventory');
    }

    public function survey()
    {
        return $this->hasOne('App\FeedbackSurvey');
    }

    public function page()
    {
        return $this->hasOne('App\CampaignPage');
    }

    // public function matching_tags()
    // {
    //     return $this->belongsToMany('App\MatchingTag', 'campaign_matching_tags')->withPivot('value');
    // }

    public function matching_data()
    {
        return $this->hasMany('App\CampaignMatchingData');
    }

    public function feedbacks()
    {
        return $this->hasMany('App\FeedbackSurvey');
    }

    public function categories()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function consumers()
    {
        return $this->belongsTo('App\ConsumerCampaign');
    }

    public function getPhotoAttribute($value)
    {
        $photos = $value;
        return !$photos ? "" : asset(\Illuminate\Support\Facades\Storage::disk('s3')->url($photos));
    }

    // public function getStartsOnAttribute($value)
    // {
    //     return Carbon::parse($value)->format('D jS \\of M Y h:i:s A');
    // }

    // public function getEndsOnAttribute($value)
    // {
    //     return Carbon::parse($value)->format('D jS \\of M Y h:i:s A');
    // }

    // public function getAddedOnAttribute(){
    //     return Carbon::parse($this->created_at)->format('D jS \\of M Y h:i:s A');
    // }


    public function getMatchedAttribute()
    {

        // $consumer = Consumer::get()->filter(function($value, $key){
        //     $consumer_matching_data = ConsumerMatchingData::where('consumer_id', $value->id)->get();
        //     $campaign_matching_data = CampaignMatchingData::where('campaign_id', $this->id)->get();

        //     $get_tag_group_count = $campaign_matching_data->groupBy('matching_tag_id')->count();
        //     $group_by_campaign_matching_tag = $campaign_matching_data->groupBy('matching_tag_id')->all();
        //     $consumer_data_ids = $consumer_matching_data->pluck('matching_data_id')->all();
        //     $matches = [];
        //     $match_count = 0;
        //     foreach (collect($group_by_campaign_matching_tag) as $gg) {
        //         $intersect = array_intersect($gg->pluck('matching_data_id')->all(), $consumer_data_ids);
        //         if (count($intersect) > 0) {
        //             $matches[] = 1;
        //             $match_count += 1;
        //         }
        //     }
        //     return $match_count === $get_tag_group_count;
        // });

        return 0;
    }

    public function getClaimedAttribute()
    {
        $claimed = $this->inbound_inventories()->sum('claimed');
        return intval($claimed);
    }

    public function getElipsisAttribute(){
        return Str::limit($this->title, '33');
    }

}
