<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'brands';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'description', 'photo'];

    protected $hidden = [];

    protected $appends = ['added_on'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function campaigns()
    {
        return $this->hasMany('App\Campaign');
    }

    public function scopeSearch($query, $string)
    {
        if ($string) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', $string . '%');
                //$query->orWhere($column, 'LIKE', '%' . $string . '%');
            }
            return $query;
        }
        return $query;

    }

    public function getPhotoAttribute($value)
    {
        $photos = $value;
        return !$photos ? asset('images/user.png') : asset(\Illuminate\Support\Facades\Storage::disk('s3')->url($photos));
    }

    public function getAddedOnAttribute()
    {
        return Carbon::parse($this->created_at)->toFormattedDateString();
    }

}
