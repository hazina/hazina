<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Survey extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'success_message', 'survey', 'photo', 'number_of_questions'];

    protected $appends = ['attachment'];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function onboarding_survey(){
        return $this->hasOne('App\OnboardingSurvey');
    }

    public function daily_survey(){
        return $this->hasOne('App\DailySurvey');
    }

    public function feedback_survey(){
        return $this->hasOne('App\FeedbackSurvey');
    }

    public function getAttachmentAttribute()
    {
        $photos = $this->photo;
        return "";
    }



}
