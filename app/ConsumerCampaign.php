<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsumerCampaign extends Model
{

    //protected $table = 'consumer_campaigns';

    protected $fillable = ['campaign_id'];

    public function campaign(){
        return $this->belongsTo('App\Campaign');
    }

    public function consumer(){
        return $this->belongsTo('App\Consumer');
    }
}
