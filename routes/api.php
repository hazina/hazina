<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/gring', 'HomeCtrl@grind');
Route::get('/playground', 'HomeCtrl@playground');
Route::match(['POST', 'GET'], '/simple-logistics', 'HomeCtrl@simple_logistics')->name('simple_logistics');
Route::match(['POST', 'GET'], '/leaderboard', 'HomeCtrl@all_ranks');
Route::match(['POST', 'GET'], '/user-stat', 'HomeCtrl@all_users');
Route::match(['POST', 'GET'], '/maker', 'HomeCtrl@maker');
Route::match(['POST', 'GET'], '/brands/analysis', 'BrandCtrl@home');
Route::match(['POST', 'GET'], '/brands/hair', 'BrandCtrl@setHairs');
Route::match(['POST', 'GET'], '/brands/campaign-report', 'BrandCtrl@campaign_report');
Route::match(['POST', 'GET'], '/brands/manual-matching', 'BrandCtrl@manual_match');
Route::match(['POST', 'GET'], '/brands/manual', '\App\Http\Controllers\Migrations\VersionTwoCtrl@handler');
Route::match(['POST', 'GET'], '/freebie/manual', 'FreebieFridayCtrl@index');
Route::match(['POST', 'GET'], '/freebie/list', 'FreebieFridayCtrl@list');
Route::match(['POST', 'GET'], '/consumer/info', 'ConsumerInfoCtrl@info');
Route::match(['POST', 'GET'], '/race/info', 'RaceCtrl@race');
Route::match(['POST', 'GET'], '/dump/db', 'RaceCtrl@dump_db');
Route::match(['POST', 'GET'], '/ch/match', 'BrandCtrl@match_consumers_to_campaigns');

/**
 * We Group the App Into A single Version
 */
Route::prefix('v1')->middleware(['api.check'])->namespace('V1')->group(function () {

    //The Consumer Routes
    Route::prefix('/consumer')->middleware([])->namespace('Consumer')->group(function () {

        // The Authentication Group
        Route::prefix('/auth')->middleware([])->group(function () {
            Route::post('/login', 'AuthCtrl@login');
            Route::get('/authy/{id}', 'AuthCtrl@authy');
            Route::post('/verify-email', 'AuthCtrl@verify_email');
            Route::post('/phone/verify', 'AuthCtrl@verify_phone');
            Route::post('/phone/send-code', 'AuthCtrl@send_code');
            Route::get('/phone/get-number', 'AuthCtrl@get_phone_number');
            Route::post('/phone/call/verify', 'AuthCtrl@call_verify');
            Route::post('/phone/whatsapp', 'AuthCtrl@whatsapp');
            Route::post('/consumer/auth/phone-code-verify', 'AuthCtrl@phone_code_verify');
            Route::post('/register', 'AuthCtrl@register');
            Route::post('/forgot-password', 'AuthCtrl@forgot_password');
            Route::post('/reset-password', 'AuthCtrl@reset_password');
            Route::post('/resend-verification', 'AuthCtrl@resend_verification');
            Route::post('/create-user', 'AuthCtrl@client_create_user');
            Route::post('/create-password', 'AuthCtrl@create_password_email');
            Route::post('/alt/create-password', 'AuthCtrl@create_password_non_email');
        });

        //The User Group
        Route::prefix('/user')->middleware(['jwt'])->group(function () {
            Route::get('/profile', 'UserCtrl@profile');
            Route::post('/basic-settings', 'UserCtrl@basic_settings');
            Route::post('/onboard-account-update', 'UserCtrl@onboard_account_update');
            Route::get('/fetch-states', 'UserCtrl@fetch_states');
            Route::post('/change-email', 'UserCtrl@change_email');
            Route::post('/change-password', 'UserCtrl@change_password');
            Route::post('/verify-email', 'UserCtrl@verify_email');
        });

        //General Groups
        Route::middleware(['jwt'])->namespace('General')->group(function () {
            Route::get('/load-home', 'DashboardCtrl@load_home');
            Route::get('/load-new-offers', 'NewOffersCtrl@fetch_new_offers');
            Route::get('/load-my-offers', 'MyOffersCtrl@fetch_my_offers');
            Route::get('/load-earn-points/{path}', 'EarnPointsCtrl@init_earn_points');
            Route::get('/load-my-products', 'MyProductsCtrl@init_my_products');
            Route::get('/load-redeem-points', 'ReedemPointsCtrl@init_redeem_points');
            Route::get('/load-onboard-survey', 'OnboardingSurveyCtrl@load_onboard_survey');
            Route::post('/save-onboarding-survey', 'OnboardingSurveyCtrl@save_onboarding_survey');
            Route::get('/load-referrals/{page?}', 'ReferralCtrl@load_referrals');
            Route::get('/load-shipping/{path}', 'ShippingCtrl@init_shipping');
            Route::post('/update-shipping', 'ShippingCtrl@update_shipping_address');
            Route::get('/load/freebie/survey/{id}', 'FreebieSurveyCtrl@load_freebie_survey');
            Route::post('/freebie/save-survey', 'FreebieSurveyCtrl@save_freebie_survey');
            Route::get('/load/feedback/survey/{id}', 'FeedbackSurveyCtrl@load_feedback_survey');
            Route::post('/feedback/save-survey', 'FeedbackSurveyCtrl@save_feedback_survey');
            Route::post('/redeem-points', 'ReedemPointsCtrl@redeem_points');
            Route::post('/accept-offer/{id}', 'AcceptOfferCtrl@accept_offer');
        });

        Route::prefix("/")->middleware(['jwt'])->namespace('General')->group(function () {
            Route::get('campaign/load-offer/{id}', 'MyOffersCtrl@load_offer_page');
        });

    });

    // The Admin Api
    Route::prefix('/admin')->middleware([])->namespace('Admin')->group(function () {

        Route::middleware(['jwt'])->group(function () {

            Route::get('/dashboard', 'DashboardCtrl@index');
            Route::get('/insights', 'InsightCtrl@index');

            //Supplement Routes
            Route::post('/brand/bulk-delete', 'BrandCtrl@bulk_delete');
            Route::post('/consumer/bulk-delete', 'ConsumerCtrl@bulk_delete');
            Route::post('/freebie/bulk-delete', 'FreebieCtrl@bulk_delete');
            Route::post('/admin-user/bulk-delete', 'AdminCtrl@bulk_delete');
            Route::get('/campaign/load-create', 'CampaignCtrl@load_create');
            Route::get('/campaign/load-edit/{id}', 'CampaignCtrl@load_edit');
            Route::post('/campaign/bulk-delete', 'CampaignCtrl@bulk_delete');
            Route::post('/onboarding-survey/bulk-delete', 'CampaignCtrl@bulk_delete');
            Route::post('/category/bulk-delete', 'CategoryCtrl@bulk_delete');
            Route::post('/banner-ads/bulk-delete', 'BannerAdsCtrl@bulk_delete');
            Route::get('/feedback-survey/campaigns', 'FeedbackSurveyCtrl@get_campaigns');
            Route::get('/outbound/all', 'OutboundInventoryCtrl@index');
            Route::resources([
                'brand' => 'BrandCtrl',
                'consumer' => 'ConsumerCtrl',
                'admin-user' => 'AdminCtrl',
                'campaign' => 'CampaignCtrl',
                'page' => 'PageCtrl',
                'category' => 'CategoryCtrl',
                'banner-ads' => 'BannerAdsCtrl',
                'freebie' => 'FreebieCtrl',
                'inbound-inventory' => 'InboundInventoryCtrl',
                'daily-survey' => 'DailySurveyCtrl',
                'feedback-survey' => 'FeedbackSurveyCtrl',
                'onboarding-survey' => 'OnboardingSurveyCtrl',
            ]);

        });

        Route::prefix('/user')->middleware(['jwt'])->group(function () {
            Route::get('/profile', 'UserCtrl@profile');
        });

        Route::prefix('/auth')->middleware([])->group(function () {
            Route::post('/login', 'AuthCtrl@login');
        });

    });

    // The Brand Api
    Route::prefix('/brand')->middleware([])->namespace('Brand')->group(function () {

    });

});
