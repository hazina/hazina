const mix = require("laravel-mix");
const {MIX_PACKAGE, NPM, VENDOR, OUTPUT, output} = require('laravel-multimix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
if (MIX_PACKAGE === 'frontend') {
mix.js("resources/js/frontend/app/main.ts", "public/js/frontend").extract([
    'vue',
    'vuetify',
    'axios',
    'vuelidate',
    'vue-toasted',
    'vue-tel-input-vuetify',
    'vue-meta'
]);
}

if (MIX_PACKAGE === 'consumer') {
mix.js("resources/js/consumer-portal/app/consumer.ts", "public/js/consumer").extract([
    'vue',
    'vuetify',
    'axios',
    'vuelidate',
    'vue-toasted',
    'vue-tel-input-vuetify',
    'vue-meta'
]);
}


// mix.js("resources/js/admin-portal/app/admin.ts", "public/js/admin").extract([
//     'vue',
//     'vuetify',
//     'axios',
//     'vuelidate',
//     'vue-toasted',
//     'vue-tel-input-vuetify',
//     'vue-meta'
// ]).mergeManifest();

// mix.js("resources/js/brand-portal/app/brand.ts", "public/js/brand").extract([
//     'vue',
//     'vuetify',
//     'axios',
//     'vuelidate',
//     'vue-toasted',
//     'vue-tel-input-vuetify',
//     'vue-meta'
// ]).mergeManifest();

// mix.js("resources/js/offer.js", "public/js/offer");
// mix.sass(
//     "resources/sass/app.scss",
//     "public/css"
// )
// .sass("resources/sass/offer.scss", "public/css")
mix.options({
    appendTsSuffixTo: [/.vue$/],
    // extractVueStyles: false
})
.webpackConfig({
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: "ts-loader",
          exclude: /node_modules/
        },
        {
          test: require.resolve('jquery'),
          use: [
              {
                  loader: 'expose-loader',
                  options: '$'
              },
              {
                  loader: 'expose-loader',
                  options: 'jQuery'
              }
          ]
      },
    //   {
    //     test: require.resolve('uikit'),
    //     use: [
    //         {
    //             loader: 'expose-loader',
    //             options: 'UIkit'
    //         }
    //     ]
    //   },
      {
          test: require.resolve('toastr'),
          use: [
              {
                  loader: 'expose-loader',
                  options: 'toastr'
              }
          ]
      }
      ]
    },
    resolve: {
       extensions: [".ts", ".tsx", ".js"],
       //mainFields: ['esnext']
     }
  });

if (mix.inProduction()) {
    mix.version();
}
