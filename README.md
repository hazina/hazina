# Welcome to Hazina!
## Content
 - Authorization
 - installation
 - files and folders
 - building
 - Pushing to git
 - Deployment

## Authorization

This repository is a private repo as such it requires authentication before you can perform any git action. For authorization run the following command

To start SSH service:

    eval $(ssh-agent -s)

To Add Hazina Gitlab's private key, run the command:

    ssh-add hazina_gitlab
You will be prompted to add a passphrase, use the passphrase: "hazina" to authorize. Once authorized, run:

    git pull

## Installation

I have installed all the required npm packages to make vuejs and vuetify run seamlessly. It is heavily based on typescript as it seems to be easier and structured than vanilla javascript (You can also use jquery if you wish, i have configured it already), all you have to do is run firts of all, install npm packages and compile the assets. To do these, run the following commands:

    yarn install
*I prefer yarn because it is faster. To install, visit [https://yarnpkg.com/](https://yarnpkg.com/)*

To compile assets run:

    npm run watch

## files and folders

All the front end files have been grouped into various sub-apps in the `resources/js/` folder, you would have to start developing in the following order: 

 1. frontend
 2. consumer portal
 3. admin portal
 4. brand portal

## Buidling

To buid, simple run the following command:

    npm run prod

